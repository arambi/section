<?php
namespace Section\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Section\View\Helper\CrumbsHelper;

/**
 * Section\View\Helper\CrumbsHelper Test Case
 */
class CrumbsHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Section\View\Helper\CrumbsHelper
     */
    public $Crumbs;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Crumbs = new CrumbsHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Crumbs);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
