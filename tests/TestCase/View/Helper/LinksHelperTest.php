<?php
namespace Section\Test\TestCase\View\Helper;

use Cake\TestSuite\TestCase;
use Cake\View\View;
use Section\View\Helper\LinksHelper;

/**
 * Section\View\Helper\LinksHelper Test Case
 */
class LinksHelperTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Section\View\Helper\LinksHelper
     */
    public $Links;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Links = new LinksHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Links);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
