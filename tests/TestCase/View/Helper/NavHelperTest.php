<?php
namespace Section\Test\TestCase\View\Helper;

use Cake\Core\Configure;
use Cake\TestSuite\TestCase;
use Cake\View\View;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;
use Cake\Core\Plugin;
use Cake\Network\Request;
use Cake\Routing\RouteCollection;
use Cake\Routing\Router;
use Cake\Routing\Route\Route;
use Section\View\Helper\NavHelper;
use Section\Action\ActionCollection;

/**
 * Section\View\Helper\NavHelperTest Test Case
 */
class NavHelperTest extends TestCase
{ 

  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.slugs',
    'plugin.section.rows',
    'plugin.section.blocks',
    'plugin.website.sites',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    TableRegistry::get( 'Website.Sites')->setSite();

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
    $this->setLanguages();
    $view = new View();
    $this->NavHelper = new NavHelper( $view);

    ActionCollection::set( 'entry', [
      'label' => 'Página fija',
      'plugin' => 'Section',
      'controller' => 'Sections',
      'action' => 'view',
      'icon' => 'fa fa-file-text-o',
    ]);

    ActionCollection::set( 'post', [
      'label' => 'Noticias',
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'index',
      'icon' => 'fa fa-newspaper-o',
      'actions' => [
        'view' => '/:slug'
      ]
    ]);

    Plugin::load( 'Section', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
    Configure::write( 'Section', [
      'menus' => [
        'main' => 'Principal',
        'bottom' => 'Inferior',
        'none' => 'Ninguno'
      ]
    ]);

    Plugin::load( 'I18n', ['bootstrap' => false, 'routes' => true, 'autoload' => true]);
    Plugin::routes();
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->NavHelper);
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
    I18n::locale( 'spa');
  }
  
  public function testNav()
  {
    // Construcción de los parámetros del Request para el helper
    $params = [
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'index',
      'section_id' => 1,
      'content_id' => false,    
    ];

    $url = Router::url( $params);

    $this->NavHelper->request = new Request( [
      'url' => $url,
      'params' => $params
    ]);
    // 
    
    $html = $this->NavHelper->nav( 'main', [
      'ul' => [
        0 => [
          'class' => 'navmain'
        ],
        1 => [
          'class' => 'submenu'
        ]
      ],
      'li' => [
        0 => [
          'class' => 'menu-item'
        ],
        1 => [
          'class' => 'submenu-item'
        ]
      ]
    ]);

    $this->assertContains( '<a href="/es/noticias">Noticias</a>',  $html);
    $this->assertContains( '<li class=" current menu-item"><a href="/es/noticias">Noticias</a></li>', $html);
    $this->assertContains( '<a href="/es/nosotros">Nosotros</a>', $html);
  }
}
