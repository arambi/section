<?php
namespace Section\Test\TestCase\View\Cell;

use Cake\TestSuite\TestCase;
use Section\View\Cell\WrapperCell;
use Cake\Core\Configure;
use Cake\Cache\Cache;
use Cake\Controller\Controller;
use Cake\Core\Plugin;
use Cake\Event\EventManager;
use Cake\View\Cell;
use Cake\View\CellTrait;
use TestApp\View\CustomJsonView;
use Cake\ORM\TableRegistry;
use Cake\Network\Request;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Component;
use Section\Controller\Component\SectionComponent;

/**
 * Section\View\Cell\WrapperCell Test Case
 */
class WrapperCellTest extends TestCase
{

  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.layouts',
    'plugin.section.wrappers',
    'plugin.section.rows',
    'plugin.section.blocks',
    'plugin.section.slugs',
    'plugin.website.sites'
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    TableRegistry::get( 'Website.Sites')->setSite();
    Plugin::load( 'TestPlugin', ['bootstrap' => false, 'routes' => true]);
    parent::setUp();
    $this->request = new Request();
    $this->response = $this->getMock('Cake\Network\Response');
    $this->View = new \Cake\View\View( $this->request, $this->response);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset( $this->View);
    unset( $this->request);
    unset( $this->response);
    parent::tearDown();
  }

/**
 * Test para comprobación del funcionamiento de las llamadas al WrapperCell
 */
  public function testRender()
  {
    $controller = new Controller( $this->request);
    $controller->theme( 'TestPlugin');
    $registry = new ComponentRegistry( $controller);
    $component = new SectionComponent( $registry);
    $section = $component->addParamSection( 4);
    $this->request->addParams( ['section' => $section]);
    $main = $this->View->cell( 'Section.Wrapper::display', ['name' => 'main']);
    $right = $this->View->cell( 'Section.Wrapper::display', ['name' => 'right']);
    
    $render = "{$right}";

    $this->assertContains( 'Bloque body', $render);
  }
  
/**
 * Test para comprobación del funcionamiento en el layout de las llamadas al WrapperCell
 */
  public function testRenderLayout()
  {
    $controller = new Controller( $this->request);
    $controller->theme( 'TestPlugin');
    $registry = new ComponentRegistry( $controller);
    $component = new SectionComponent( $registry);
    $section = $component->addParamSection( 4);
    $View = $controller->createView();
    $render = $View->renderLayout( 'Content for layout', 'full');
    $this->assertContains( 'Content for layout', $render);
    $this->assertContains( 'Bloque body', $render);
  }
}
