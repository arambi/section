<?php

namespace Section\Test\Action;

use Cake\TestSuite\TestCase;
use Section\Action\ActionCollection;
use Cake\ORM\TableRegistry;

class ActionCollectionTest extends TestCase
{

  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.slugs',
    'plugin.website.sites',
    'plugin.section.rows',
    'plugin.section.blocks',
  ];
  
  public function setUp() 
  {
    parent::setUp();
  }


  public function tearDown() 
  {
    parent::tearDown();
  }

  public function testSetAction()
  {
    ActionCollection::set( 'entry', [
      'plugin' => 'Section',
      'controller' => 'Sections' 
    ]);

    $this->assertEquals( 'Section', ActionCollection::get( 'entry')->plugin);
    $this->assertEquals( 'index', ActionCollection::get( 'entry')->action);
  }

  public function testToArray()
  {
    ActionCollection::set( 'entry', [
      'plugin' => 'Section',
      'controller' => 'Sections' 
    ]);

    $array = ActionCollection::get( 'entry')->toArray();
    $this->assertEquals( 'Section', $array ['plugin']);
    $this->assertEquals( 'Sections', $array ['controller']);
  }

  public function testAutocreates()
  {
    ActionCollection::set( 'entry', [
      'plugin' => 'Section',
      'controller' => 'Sections' 
    ]);

    ActionCollection::autocreate();
  }

  public function testTypes()
  {
    ActionCollection::set( 'entry', [
      'plugin' => 'Section',
      'controller' => 'Sections',
      'label' => 'Página fija'
    ]);
    $types = ActionCollection::types();

    $this->assertTrue( is_array( $types));
    $this->assertTrue( array_key_exists( 'entry', $types));
    $this->assertTrue( in_array( 'Página fija', $types));
  }
}