<?php
namespace Section\Test\TestCase\Lib;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use Section\Lib\Layout;
use Cake\Network\Request;
use Cake\Network\Response;
use Cake\Controller\Controller;

/**
 * Section\Lib\Layout Test Case
 */
class LayoutTest extends TestCase 
{
  
  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.slugs',
    'plugin.website.sites',
    'plugin.section.rows',
    'plugin.section.blocks',
  ];
  
/**
 * setUp method
 *
 * @return void
 */
  public function setUp() 
  {
    parent::setUp(); 
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() 
  {
    parent::tearDown();
  }

  public function testLoadLayouts()
  {

  }
}
