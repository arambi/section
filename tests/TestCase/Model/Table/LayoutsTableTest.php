<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Section\Model\Table\LayoutsTable;
use Cake\Core\Configure;


/**
 * Section\Model\Table\LayoutsTable Test Case
 */
class LayoutsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
      'Layouts' => 'plugin.section.layouts',
      'Sites' => 'plugin.website.sites',
      'Wrappers' => 'plugin.section.wrappers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
      parent::setUp();
      $config = TableRegistry::exists('Layouts') ? [] : ['className' => 'Section\Model\Table\LayoutsTable'];
      $this->Layouts = TableRegistry::get('Layouts', $config);
      

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
      unset($this->Layouts);

      parent::tearDown();
    }


    public function testConfigure()
    {
      
    }

  
}
