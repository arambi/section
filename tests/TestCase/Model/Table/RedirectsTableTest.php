<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Section\Model\Table\RedirectsTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Section\Model\Table\RedirectsTable Test Case
 */
class RedirectsTableTest extends CrudTestCase
{
    /**
     * Test subject
     *
     * @var \Section\Model\Table\RedirectsTable
     */
    public $Redirects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Section.Redirects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Redirects') ? [] : ['className' => RedirectsTable::class];
        $this->Redirects = TableRegistry::getTableLocator()->get('Redirects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Redirects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Redirects);
        $this->assertCrudDataIndex( 'index', $this->Redirects);
      }
}
