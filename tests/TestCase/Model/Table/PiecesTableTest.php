<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Section\Model\Table\PiecesTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Section\Model\Table\PiecesTable Test Case
 */
class PiecesTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.pieces'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Pieces') ? [] : ['className' => 'Section\Model\Table\PiecesTable'];
        $this->Pieces = TableRegistry::get('Pieces', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pieces);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Pieces);
        $this->assertCrudDataIndex( 'index', $this->Pieces);
      }
}
