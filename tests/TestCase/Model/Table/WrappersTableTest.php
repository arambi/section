<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Section\Model\Table\WrappersTable;
use Cake\Core\Configure;

/**
 * Section\Model\Table\WrappersTable Test Case
 */
class WrappersTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
      'Wrappers' => 'plugin.section.wrappers',
      'Layouts' => 'plugin.section.layouts',
      'Sites' => 'plugin.Website.sites',
      'plugin.section.slugs',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
      parent::setUp();
      $config = TableRegistry::exists('Wrappers') ? [] : ['className' => 'Section\Model\Table\WrappersTable'];
      $this->Wrappers = TableRegistry::get('Wrappers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
      unset($this->Wrappers);

      parent::tearDown();
    }

    public function testDefault()
    {

    }
  
}
