<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Section\Model\Table\SectionsTable;
use Cake\TestSuite\TestCase;
use Cake\Core\Configure;
use I18n\Lib\Lang;
use Cake\I18n\I18n;
use Cake\Collection\Collection;
use Section\Action\ActionCollection;
use Manager\TestSuite\CrudTestCase;
use Cake\Utility\Hash;

/**
 * Section\Model\Table\SectionsTable Test Case
 */
class SectionsTableTest extends CrudTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'plugin.section.sections',
		'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.manager.drafts',
    'plugin.block.rows',
    'plugin.block.columns',
    'plugin.block.contents',
    'plugin.section.layouts',
    'plugin.section.wrappers',
    'plugin.section.slugs',
    'plugin.website.sites',
    'plugin.seo.seos',
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() 
	{
		parent::setUp();
		$config = TableRegistry::exists('Sections') ? [] : ['className' => 'Section\Model\Table\SectionsTable'];
		$this->Sections = TableRegistry::get('Sections', $config);

		ActionCollection::set( 'post', [
      'label' => 'Noticias',
      'plugin' => false,
      'controller' => 'Articles',
      'action' => 'index',
      'icon' => 'fa fa-newspaper-o',
		]);

		ActionCollection::set( 'entry', [
      'label' => 'Noticias',
      'plugin' => false,
      'controller' => 'Articles',
      'action' => 'index',
      'icon' => 'fa fa-newspaper-o',
		]);

		Configure::write( 'Section', [
			'menus' => [
				[
          'key' => 'main',
          'title' => __d( 'admin', 'Principal'),
          'hasSubtitle' => true
        ],
        [
          'key' => 'bottom',
          'title' => __d( 'admin', 'Inferior'),
        ],
        [
          'key' => 'none',
          'title' => __d( 'admin', 'Ninguno'),
        ],
			],			
		]);


		$this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sections);

		parent::tearDown();
	}

	public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }

  public function testConfig()
  {

  }

	public function testSave()
	{
		$this->setLanguages();
		I18n::locale( 'spa');
		$data = [
			'_translations' => [
				'spa' => [
					'title' => 'Una sección'
				],
				'eng' => [
					'title' => 'One Section'
				],
			],
			'layout_id' => 1
		];

		$section = $this->Sections->getNewEntity( $data);
		$saved = $this->Sections->saveContent( $section);

		$this->assertTrue( $saved !== false);

		$section = $this->Sections->find( 'content')->where( ['Sections.id' => $saved->id])->first();
		$this->assertEquals( $section->slug, 'una-seccion');
	}


/**
 * Comprueba el correcto funcionamiento de SectionsTable::getSections()
 */
	public function testGetSections()
	{
		$sections = $this->Sections->getSections();

		// Comprueba el el count() del array devuelto es el mismo número que el número de  menús disponibles
		$this->assertEquals( count( Configure::read( 'Section.menus')), count( $sections));
	}

	public function testSaveTree()
	{
		$section = $this->Sections->get(1);
		$section->set( 'position', 2);
		$this->Sections->save( $section);
		$section = $this->Sections->get(1);
		

		$sections = $this->Sections->getSections();

		foreach( $sections as $menu => $_sections)
		{
			foreach( $_sections ['contents'] as $section)
			{
				$this->Sections->save( $section);
			}
		}

		$sections = $this->Sections->getSections();
	}


	public function testGetByAction()
	{
		$section = $this->Sections->getByAction( 'post');
	}

	public function testGetUrl()
	{
		$url = $this->Sections->getUrl( 5);
		$this->assertEquals( '/es/nosotros/localizacion/lugar', $url);
	}

	public function testGetPath()
	{
		$section = $this->Sections->get( 5);
		$path = $this->Sections->getPath( $section);

		$result = [
			0 => 2,
			1 => 3,
			2 => 5
		];

		$this->assertEquals( 3, count( $path));

		foreach( $path as $key => $_section)
		{
			$this->assertEquals( $result [$key], $_section->id);
		}
	}


	public function testSelectOptions()
	{
		$sections = $this->Sections->selectOptions();
	}

}
