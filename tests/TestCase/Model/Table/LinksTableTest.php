<?php
namespace Section\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Section\Model\Table\LinksTable;
use Manager\TestSuite\CrudTestCase;


/**
 * Section\Model\Table\LinksTable Test Case
 */
class LinksTableTest extends CrudTestCase
{

    /**
     * Test subject
     *
     * @var \Section\Model\Table\LinksTable
     */
    public $Links;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.links'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Links') ? [] : ['className' => 'Section\Model\Table\LinksTable'];
        $this->Links = TableRegistry::get('Links', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Links);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Links);
        $this->assertCrudDataIndex( 'index', $this->Links);
      }
}
