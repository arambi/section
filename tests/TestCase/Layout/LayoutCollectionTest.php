<?php

namespace Section\Test\Action;

use Cake\TestSuite\TestCase;
use Section\Layout\LayoutCollection;

class LayoutCollectionTest extends TestCase
{

  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.slugs',
    'plugin.website.sites',
    'plugin.section.rows',
    'plugin.section.blocks',
  ];
  
  public function setUp() 
  {
    parent::setUp();
  }


  public function tearDown() 
  {
    parent::tearDown();
  }

  public function testBuild()
  {
    LayoutCollection::build( 'TestPlugin');

    $layout = LayoutCollection::get( 'default');
    $this->assertEquals( 'Por defecto', $layout->name);

    $wrapper = $layout->wrapper( 'main');
    $this->assertEquals( 'Principal', $wrapper->name);
  }

 
}