<?php
namespace Section\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Section\Controller\PiecesController;

/**
 * Section\Controller\PiecesController Test Case
 */
class PiecesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.pieces'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
