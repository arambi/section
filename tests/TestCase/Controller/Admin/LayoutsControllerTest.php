<?php
namespace Section\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Section\Controller\Admin\LayoutsController;

/**
 * Section\Controller\Admin\LayoutsController Test Case
 */
class LayoutsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Layouts' => 'plugin.section.layouts'
    ];



    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
    }

    
}
