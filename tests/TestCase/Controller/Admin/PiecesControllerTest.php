<?php
namespace Section\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Section\Controller\Admin\PiecesController;

/**
 * Section\Controller\Admin\PiecesController Test Case
 */
class PiecesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.pieces'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
