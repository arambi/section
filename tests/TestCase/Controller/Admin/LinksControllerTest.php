<?php
namespace Section\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Section\Controller\Admin\LinksController;

/**
 * Section\Controller\Admin\LinksController Test Case
 */
class LinksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.links'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
