<?php
namespace Section\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use Section\Controller\Admin\RedirectsController;

/**
 * Section\Controller\Admin\RedirectsController Test Case
 *
 * @uses \Section\Controller\Admin\RedirectsController
 */
class RedirectsControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Section.Redirects',
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
