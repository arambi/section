<?php
namespace Section\Test\TestCase\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use Section\Controller\Component\RedirectComponent;

/**
 * Section\Controller\Component\RedirectComponent Test Case
 */
class RedirectComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Section\Controller\Component\RedirectComponent
     */
    public $Redirect;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Redirect = new RedirectComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Redirect);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
