<?php
namespace Section\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Network\Request;
use Cake\Routing\RouteCollection;
use Cake\Routing\Router;
use Cake\Routing\Route\Route;
use Cake\TestSuite\TestCase;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

/**
 * Blog\Controller\PostsController Test Case
 */
class SectionIntegrationTest extends IntegrationTestCase 
{

/**
 * Fixtures
 *
 * @var array
 */
  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.layouts',
    'plugin.section.wrappers',
    'plugin.section.rows',
    'plugin.section.blocks',
    'plugin.website.sites',
    'plugin.section.slugs',
  ];

  public function setUp() {
    Configure::write('App.namespace', 'Section\TestApp');
    parent::setUp();
    
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

    // Configure::write('Routing', array('admin' => null, 'prefixes' => []));
    Router::fullbaseUrl('');
    Configure::write('App.fullBaseUrl', 'http://localhost');
    $this->setLanguages();

    Plugin::load( 'Section', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
    Configure::write( 'Section', [
      'menus' => [
        'main' => 'Principal',
        'bottom' => 'Inferior',
        'none' => 'Ninguno'
      ],
    ]);


    Plugin::load( 'I18n', ['bootstrap' => false, 'routes' => true, 'autoload' => true]);
    Plugin::routes();
  }


  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
    I18n::locale( 'spa');
  }


  public function testHomepage()
  {
    $this->get( '/');
    
    $section = $this->_controller->request->params ['section'];
    $layout = $this->_controller->request->params ['layout'];
    $this->assertTrue( $layout->has( 'wrappers'));
    $this->assertTrue( $layout->wrappers [0]->has( 'rows'));
    $this->assertTrue( $layout->wrappers [0]->rows [0]->has( 'blocks'));
    
    $this->assertEquals( $layout->wrappers [0]->rows [0]->blocks [0]->subtype, 'content');
    $this->assertEquals( $layout->wrappers [1]->rows [0]->blocks [0]->subtype, 'text');
  }
}
