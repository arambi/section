<?php
namespace Section\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Section\Controller\LinksController;

/**
 * Section\Controller\LinksController Test Case
 */
class LinksControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.section.links'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
