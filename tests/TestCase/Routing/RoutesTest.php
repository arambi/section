<?php

namespace Cake\Test\TestCase\Routing;

use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Network\Request;
use Cake\Routing\RouteCollection;
use Cake\Routing\Router;
use Cake\Routing\Route\Route;
use Cake\TestSuite\TestCase;
use I18n\Lib\Lang;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

/**
 * RouterTest class
 *
 */
class RouterTest extends TestCase 
{

  public $fixtures = [
    'plugin.section.sections',
    'plugin.i18n.languages',
    'plugin.manager.translates',
    'plugin.section.slugs',
    'plugin.website.sites',
    'plugin.section.rows',
    'plugin.section.blocks',
  ];

/**
 * setUp method
 *
 * @return void
 */
  public function setUp() {
    parent::setUp();
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);

    // Configure::write('Routing', array('admin' => null, 'prefixes' => []));
    Router::fullbaseUrl('');
    Configure::write('App.fullBaseUrl', 'http://localhost');
    $this->setLanguages();

    Plugin::load( 'Section', ['bootstrap' => true, 'routes' => true, 'autoload' => true]);
    Configure::write( 'Section', [
      'menus' => [
        'main' => 'Principal',
        'bottom' => 'Inferior',
        'none' => 'Ninguno'
      ],
    ]);


    Plugin::load( 'I18n', ['bootstrap' => false, 'routes' => true, 'autoload' => true]);
    Plugin::routes();
    
  }

/**
 * tearDown method
 *
 * @return void
 */
  public function tearDown() {
    parent::tearDown();
    Configure::write('App.namespace', 'Section\TestApp');
    Plugin::unload();
    Router::reload();
    Router::defaultRouteClass('Cake\Routing\Route\Route');
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
    I18n::locale( 'spa');
  }

/**
 * Pruebas para URLS
 *
 */
  public function testbaseUrl() 
  {
    $url = Router::url([
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'index',
      'section_id' => 1,
      'content_id' => false,
    ]);

    $this->assertEquals( $url, '/es/noticias');

    $url = Router::url([
      'plugin' => 'Blog',
      'controller' => 'Posts',
      'action' => 'view',
      'slug' => 'primer-post',
      'section_id' => 1,
      'content_id' => false,
    ]);
    $this->assertEquals( $url, '/es/noticias/primer-post');

    $params = [
      'plugin' => 'Section',
      'controller' => 'Sections',
      'action' => 'view',
      'section_id' => 3,
      'content_id' => false,
    ];

    $url = Router::url( $params);

    $this->assertEquals( $url, '/es/nosotros/localizacion');

    $data = Router::parse( "/es/nosotros/localizacion");
    $this->assertEquals( $data ['lang'], 'es');
    $this->assertEquals( $data ['plugin'], 'section');
    $this->assertEquals( $data ['controller'], 'sections');
    $this->assertEquals( $data ['action'], 'view');
    $this->assertEquals( $data ['section_id'], 3);

    $url = Router::url([
      'plugin' => 'Section',
      'controller' => 'Sections',
      'action' => 'view',
      'section_id' => 4,
      'content_id' => false,
    ]);

    $this->assertEquals( $url, '/es');
    $data = Router::parse( "/es");
    $this->assertEquals( $data ['section_id'], 4);

    $data = Router::parse( "/");
    $this->assertEquals( $data ['section_id'], 4);
  }



}
