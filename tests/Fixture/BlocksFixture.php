<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BlocksFixture
 *
 */
class BlocksFixture extends TestFixture
{

/**
 * Table name
 *
 * @var string
 */
    public $table = 'contents';

/**
 * Fields
 *
 * @var array
 */
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'content_type' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'site_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'parent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'subtype' => ['type' => 'string', 'length' => 32, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'category_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'slug' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'published' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'published_at' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'antetitle' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'subtitle' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'body' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'row' => ['type' => 'integer', 'length' => 3, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'position' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'settings' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'photo' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'content_type' => ['type' => 'index', 'columns' => ['content_type'], 'length' => []],
            'site_id' => ['type' => 'index', 'columns' => ['site_id'], 'length' => []],
            'parent_id' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
            'category_id' => ['type' => 'index', 'columns' => ['category_id'], 'length' => []],
            'slug' => ['type' => 'index', 'columns' => ['slug'], 'length' => []],
            'parent_id_2' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
            'published_at' => ['type' => 'index', 'columns' => ['published_at'], 'length' => []],
            'row' => ['type' => 'index', 'columns' => ['row'], 'length' => []],
            'position' => ['type' => 'index', 'columns' => ['position'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];

/**
 * Records
 *
 * @var array
 */
    public $records = [
      [
        'id' => 111,
        'content_type' => 'Blocks',
        'site_id' => null,
        'parent_id' => null,
        'subtype' => 'content',
        'category_id' => 1,
        'slug' => '',
        'published' => 0,
        'published_at' => null,
        'title' => '',
        'antetitle' => '',
        'subtitle' => '',
        'body' => null,
        'created' => '2014-12-03 15:00:23',
        'modified' => '2014-12-03 15:00:23'
      ],
      [
        'id' => 112,
        'content_type' => 'Blocks',
        'site_id' => null,
        'parent_id' => null,
        'subtype' => 'text',
        'category_id' => 2,
        'slug' => '',
        'published' => 0,
        'published_at' => null,
        'title' => '',
        'antetitle' => '',
        'subtitle' => '',
        'body' => 'Bloque body',
        'created' => '2014-12-03 15:00:23',
        'modified' => '2014-12-03 15:00:23'
      ],
        
    ];
}
