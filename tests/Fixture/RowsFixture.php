<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RowsFixture
 *
 */
class RowsFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'content_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'position' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'model' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'content_id' => ['type' => 'index', 'columns' => ['content_id'], 'length' => []],
            'position' => ['type' => 'index', 'columns' => ['position'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];

/**
 * Records
 *
 * @var array
 */
  public $records = [
    [
      'id' => 1,
      'content_id' => 1,
      'position' => 1,
      'model' => 'Wrappers',
      'created' => '2014-12-08 13:04:41'
    ],
    [
      'id' => 2,
      'content_id' => 2,
      'position' => 1,
      'model' => 'Wrappers',
      'created' => '2014-12-08 13:04:41'
    ],
    [
      'id' => 3,
      'content_id' => 1,
      'position' => 1,
      'model' => 'Wrappers',
      'created' => '2014-12-08 13:04:41'
    ],
    [
      'id' => 4,
      'content_id' => 4,
      'position' => 1,
      'model' => 'Sections',
      'created' => '2014-12-08 13:04:41'
    ],
  ];

}
