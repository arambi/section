<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * WrappersFixture
 *
 */
class WrappersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'layout_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cname' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    

    public $records = [
        [
            'id' => 1,
            'layout_id' => 1,
            'cname' => 'main',
            'title' => 'Main',
            'salt' => '6705b4ac3c60b281f51451b1ef5f121a',
            'created' => '2015-01-27 08:32:48',
            'modified' => '2015-01-27 08:32:48'
        ],
        [
            'id' => 2,
            'layout_id' => 1,
            'cname' => 'right',
            'title' => 'Right',
            'salt' => '1705b4ac3c10a281f51451b1ef5a121a',
            'created' => '2015-01-27 08:32:48',
            'modified' => '2015-01-27 08:32:48'
        ],
        [
            'id' => 3,
            'layout_id' => 2,
            'cname' => 'main',
            'title' => 'Main',
            'salt' => '1105b4aa3c10a181a51451b1af5f121a',
            'created' => '2015-01-27 08:32:48',
            'modified' => '2015-01-27 08:32:48'
        ]
    ];
}
