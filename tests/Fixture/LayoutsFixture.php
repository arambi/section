<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LayoutsFixture
 *
 */
class LayoutsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'site_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'template' => ['type' => 'string', 'length' => 36, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'salt' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'by_default' => ['type' => 'boolean', 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'by_default' => ['type' => 'index', 'columns' => ['site_id'], 'length' => []],
            'site_id' => ['type' => 'index', 'columns' => ['by_default'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'site_id' => 1,
            'template' => 'default',
            'title' => 'Por defecto',
            'salt' => '6705b4fc3c60b281f51451b1ef5f921b',
            'by_default' => 1,
            'created' => '2015-01-04 17:01:09',
            'modified' => '2015-01-04 17:01:09'
        ],
        [
            'id' => 2,
            'site_id' => 1,
            'template' => 'full',
            'title' => 'Página completa',
            'salt' => '6705b4ac3c60b281f51451b1ef5f921b',
            'by_default' => 0,
            'created' => '2015-01-04 17:01:09',
            'modified' => '2015-01-04 17:01:09'
        ],
    ];
}
