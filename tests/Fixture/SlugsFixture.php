<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SlugsFixture
 *
 */
class SlugsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'slug' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'model' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'foreign_key' => ['type' => 'integer', 'length' => 8, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'locale' => ['type' => 'string', 'length' => 5, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'model' => ['type' => 'index', 'columns' => ['model'], 'length' => []],
            'foreign_key' => ['type' => 'index', 'columns' => ['foreign_key'], 'length' => []],
            'locale' => ['type' => 'index', 'columns' => ['locale'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'slug' => 'noticias',
            'model' => 'Sections',
            'foreign_key' => 1,
            'locale' => 'spa',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 2,
            'slug' => 'news',
            'model' => 'Sections',
            'foreign_key' => 1,
            'locale' => 'eng',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 3,
            'slug' => 'nosotros',
            'model' => 'Sections',
            'foreign_key' => 2,
            'locale' => 'spa',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 4,
            'slug' => 'about-us',
            'model' => 'Sections',
            'foreign_key' => 2,
            'locale' => 'eng',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 5,
            'slug' => 'localizacion',
            'model' => 'Sections',
            'foreign_key' => 3,
            'locale' => 'spa',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 6,
            'slug' => 'localization',
            'model' => 'Sections',
            'foreign_key' => 3,
            'locale' => 'eng',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 7,
            'slug' => 'inicio',
            'model' => 'Sections',
            'foreign_key' => 4,
            'locale' => 'spa',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 8,
            'slug' => 'home',
            'model' => 'Sections',
            'foreign_key' => 4,
            'locale' => 'eng',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 9,
            'slug' => 'lugar',
            'model' => 'Sections',
            'foreign_key' => 5,
            'locale' => 'spa',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
        [
            'id' => 10,
            'slug' => 'place',
            'model' => 'Sections',
            'foreign_key' => 5,
            'locale' => 'eng',
            'created' => '2015-04-02 07:37:41',
            'modified' => '2015-04-02 07:37:41'
        ],
    ];
}
