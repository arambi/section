<?php
namespace Section\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SectionsFixture
 *
 */
class SectionsFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'layout_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'site_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'title_menu' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'cname' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'parent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'content_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'level' => ['type' => 'integer', 'length' => 3, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'position' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'body_class' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'action' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'target_blank' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'show_menu' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'draft' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'has_action_layouts' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'menu' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'is_homepage' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'published' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'settings' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'layout_id' => ['type' => 'index', 'columns' => ['layout_id'], 'length' => []],
			'parent_id' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
			'content_id' => ['type' => 'index', 'columns' => ['content_id'], 'length' => []],
			'position' => ['type' => 'index', 'columns' => ['position'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'layout_id' => 1,
			'site_id' => 1,
			'title' => 'Noticias',
			'title_menu' => 'Noticias',
			'parent_id' => 0,
			'content_id' => null,
			'position' => 1,
			'body_class' => '',
			'action' => 'post',
			'target_blank' => 0,
			'menu' => 'main',
			'is_homepage' => 0,
			'settings' => '',
			'created' => '2014-11-16 18:22:07',
			'modified' => '2014-11-16 18:22:07'
		],
		[
			'id' => 2,
			'layout_id' => 1,
			'site_id' => 1,
			'title' => 'Nosotros',
			'title_menu' => 'Nosotros',
			'parent_id' => 0,
			'content_id' => null,
			'position' => 2,
			'body_class' => '',
			'action' => 'entry',
			'target_blank' => 0,
			'menu' => 'main',
			'is_homepage' => 0,
			'settings' => '',
			'created' => '2014-11-16 18:22:07',
			'modified' => '2014-11-16 18:22:07'
		],
		[
			'id' => 3,
			'layout_id' => 1,
			'site_id' => 1,
			'title' => 'Localización',
			'title_menu' => 'Localización',
			'parent_id' => 2,
			'content_id' => null,
			'level' => 1,
			'position' => 1,
			'body_class' => '',
			'action' => 'entry',
			'target_blank' => 0,
			'menu' => 'main',
			'is_homepage' => 0,
			'settings' => '',
			'created' => '2014-11-16 18:22:07',
			'modified' => '2014-11-16 18:22:07'
		],
		[
			'id' => 4,
			'layout_id' => 1,
			'site_id' => 1,
			'title' => 'Inicio',
			'title_menu' => 'Inicio',
			'parent_id' => 0,
			'content_id' => null,
			'level' => 1,
			'position' => 1,
			'body_class' => '',
			'action' => 'entry',
			'target_blank' => 0,
			'menu' => 'main',
			'is_homepage' => 1,
			'settings' => '',
			'created' => '2014-11-16 18:22:07',
			'modified' => '2014-11-16 18:22:07'
		],
		[
			'id' => 5,
			'layout_id' => 1,
			'site_id' => 1,
			'title' => 'Lugar',
			'title_menu' => 'Lugar',
			'parent_id' => 3,
			'content_id' => null,
			'level' => 1,
			'position' => 1,
			'body_class' => '',
			'action' => 'entry',
			'target_blank' => 0,
			'menu' => 'main',
			'is_homepage' => 1,
			'settings' => '',
			'created' => '2014-11-16 18:22:07',
			'modified' => '2014-11-16 18:22:07'
		],
	];

}
