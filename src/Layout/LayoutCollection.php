<?php
namespace Section\Layout;
use Section\Layout\Layout;
use Section\Layout\Wrapper;
use Cake\Core\Plugin;
use Cake\Core\App;
use Cake\Filesystem\Folder;
use Website\Lib\Website;
use Cake\ORM\TableRegistry;

/**
 * Se encarga de gestionar las acciones de las secciones
 */
class LayoutCollection
{
/**
 * Conjunto de objetos Layout disponibles en el script de la aplicación
 * @var array
 */
  protected static $_layouts;


  public static function build( $theme)
  {
    $path = App::path( 'Template/Layout', $theme);
    $Folder = new Folder( $path [0]);
    $files = $Folder->find( '.*json');
    foreach( $files as $file)
    {
      $json = file_get_contents( $path [0] . $file);
      $config = json_decode( $json, true);
      $key = str_replace( '.json', '', $file);

      $layout = new Layout( $key, $config);
      static::$_layouts [$key] = $layout;
    }
  }

  public static function get( $key = null)
  {
    if( !Website::get())
    {
      TableRegistry::get( 'Website.Sites')->setSite();
    }
    
    if( empty( static::$_layouts) && Website::get())
    {
      static::build( Website::get()->theme);
    }

    if( !$key)
    {
      return static::$_layouts;
    }

    return static::$_layouts [$key];
  }

}