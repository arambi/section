<?php

namespace Section\Layout;
use Section\Layout\Wrapper;
use Website\Lib\Website;

/**
 * Registrador de bloques para añadir y leer
 */

class Layout
{  
  public $key;

  public $name;

  public $wrappers = [];

  public function __construct( $key, array $config)
  {
    $this->key = $key;
    $this->name = $config ['name'];
    $wrappers = [];

    foreach( $config ['wrappers'] as $_key => $data)
    {
      $data ['key'] = $_key;
      $wrappers [$_key] = new Wrapper( $this, $data);
    }

    $this->wrappers = $wrappers;
  }

  public function wrapper( $key)
  {
    if( !isset( $this->wrappers [$key]))
    {
      throw new \RuntimeException( 'Cannot detect wrapper action for layout ' . $this->name);
    }

    return $this->wrappers [$key];
  }
}