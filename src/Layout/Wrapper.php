<?php

namespace Section\Layout;

use Cake\Core\Configure;
use Cake\Core\App;
use Block\Lib\BlocksRegistry;


/**
 * Registrador de bloques para añadir y leer
 */

class Wrapper
{
  public $layout;

  public $name;

  public $hasContent = false;

  public $defaultBlock = false;

  public $position = 'left';

  public $cols;

  public $blocks = [];


  public function __construct( Layout $layout, array $data)
  {
    // $this->layout = $layout;

    foreach( $data as $key => $value)
    {
      $this->$key = $value;
    }

    $this->buildBlocks();
  }

  public function buildBlocks()
  {
    $blocks = [];

    foreach( $this->blocks as $key)
    {
      $blocks [$key] = BlocksRegistry::get( $key);
    }

    $this->blocks = $blocks;
  }

}