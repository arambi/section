<?php

namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Links Model
 *
 * @method \Section\Model\Entity\Link get($primaryKey, $options = [])
 * @method \Section\Model\Entity\Link newEntity($data = null, array $options = [])
 * @method \Section\Model\Entity\Link[] newEntities(array $data, array $options = [])
 * @method \Section\Model\Entity\Link|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Section\Model\Entity\Link patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Section\Model\Entity\Link[] patchEntities($entities, array $data, array $options = [])
 * @method \Section\Model\Entity\Link findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LinksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('links');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // Behaviors
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.Sortable');
        $this->addBehavior('Upload.UploadJsonable', [
            'fields' => [
                'doc',
            ]
        ]);
        $this->addBehavior(Configure::read('I18n.behavior'), [
            'fields' => ['title', 'url']
        ]);

        $this->addBehavior('Cofree.Jsonable', [
            'fields' => ['settings']
        ]);
        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);



        $this->crud
            ->addFields([
                'title' => __d('app', 'Título'),
                'type' => [
                    'label' => 'Tipo de enlace',
                    'type' => 'select',
                    'options' => [
                        'section' => 'Una sección',
                        'url' => 'Una URL',
                        'doc' => 'Un fichero'
                    ]
                ],
                'target_blank' => [
                    'label' => 'Abrir en una ventana nueva',
                    'type' => 'boolean'
                ],
                'url' => [
                    'label' => __d('admin', 'Url'),
                    'type' => 'string',
                    'show' => "content.type == 'url' "
                ],
                'doc' => [
                    'type' => 'upload',
                    'label' => 'Fichero',
                    'config' => [
                        'type' => 'doc',
                    ],
                    'show' => "content.type == 'doc' "
                ],
                'section_id' => [
                    'label' => __d('admin', 'Enlace a sección'),
                    'type' => 'select',
                    'options' => function ($crud) {
                        return TableRegistry::get('Section.Sections')->selectOptions();
                    },
                    'show' => "content.type == 'section' "
                ],
            ])
            ->addIndex('index', [
                'fields' => [
                    'title',
                    'type',
                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Enlaces'),
                'plural' => __d('admin', 'Enlaces'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'key' => 'general',
                        'box' => [
                            [
                                'key' => 'general',
                                'elements' => [
                                    'title',
                                    'type',
                                    'url',
                                    'doc',
                                    'section_id',
                                    'target_blank'
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => []
            ], ['update']);

        if (Configure::read('Links.icons')) {
            $this->crud->addFields([
                'settings.icon' => [
                    'label' => __d('admin', 'Icono'),
                    'type' => 'select',
                    'options' => array_merge([0 => '-- Ninguno --'], Configure::read('Links.icons'))
                ]
            ]);

            $this->crud->addFieldToView(['create', 'update'], 'settings.icon', 'type');
        }
    }
}
