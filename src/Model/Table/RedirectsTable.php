<?php

namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Redirects Model
 *
 * @method \Section\Model\Entity\Redirect get($primaryKey, $options = [])
 * @method \Section\Model\Entity\Redirect newEntity($data = null, array $options = [])
 * @method \Section\Model\Entity\Redirect[] newEntities(array $data, array $options = [])
 * @method \Section\Model\Entity\Redirect|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Section\Model\Entity\Redirect saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Section\Model\Entity\Redirect patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Section\Model\Entity\Redirect[] patchEntities($entities, array $data, array $options = [])
 * @method \Section\Model\Entity\Redirect findOrCreate($search, callable $callback = null, $options = [])
 */
class RedirectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('redirects');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        // Behaviors
        $this->addBehavior('Manager.Crudable');

        // CRUD Config
        //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
        // $this->crud->associations([]);

        $this->crud
            ->addFields([
                'origin_url' => __d('admin', 'URL origen'),
                'redirect_url' => __d('admin', 'URL destino'),

            ])
            ->addIndex('index', [
                'fields' => [
                    'origin_url',
                    'redirect_url',

                ],
                'actionButtons' => ['create'],
                'saveButton' => false,
            ])
            ->setName([
                'singular' => __d('admin', 'Redirecciones'),
                'plural' => __d('admin', 'Redirecciones'),
            ])
            ->addView('create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'origin_url',
                                    'redirect_url',
                                ]
                            ]
                        ]
                    ]
                ],
                'actionButtons' => ['create', 'index']
            ], ['update']);
    }

    public function findFront(Query $query)
    {
        return $query;
    }
}
