<?php
namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Wrappers Model
 */
class WrappersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize( array $config)
    {
        $this->table( 'wrappers');
        $this->displayField( 'name');
        $this->primaryKey( 'id');
        
        // Behaviors
        $this->addBehavior( 'Timestamp');
        $this->addBehavior( 'Manager.Crudable');
        $this->addBehavior( 'Block.Blockable');
        $this->addBehavior( 'Cofree.Saltable');

        // CRUD Config
        $this->crud->associations( [ 'rows', 'blocks']);

        $this->crud
            ->addFields([
                'title' => 'Título',
                'body' => [
                    'label' => 'Cuerpo',
                ],
                'photo' => [
                    'type' => 'upload',
                    'label' => 'Foto',
                    'config' => [
                        'type' => 'post',
                        'size' => 'thm'
                    ]
                ],
            ])->addIndex( 'index', [
                'fields' => [
                    'title',
                ],
                'actionButtons' => ['create']
            ])->setName( [
                'singular' => __d( 'admin', 'Noticias'),
                'plural' => __d( 'admin', 'Noticias'),
            ])->addView( 'create', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'published',
                                    'published_at',
                                    'categories',
                                    'blocks'
                                ]
                            ]
                        ]
                    ]
                ]
            ])->addView( 'update', [
                'columns' => [
                    [
                        'cols' => 8,
                        'box' => [
                            [
                                'elements' => [
                                    'title',
                                    'published',
                                    'published_at',
                                    'categories',
                                    'blocks'
                                ]
                            ]
                        ]
                    ],
                ],
                'actionButtons' => ['create', 'index']
            ]);


        $this->belongsTo('Layouts', [
            'foreignKey' => 'layout_id',
            'className' => 'Section.Layouts'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator instance
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        // $validator
        //     ->add('id', 'valid', ['rule' => 'numeric'])
        //     ->allowEmpty('id', 'create')
        //     ->add('layout_id', 'valid', ['rule' => 'numeric'])
        //     ->requirePresence('layout_id', 'create')
        //     ->notEmpty('layout_id')
        //     ->requirePresence('name', 'create')
        //     ->notEmpty('name')
        //     ->allowEmpty('salt');

        return $validator;
    }
}
