<?php
namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Section\Model\Entity\Piece;
use Cake\Core\Configure;

/**
 * Pieces Model
 */
class PiecesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    parent::initialize($config);

    $this->table('pieces');
    $this->displayField('id');
    $this->primaryKey('id');

    $this->addBehavior('Timestamp');
    
    // Behaviors
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['body']
    ]);

    // CRUD Config
    //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
    // $this->crud->associations([]);

    $this->crud
      ->addFields([
        'body' => __d( 'admin', 'Cuerpo de texto'),
        'position' => __d( 'admin', 'Posición'),
        'file' => __d( 'admin', 'Archivo'),
        'folder' => __d( 'admin', 'Directorio'),
        
      ])
      ->addIndex( 'index', [
        'fields' => [
          'body',
          'position',
          'file',
          'folder',
        ],
        'actionButtons' => ['create'],
        'saveButton' => false,
      ])
      ->setName( [
        'singular' => __d( 'admin', 'Párrafos'),
        'plural' => __d( 'admin', 'Párrafos'),
      ])
      ->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'body',
                  'position',
                  'file',
                  'folder',
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['create', 'index']
      ], ['update'])
      ;
      
  }

  public function getPiece( $options)
  {
    $data = [
        'file' => $options ['file'],
        'folder' => $options ['folder'],
        'position' => $options ['position'],
    ];

    $content = $this->find() 
      ->where( $data)
      ->first();

    if( !$content)
    {
      $content = $this->newEntity( $data);
      $this->save( $content);
    }

    return $content;
  }
}
