<?php

namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Section\Action\ActionCollection;
use Cake\Collection\Collection;
use I18n\Lib\Lang;
use Cake\Routing\Router;
use Cake\Event\EventManager;
use Section\Routing\RouteData;
use Manager\Validation\TitleValidator;
use Cake\Datasource\EntityInterface;
use Cake\I18n\I18n;

/**
 * Sections Model
 */
class SectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        EventManager::instance()->attach($this);

        $this->setTable('sections');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');

        // Associations
        $this->belongsTo('Layouts', [
            'foreignKey' => 'layout_id',
            'className' => 'Section.Layouts'
        ]);

        if (Configure::read('User.hasUsers')) {
            $this->belongsToMany('Groups', [
                'className' => 'User.Groups',
                'joinTable' => 'sections_groups',
                'foreignKey' => 'section_id',
                'targetForeignKey' => 'group_id',
            ]);

            $this->belongsToMany('NoGroups', [
                'className' => 'User.Groups',
                'joinTable' => 'sections_nogroups',
                'foreignKey' => 'section_id',
                'targetForeignKey' => 'group_id',
            ]);
        }

        // Behaviors
        if (!$this->hasBehavior('I18nTranslate') && !$this->hasBehavior('Translate')) {
            $this->addBehavior(Configure::read('I18n.behavior'), [
                'fields' => [
                    'title',
                    'title_menu',
                    'subtitle_menu',
                    'external_url'
                ],
                'validator' => 'translated'
            ]);
        }
        
        $this->addBehavior('Manager.Crudable');
        $this->addBehavior('Manager.Drafter');

        if (!$this->hasBehavior('UploadJsonable')) {
            $this->addBehavior('Upload.UploadJsonable', [
                'fields' => [
                    'file'
                ]
            ]);
        }

        

        $this->addBehavior('Slug.Sluggable');
        $this->addBehavior('Cofree.Saltable');
        $this->addBehavior('Cofree.Jsonable', [
            'fields' => ['settings', 'action_layouts']
        ]);

        $this->addBehavior('Cofree.BooleanUnique', [
            'fields' => ['is_homepage']
        ]);

        $this->addBehavior('Block.Blockable', [
            'blocks' => [
                'text', 'gallery'
            ],
            'defaults' => [
                'text'
            ]
        ]);

        $this->addBehavior('Search.Searchable', [
            'fields' => [
                'title',
                'rows.columns.blocks.body',
                'rows.columns.blocks.title',
                'rows.columns.blocks.save_search',
            ]
        ]);

        // CRUD Config
        if (Configure::read('User.hasUsers')) {
            $this->crud->addAssociations(['Groups']);
            $this->crud->addAssociations(['NoGroups']);
        }

        $this->crud->defaults(function ($data) {
            $layout = $this->Layouts->find()
                ->where([
                    'Layouts.by_default' => 1
                ])
                ->first();

            if ($layout) {
                $data['layout_id'] = $layout->id;
            }

            return $data;
        });

        $fields = [
            'title' => __d('admin', 'Título'),
            'title_menu' => [
                'label' => __d('admin', 'Título del menú'),
                'show' => "data.content.action != 'external'"
            ],
            'subtitle_menu' => [
                'label' => __d('admin', 'Subtítulo del menu'),
                'show' => "data.menuType.hasSubtitle && data.content.action != 'external'"
            ],
            'anchor' => [
                'label' => __d('admin', 'Ancla'),
                'help' => __d('admin', 'Ancla de la página a donde irá al llegar a la sección'),
                'show' => "data.content.action != 'external' && administrator.superadmin && data.content.action != 'container'"
            ],
            'published' => __d('admin', 'Publicado'),
            'show_menu' => __d('admin', 'Mostrar en menú'),
            'show_submenu' => [
                'label' => __d('admin', 'Mostrar en menú lateral'),
                'show' => "!content.show_menu && data.content.action != 'external'"
            ],
            'is_homepage' => [
                'label' => __d('admin', 'Página principal'),
                'show' => "data.content.action != 'reference' && data.content.action != 'external' && data.content.action != 'container' && data.content.action != 'anchor'",
            ],
            'external_url' => [
                'label' => __d('admin', 'Url externa'),
                'show' => "data.content.action == 'external' && data.content.action != 'container' && data.content.action != 'anchor'",
            ],
            'show_children' => [
                'label' => __d('admin', 'Mostrar una sección hija'),
                'help' => __d('admin', 'Selecciona esta opción si quieres que no se muestre esta sección sino una sección hija'),
                'show' => "content.id && crudConfig.view.fields.show_children_id.options.length > 0 && data.content.action != 'external' && data.content.action != 'container'"
            ],
            'nolink' => [
                'label' => __d('admin', 'No tiene link'),
                'type' => 'boolean',
                'show' => "!content.show_children && crudConfig.view.fields.show_children_id.options.length > 0 && data.content.action != 'external'",
                'help' => __d('admin', 'Al seleccionar esta opción la sección no tendrá link sino que será un contenedor de subsecciones')
            ],
            'show_children_id' => [
                'label' => __d('admin', 'Sección hija'),
                'type' => 'select',
                'options' => function ($crud) {
                    $content = $crud->getContent();

                    if (!empty($content['id'])) {
                        return $this->find('list')
                            ->where([
                                'parent_id' => $content['id']
                            ]);
                    }
                },
                'show' => "content.show_children && data.content.action != 'external'"
            ],
            'settings.sections_categories_bool' => [
                'label' => __d('app', 'Usar opciones categorizadas'),
                'type' => 'boolean',
                'help' => __d('app', 'Utiliza opciones de categorización para esta sección'),
                'show' => "content.sectionType.sectionsCategories && data.content.action != 'external'"
            ],
            'settings.sections_categories' => [
                'type' => 'sectionsCategories',
                'template' => 'Section.fields/sections_categories',
                'show' => "content.settings.sections_categories_bool && data.content.action != 'external'"
            ],
            'settings.class_body' => [
                'label' => __d('app', 'Class CSS para la etiqueta body'),
                'type' => 'string',
                'help' => __d('app', 'Se le aplicará esta class a la etiqueta body en esta página'),
                'show' => "administrator.superadmin && data.content.action != 'external'",
            ],
            'target_blank' => [
                'label' => __d('admin', 'Se abre en una página nueva'),
                'show' => "data.content.action != 'container'"
            ],
            'layout_id' => [
                'label' => __d('admin', 'Plantilla'),
                'type' => 'select',
                'options' => function ($crud) {
                    return $this->Layouts->find('list');
                },
                'show' => "content.action != 'reference' && data.content.action != 'external' && data.content.action != 'reference' && data.content.action != 'container'"
            ],
            'file' => [
                'label' => __d('admin', 'Archivo'),
                'type' => 'uploads',
                'config' => [
                    'type' => 'doc',
                    'langs_multiple' => true,
                ],
                'show' => "content.action == 'file'"
            ],
            'reference_id' => [
                'label' => __d('admin', 'Mostrar esta sección'),
                'type' => 'select',
                'options' => function ($crud) {
                    $sections = $this->find('tree')
                        ->where([
                            'Sections.action NOT IN' => [
                                'reference',
                                'container',
                            ]
                        ])
                        ->order([
                            'Sections.title'
                        ])
                        ->toArray();
                    asort($sections);
                    return $sections;
                },
                'show' => "data.content.action == 'reference'",
            ],

            'has_action_layouts' => [
                'label' => __d('admin', 'Utilizar distintos layouts para otras vistas'),
                'show' => "content.sectionType.actionLayouts && data.content.action != 'external'"
            ],
            'action_layouts' => [
                'label' => __d('admin', 'Seleccionar layouts'),
                'template' => 'Section.fields.action_layouts',
                'options' => function ($crud) {
                    return $this->Layouts->find('list');
                },
                'show' => "content.has_action_layouts && data.content.action != 'external'"
            ],
            'content_id' => [
                'type' => 'hidden'
            ],
            'parent_id' => [
                'type' => 'hidden'
            ],
            'menu' => [
                'type' => 'hidden'
            ],
            'in_crumbs' => [
                'label' => __d('admin', 'Presente en las migas de pan'),
                'help' => __d('admin', 'Marca esta opción para que esta sección esté presente en las migas de pan'),
                'show' => "data.content.action != 'external'"
            ],
            'no_webmap' => [
                'label' => __d('admin', 'Ocultar en mapa web'),
                'help' => __d('admin', 'Marca esta opción para que esta sección no esté presente en el mapa web'),
                'show' => 'administrator.superadmin',
            ],
            'sitemap_exclude' => [
                'label' => __d('admin', 'Ocultar en sitemap'),
                'help' => __d('admin', 'Marca esta opción para que esta sección no esté presente en el sitemap para los buscadores'),
            ],
            'blocks' => [
                'label' => __d('admin', 'Contenido'),
                'type' => 'block',
                'template' => 'Block.fields.blocks',
                'show' => "data.content.action == 'entry' || data.content.sectionType.withBlocks",
                'adapter' => 'block'
            ],
            'cname' => [
                'label' => __d('admin', 'Clave para uso en programación'),
                'help' => __d('admin', 'Clave usada por el programador del web'),
                'show' => 'administrator.superadmin',
            ],
        ];

        $elements = [
            'action_type' => [
                'type' => 'string',
                'template' => 'Section/fields/action_type'
            ],
            'title',
            'title_menu',
            'subtitle_menu',
            'reference_id',
            'external_url',
            'layout_id',
            'has_action_layouts',
            'action_layouts',
            'settings.sections_categories_bool',
            'settings.sections_categories',
            'published',
            'show_menu',
            'show_submenu',
            'slugs' => [
                'show' => "content.action != 'reference' && data.content.action != 'external' && data.content.action != 'anchor'"
            ],
            'anchor',
            'file',
            'content_id',
            'is_homepage',
            'in_crumbs',
            'no_webmap',
            'sitemap_exclude',
            'show_children',
            'show_children_id',
            'settings.class_body',
            'cname',
            'nolink',
            'target_blank',
            'parent_id',
            'menu',
        ];


        if (Configure::read('User.hasUsers')) {
            $fields['restricted'] = [
                'label' => __d('admin', 'Acceso restringido'),
                'help' => __d('admin', 'Marca esta opción para que esta sección sea de acceso restrigido para algunos usuarios'),
                'show' => "data.content.action != 'container'"
            ];

            $fields['no_access_group'] = [
                'label' => __d('admin', 'Evitar acceso para algunos grupos'),
                'help' => __d('admin', 'Marca esta opción para que esta sección no sea de acceso para algunos grupos'),
                'show' => "data.content.action != 'container'"
            ];

            $fields['hide_without_permissions'] = [
                'label' => __d('admin', 'No mostrar en el menu sin permisos'),
                'help' => __d('admin', 'Marca esta opción si quieres ocultar la sección del menú cuando el usuario no tiene permisos'),
                'show' => "data.content.restricted || data.content.no_access_group"
            ];

            $fields['groups'] = [
                'label' => __d('admin', 'Grupos'),
                'type' => 'checkboxes',
                'show' => 'content.restricted'
            ];

            $fields['no_groups'] = [
                'label' => __d('admin', 'Grupos'),
                'type' => 'checkboxes',
                'show' => 'content.no_access_group'
            ];

            $elements[] = 'restricted';
            $elements[] = 'groups';
            $elements[] = 'no_access_group';
            $elements[] = 'no_groups';
            $elements[] = 'hide_without_permissions';
        }

        if (Configure::read('Section.withPassword')) {
            $fields['with_pwd'] = 'Acceso con contraseña';
            $fields['pwd'] = [
                'label' => __d('admin', 'Contraseña de acceso'),
                'show' => 'content.with_pwd',
            ];

            $elements[] = 'with_pwd';
            $elements[] = 'pwd';
        }

        $elements[] = 'blocks';

        $this->crud
            ->addJsFiles(['/section/js/sections.js'])
            ->addFields($fields)
            ->addIndex('index', [
                'fields' => [
                    'title',
                ],
                'saveButton' => false,
                'template' => 'Section/sections_index',
            ])

            ->setName([
                'singular' => __d('admin', 'Sección'),
                'plural' => __d('admin', 'Secciones'),
            ])

            ->addView('add', [
                'template' => 'Section/sections_add',
                'columns' => [
                    [
                        'cols' => 12,
                        'box' => [
                            [
                                'title' => null,
                                'elements' => []
                            ]
                        ],
                    ]
                ]
            ])

            ->addView('create', [
                'columns' => [
                    [
                        'key' => 'general',
                        'cols' => 12,
                        'title' => __d('admin', 'General'),
                        'box' => [
                            [
                                'key' => 'general',
                                'title' => null,
                                'elements' => $elements
                            ],
                        ],
                    ],
                ],
                'previewButton' => true,
                'template' => 'Section/sections_update',
                'actionButtons' => [
                    'index',
                    function ($crud) {
                        $content = $crud->getContent();
                        if (!$content || empty($content['id'])) {
                            return false;
                        }

                        $menus = collection(Configure::read('Section.menus'));
                        $menu = $menus->firstMatch(['key' => $content['menu']]);
                        return [
                            'label' => __d('admin', '+ sección hija', [$menu['title']]),
                            'url' => '/admin/section/sections/add/' . $menu['key'] . '/' . $content['id'],
                            'modal' => true,
                        ];
                    },
                    function ($crud) {
                        $content = $crud->getContent();
                        if (!$content || empty($content['id'])) {
                            return false;
                        }

                        $menus = collection(Configure::read('Section.menus'));
                        $menu = $menus->firstMatch(['key' => $content['menu']]);
                        return [
                            'label' => __d('admin', '+ sección hermana', [$menu['title']]),
                            'url' => '/admin/section/sections/add/' . $menu['key'] . '/' . $content['parent_id'],
                            'modal' => true,
                        ];
                    }
                ],
            ], ['update']);

        $this->crud->defaults([
            'show_menu' => true,
            'published' => true,
            'in_crumbs' => true
        ]);

        $this->addBehavior('Seo.Seo');
    }



    public function validationDefault(Validator $validator)
    {
        $validator->allowEmpty('is_homepage');
        $validator->add('is_homepage', 'custom', [
            'rule' => function ($value, $context) {
                if (!$value && !empty($context['data']['id'])) {
                    return $this->exists([
                        'id !=' => $context['data']['id'],
                        'is_homepage' => true,
                    ]);
                }

                return true;
            },
            'message' => __d('admin', 'No es posible que el web no tenga página principal')
        ]);

        return $validator;
    }

    public function validationTranslated(Validator $validator)
    {
        return $validator;
        // return new TitleValidator();
    }

    /**
     * Genera la propiedad "sectionType" con la información del tipo de action
     * @param  Event  $event 
     * @param  Query  $query 
     * @return void
     */
    public function beforeFind(Event $event, Query $query)
    {
        $map = function ($row, $key, $mapReduce) {

            if (!empty($row->action)) {
                $sectionType = ActionCollection::get($row->action);
                $row->set('sectionType', $sectionType);
            }

            $mapReduce->emit($row, $key);
        };

        $query->mapReduce($map, function () {
        });
    }

    public function beforeDelete(Event $event, EntityInterface $entity)
    {
        if ($entity->is_homepage) {
            $this->setBeforeDeleteError($entity, [
                'message' => __d('admin', 'No se puede borrar la sección que es la página principal')
            ]);

            return false;
        }

        return true;
    }

    public function findFront(Query $query, array $options)
    {
        $event = new Event('Section.Model.Table.Sections.findFront', $this, [
            $query
        ]);

        EventManager::instance()->dispatch($event);

        return $query;
    }

    public function findNav(Query $query, $options = [])
    {
        $query->find('front');
        $query->find('threaded');
        $query->formatResults(function ($results) {
            return $this->_rowMapper($results);
        });

        if (Configure::read('User.hasUsers')) {
            $query->contain('Groups');
            $query->contain('NoGroups');
        }

        return $query;
    }

    protected function _rowMapper($results)
    {
        return $results->map(function ($row) {
            if (!empty($row->children)) {
                $row->set('children', $this->_mapChildrens($row->children));
            }

            if ($row && $row->has('sectionType') && $row->sectionType->childrensTable) {
                $data = $row->sectionType->childrensTable;
                $table = TableRegistry::getTableLocator()->get($data['model']);
                $finder = 'all';

                if ($table->hasFinder('sections')) {
                    $finder = 'sections';
                }

                $contents = $table->find($finder, [
                    'section' => $row
                ]);



                $children = $row->children;
                $childrensTable = $this->_transformEntities($contents, $row, $table);

                $position = !empty($data['position']) ? $data['position'] : 'end';

                if ($position == 'start') {
                    $children = array_merge($childrensTable, (array)$children);
                } else {
                    $children = array_merge((array)$children, $childrensTable);
                }

                $row->set('children', $children);
            }

            return $row;
        });
    }

    protected function _mapChildrens($childrens)
    {
        foreach ($childrens as $row) {
            if ($row && $row->has('sectionType') && $row->sectionType->childrensTable) {
                $data = $row->sectionType->childrensTable;
                $table = TableRegistry::get($data['model']);
                $finder = 'all';

                if ($table->hasFinder('sections')) {
                    $finder = 'sections';
                }

                $contents = $table->find($finder);

                if (!empty($row->children)) {
                    $row->set('children', $this->_mapChildrens($row->children));
                }

                $children = $row->children;
                $childrensTable = $this->_transformEntities($contents, $row, $table);

                $children = array_merge((array)$children, $childrensTable);
                $row->set('children', $children);
            }
        }

        return $childrens;
    }

    protected function _transformEntities($contents, $row, $table, $slug = '')
    {
        $return = [];

        foreach ($contents as $content) {
            $data = [
                'title' => $content->{$table->displayField()},
                'slug' => $content->slug,
                'content_slug' => $content->content_slug ? $content->content_slug : $content->slug,
                'url_slug' => $slug . '/' . $content->slug,
                'id' => $content->id,
                'sectionType' => $row->sectionType,
                'anchor' => $content->anchor,
                'in_crumbs' => true,
                'nolink' => $content->has('nolink') ? $content->nolink : false,
                'children' => $content->has('children') ? $this->_transformEntities($content->children, $row, $table, $slug . '/' . $content->slug) : [],
                'is_current' => $content->has('is_current') ? $content->is_current : false,
                'is_content' => true
            ];

            if ($content->slugs) {
                $data['slugs'] = $content->slugs;
            }

            if (!empty($content->content_type)) {
                $data['content_type'] = $content->content_type;
            }

            $entity = TableRegistry::getTableLocator()->get($content->getSource())->newEntity($data);
            // $entity = $this->newEntity( $data);

            $return[] = $entity;
        }

        return $return;
    }

    /**
     * Devuelve la section pedida
     * Es usada para tomar una section y ser usada en entorno de Request
     *   
     * @param  integer $id
     * @return Entity
     */
    public function findSection($id)
    {
        $query = $this->find('front')
            ->contain([
                'Rows' => [
                    'Columns' => [
                        'Blocks'
                    ]
                ],
            ])
            ->formatResults(function ($results) {
                return $this->_rowMapper($results);
            })
            ->where(['Sections.id' => $id]);

        if (Configure::read('User.hasUsers')) {
            $query->contain('Groups');
            $query->contain('NoGroups');
        }

        $section = $query->first();

        return $section;
    }

    public function getByAction($action)
    {
        $section = $this->find()
            ->where([
                'Sections.action' => $action
            ])
            ->first();

        return $section;
    }

    public function cnameUrl($cname, $locale = null)
    {
        if ($locale) {
            $currentLang = I18n::getLocale();
            I18n::setLocale($locale);
        }

        $section = $this->findByCname($cname)->first();

        if (!$section) {
            return false;
        }

        $url = $this->getUrl($section);

        if ($locale) {
            I18n::setLocale($currentLang);
        }

        return $url;
    }

    public function getUrl($section)
    {
        if (is_numeric($section)) {
            $section = $this->find()
                ->where([
                    'Sections.id' => $section
                ])
                ->first();

            if (!$section) {
                return null;
            }
        }

        if ($section->action == 'external') {
            return $section->external_url;
        }

        $path = $this->getPath($section);

        foreach ($path as $section) {
            $return[] = $section->slug;
        }

        if (!Configure::read('I18n.disable')) {
            $lang_url = Lang::current() . '/';
        } else {
            $lang_url = '';
        }

        if ($section->is_homepage) {
            return '/' . $lang_url;
        }

        return '/' . $lang_url . implode('/', $return);
    }

    public function getPreviewUrl($content)
    {
        return Router::url($this->getUrl($content->id), true);
    }

    /**
     * Devuelve los parámetros de la URL a partir de la entity
     * 
     * @param  Section\Model\Entity\Section     $section
     * @return array  Url
     */
    public function paramsUrl($url)
    {
        $section = RouteData::getSection($url);

        if (!$section) {
            return '';
        }

        if ($section && is_object($section['section'])) {
            $route = $url;

            if (empty($section['section']->sectionType->actions)) {
                $route['section_id'] = $section['section']->id;
            }

            if (property_exists($section['section']->sectionType, 'sectionsCategories') && !empty($section['section']->settings) && isset($section['section']->settings->sections_categories)) {
                $route['section_categories'] = $section['section']->settings->sections_categories;
            }

            if (!Configure::read('I18n.disable')) {
                $route['locale'] = Lang::current('iso3');
                $route['lang'] = Lang::current('iso2');
            }
            return Router::url($route);
        }

        if (!Configure::read('I18n.disable')) {
            $url['locale'] = Lang::current('iso3');
            $url['lang'] = Lang::current('iso2');
        }

        $params = [
            'plugin' => $section->sectionType->plugin,
            'controller' => $section->sectionType->controller,
            'action' => $section->sectionType->action,
        ];

        if (empty($section->sectionType->actions)) {
            $params['section_id'] = $section->id;
        }

        return $params;
    }

    /**
     * Devuelve un path de una section dada o un id
     * 
     * @param  Section\Model\Entity\Section | numneric $section La entidad de una sección o un id
     * @param  array  $path EL path guardado hasta la llamada al método 
     * @return array Matriz de secciones
     */
    public function getPath($section, $path = [], $slug = null)
    {
        if (is_numeric($section)) {
            $section = $this->find()
                ->where([
                    'Sections.id' => $section
                ])
                ->formatResults(function ($results) {
                    return $this->_rowMapper($results);
                })
                ->first();

            if (!$section) {
                return null;
            }
        }

        if (!$section) {
            return [];
        }

        if (empty($path)) {
            $path[] = $section;
        }

        $new = $this->find()
            ->where([
                'Sections.id' => $section->parent_id
            ])
            ->formatResults(function ($results) {
                return $this->_rowMapper($results);
            })
            ->first();

        if ($new) {
            $path[] = $new;
        } else {
            if ($slug && is_object($section) && isset($section->sectionType->childrensTable)) {
                $children_path = $this->__getChildrenPath($section, $slug);

                if (!empty($children_path)) {
                    $path = array_merge(array_reverse($children_path), $path);
                }
            }
            return array_reverse($path);
        }

        return $this->getPath($new, $path);
    }

    private function __getChildrenPath($section, $slug, $path = [])
    {
        foreach ($section->children as $children) {
            $_path = array_merge($path, [$children]);

            if ($children->content_slug == $slug) {
                return $_path;
            }

            if (!empty($children->children)) {
                $paths = $this->__getChildrenPath($children, $slug, $_path);

                if (!empty($paths)) {
                    return $paths;
                }
            }
        }

        return null;
    }

    /**
     * Devuelve un submenu del árbol de contenidos dada una section
     * Se debe indicar el nivel de submenu que se quiere conseguir
     * Por ejemplo, en unas migas de pan tipo Empresa > Nosotros > Departamentos  Si se pasa la section Departamentos y se indica nivel 0, 
     *   nos devolverá las secciones hijas de Empresa
     *     
     * @param  Section\Model\Entity\Section $section
     * @param  integer $level
     * @return array
     */
    public function getSubNav($section, $level)
    {
        $path = $this->getPath($section);

        $sections = $this->find('nav')
            ->where([
                'Sections.published' => 1,
                'OR' => [
                    'Sections.show_menu' => 1,
                    'Sections.show_submenu' => 1,
                ]
            ])
            ->order('position');


        if (!isset($path[$level])) {
            return;
        }

        if ($level > 0) {
            for ($i = 0; $i <= $level; $i++) {
                if (isset($path[$i])) {
                    $section = $path[$i];
                    $sections = $sections->firstMatch([
                        'id' => $section->id
                    ])->children;

                    if (empty($sections)) {
                        return [];
                    }

                    $sections = (new Collection($sections));
                }
            }


            return $sections->toArray();
        } else {
            $section = $path[$level];
            $section_return = $sections->firstMatch([
                'id' => $section->id
            ]);

            if ($section_return) {
                return $section_return->children;
            }
        }
    }

    /**
     * Toma todas las secciones agrupadas por claves de menú
     *
     * @return array
     */
    public function getSections()
    {
        $sections = [];

        $menus = Configure::read('Section.menus');

        foreach ($menus as $key => $menu) {
            if ($menu['key'] == 'none') {
                $key = 'none';
            }
            $sections[$key] = [];

            $data = $this->find('threaded')
                ->where(['menu' => $menu['key']])
                ->order('position')
                ->toArray();

            $data = $this->__buildForJson($data);
            $sections[$key] = [
                'menu' => [
                    'key' => $menu['key'],
                    'title' => $menu['title']
                ],
                'contents' => $data
            ];
        }

        return $sections;
    }

    public function getByMenu($menu, $finder = 'nav', $callback = null)
    {
        if ($this->hasBehavior('Blockable')) {
            $this->removeBehavior('Blockable');
        }

        $query = $this->find($finder)
            ->where([
                'Sections.menu' => $menu,
                'Sections.published' => 1,
                'Sections.show_menu' => 1,
            ])
            ->order('position');

        if (Configure::read('User.hasUsers')) {
            $query->contain([
                'Groups',
                'NoGroups',
            ]);
        }

        if ($callback) {
            $callback($query);
        }

        $data = $query->toArray();

        return $data;
    }


    public function selectOptions()
    {
        $sections = $this->find('list')
            ->where([
                'Sections.action NOT IN' => ['reference', 'container', 'anchor']
            ])
            ->order([
                'Sections.title'
            ]);
        return $sections;
    }


    /**
     * Guarda el array de las secciones para ser usado por un js sortable
     * Utiliza la clave 'items' para las secciones hijo
     *
     * @param array $datas 
     * @return array
     */
    private function __buildForJson($datas)
    {
        foreach ($datas as $data) {
            if (!empty($data->children)) {
                $data->set('nodes', $this->__buildForJson($data->children));
                $data->unsetProperty('children');
            } else {
                $data->nodes = [];
            }
        }

        return $datas;
    }

    public function reordering($data)
    {
        foreach ($data as $menu => $_data) {
            $this->__reordering($_data['contents'], $_data['menu']['key']);
        }
    }

    private function __reordering($data, $menu, $parent_id = 0, $position = 1)
    {
        foreach ($data as $section) {
            $content = $this->get($section['id']);
            $content->set('position', $position)
                ->set('menu', $menu)
                ->set('parent_id', $parent_id);
            $this->save($content);

            $position++;

            if (!empty($section['nodes'])) {
                $this->__reordering($section['nodes'], $menu, $section['id']);
            }
        }
    }

    public function beforeDuplicate($event, $content)
    {
        $content->set('is_homepage', 0);
    }

    public function beforeSaveDraft($event, $original, $draft)
    {
        if ($original->is_homepage == 1) {
            $draft->set('is_homepage', 1);
        }
    }

    /**
     * Initialize para trabajar con links
     */
    public function initializeLinks()
    {
        $this->hasMany('Links', [
            'className' => 'Section.Links',
            'foreignKey' => 'foreign_key',
            'dependent' => true
        ]);

        $this->crud->addAssociations(['Links']);
    }

    /**
     * Build Crud para trabajar con links
     */
    public function buildLinks()
    {
        $this->crud->addFields([
            'links' => [
                'type' => 'hasMany',
                'label' => __d('admin', 'Enlaces'),
            ],
        ]);

        $this->crud->addBoxToColumn(['update', 'create'], 'general', [
            'title' => __d('admin', 'Enlaces'),
            'elements' => [
                'links',
            ]
        ]);
    }

    public function implementedEvents()
    {
        $eventMap = parent::implementedEvents();
        $eventMap['Manager.Behavior.Crudable.Sections.beforeDuplicate'] = 'beforeDuplicate';
        $eventMap['Manager.Behavior.Drafter.Sections.beforeSaveDraft'] = 'beforeSaveDraft';

        return $eventMap;
    }
}
