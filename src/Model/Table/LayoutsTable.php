<?php
namespace Section\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Manager\Crud\Flash;

use Section\Layout\LayoutCollection;

/**
 * Layouts Model
 */
class LayoutsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'layouts');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');

    $this->addBehavior( 'Cofree.BooleanUnique', [
      'fields' => [ 'by_default']
    ]);
    
    $this->hasMany( 'Wrappers', [
        'foreignKey' => 'layout_id',
        'className' => 'Section.Wrappers'
    ]);
    
    $this->hasMany( 'Sections', [
        'foreignKey' => 'layout_id',
        'className' => 'Section.Sections'
    ]);

    $this->crud->associations( ['Wrappers', 'Rows', 'Blocks', 'Columns']);

    // CRUD Config
    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Título'),
        'by_default' => __d( 'admin', 'Plantilla por defecto'),
        'blocks' => [
          'label' => __d( 'admin', 'Contenido'),
          'type' => 'block',
          'template' => 'Block.fields.blocks',
          'show' => "data.content.action == 'entry'",
          'adapter' => 'block'
        ],
      ])

      ->addIndex( 'index', [
        'fields' => [
          'title',
          'by_default'
        ],
        'saveButton' => false,
        'actionButtons' => [
          [
            'url' => 'add',
            'label' => __d( 'admin', 'Nuevo')
          ]
        ]
      ])

      ->setName( [
        'singular' => __d( 'admin', 'Plantilla'),
        'plural' => __d( 'admin', 'Plantillas'),
      ])

      ->addView( 'add', [
        'template' => 'Section/layouts_add',
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [

                ]
              ]
            ],  
          ]
        ]
      ])
      ->addJsFiles([
        '/block/js/directives.js',
        '/block/js/services.js'
      ])
      ->addView( 'create', [
        'template' => 'Section/layouts_create',
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                   'title',
                   'by_default'
                ]
              ]
            ],  
          ]
        ]
      ])
      ->addView( 'update', [
        'template' => 'Section/layouts_update',
        'columns' => [
          [
            'cols' => 12,
            'box' => [
              [
                'title' => null,
                'elements' => [
                  'title',
                  'by_default'
                ]
              ]
            ],
          ]
        ],
        'actionButtons' => ['index', [
            'url' => 'add',
            'label' => __d( 'admin', 'Nuevo')
        ]]
      ])
    ;

    // Añade la clave "layouts" a la configuración del crud, para que se puedan crear los tipos de bloque para cada wrapper del layout
    $layouts = LayoutCollection::get();
    
    $this->crud->addConfig( ['layouts' => $layouts]);
  }

/**
 * Hace varias comprobaciones antes de eliminar un layout
 * 
 * @param  Event  $event 
 * @param  Entity $entity
 * @return boolean
 */
  public function beforeDelete( Event $event, Entity $entity)
  {
    if( $this->checkOnceLayout())
    {
      Flash::error( __d( 'admin', 'No es posible eliminar el único layout que hay disponible'));
      return false;
    }

    if( $this->usedBySections( $entity))
    {
      Flash::error( __d( 'admin', 'El layout está siendo usado por alguna sección y no es posible borrarlo'));
      return false;
    }
    
    return true;
  }

/**
 * Comprueba que haya un sólo layout disponible
 * 
 * @return boolean
 */
  public function checkOnceLayout()
  {
    $count = $this->find( 'all')->count();
    return $count == 1;
  }

/**
 * Comprueba que el layout esté siendo usado por alguna sección
 * 
 * @param  Entity $entity
 * @return boolean
 */
  public function usedBySections( Entity $entity)
  {
    $sectionCount = $this->Sections->find( 'all')
      ->where([
        'Sections.layout_id' => $entity->id
      ])
      ->count();

    return $sectionCount > 0;
  }


/**
 * Busca un layout dado un id
 * 
 * @param  integer $id
 * @return Entity
 */
  public function findLayout( $id)
  {
    $layout = $this->find()
      ->contain([
        'Wrappers' => [
            'Rows' => [
              'Columns' => [
                'Blocks'
              ]
            ]
          ]
      ])
      ->where(['Layouts.id' => $id])
      ->first();

    return $layout;
  }

/**
 * Busca el layout por defecto
 * 
 * @return Entity
 */
  public function findDefault()
  {
    $layout = $this->find()
      ->contain([
        'Wrappers' => [
            'Rows' => [
              'Columns' => [
                'Blocks'
              ]
            ]
          ]
      ])
      ->where(['Layouts.by_default' => 1])
      ->first();

    return $layout;
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator instance
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
      // $validator
      //     ->add('id', 'valid', ['rule' => 'numeric'])
      //     ->allowEmpty('id', 'create')
      //     ->add('site_id', 'valid', ['rule' => 'numeric'])
      //     ->allowEmpty('site_id')
      //     ->requirePresence('template', 'create')
      //     ->notEmpty('template')
      //     ->requirePresence('title', 'create')
      //     ->notEmpty('title')
      //     ->allowEmpty('salt');

      return $validator;
  }
}
