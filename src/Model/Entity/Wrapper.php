<?php
namespace Section\Model\Entity;

use Cake\ORM\Entity;
use Block\Model\Entity\BlockTrait;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Wrapper Entity.
 */
class Wrapper extends Entity
{
	use BlockTrait;
  use CrudEntityTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      'layout_id' => true,
      'cname' => true,
      'salt' => true,
      'layout' => true,
      'rows' => true,
  ];
}
