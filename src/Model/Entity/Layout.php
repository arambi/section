<?php
namespace Section\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Layout Entity.
 */
class Layout extends Entity
{

  use CrudEntityTrait;


  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
      'id' => true,
      'site_id' => true,
      'by_default' => true,
      'template' => true,
      'title' => true,
      'salt' => true,
      'site' => true,
      'wrappers' => true,
  ];
}
