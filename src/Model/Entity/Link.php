<?php
namespace Section\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Link Entity
 *
 * @property int $id
 * @property string $model
 * @property string $foreign_key
 * @property string $section_id
 * @property string $doc
 * @property int $position
 * @property string $type
 * @property string $url
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Link extends Entity
{
  use CrudEntityTrait;
  use TranslateTrait;

  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * Note that when '*' is set to true, this allows all unspecified fields to
   * be mass assigned. For security purposes, it is advised to set '*' to false
   * (or remove it), and explicitly make individual fields accessible as needed.
   *
   * @var array
   */
  protected $_accessible = [
      '*' => true,
      'position' => false
  ];

  protected function _getLink()
  {
    $url = null;

    if( $this->type == 'section')
    {
      $url = TableRegistry::get( 'Section.Sections')->getUrl( $this->section_id);
    }
    elseif( $this->type == 'url')
    {
      $url = $this->url;
    }
    elseif( $this->type == 'doc')
    {
      $url = $this->doc->paths [0];
    }

    return $url;
  }
}
