<?php

namespace Section\Model\Entity;

use Cake\ORM\Entity;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Event\EventManager;
use Slug\Model\Entity\SlugTrait;
use Block\Model\Entity\BlockTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Search\Model\Entity\SearchEntityTrait;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Section Entity.
 */
class Section extends Entity
{
    use CrudEntityTrait;
    use TranslateTrait;
    use SlugTrait;
    use BlockTrait;
    use SearchEntityTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
        'site_id' => true,
        'title' => true,
        'title_menu' => true,
        'parent_id' => true,
        'layout_id' => true,
        'content_id' => true,
        'body_class' => true,
        'action' => true,
        'target_blank' => true,
        'menu' => true,
        'is_homepage' => true,
        'settings' => true,
        'site' => true,
        'parent_section' => true,
        'content' => true,
        'published' => true,
        'child_sections' => true,
        'rows' => true,
        'slugs' => true,
        'action_layouts' => true,
        'has_action_layouts' => true,
        'in_crumbs' => true,
        'links' => true
    ];

    public function __construct(array $properties = [], array $options = [])
    {
        parent::__construct($properties, $options);
        $event = new Event('Section.Model.Entity.Section.construct', $this);
        EventManager::instance()->dispatch($event);
    }


    public function menuTitle()
    {
        if (!empty($this->title_menu)) {
            return $this->title_menu;
        }

        return $this->title;
    }

    protected function _getBody()
    {
        if (isset($this->rows[0]->columns[0]->blocks[0]) && $this->rows[0]->columns[0]->blocks[0]->subtype == 'text') {
            return $this->rows[0]->columns[0]->blocks[0]->body;
        }
    }

    public function link()
    {
        return TableRegistry::get('Section.Sections')->getUrl($this);
    }

    public function saveSearch($locale)
    {
        $out = [];

        foreach ($this->rows as $row) {
            foreach ($row->columns as $column) {
                foreach ($column->blocks as $block) {
                    $text = $block->saveSearch($locale);

                    if ($text) {
                        $out[] = $text;
                    }
                }
            }
        }

        return implode(' ', $out);
    }

    protected function _getLevel($value)
    {
        if ($value !== null) {
            return $value;
        }

        $path = TableRegistry::getTableLocator()->get('Section.Sections')->getPath($this);
        $value = count($path) - 1;
        $this->set('level', $value);
        return $value;
    }
}
