<?php
namespace Section\Model\Entity;

use Cake\ORM\Entity;

/**
 * LayoutsWrapper Entity.
 */
class LayoutsWrapper extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'layout_id' => true,
        'wrapper_id' => true,
    ];
}
