<?php
namespace Section\Model\Entity;

use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;use Cake\ORM\Entity;

/**
 * Redirect Entity
 *
 * @property int $id
 * @property string|null $origin_url
 * @property string|null $redirect_url
 */
class Redirect extends Entity
{
    use CrudEntityTrait;
    use SlugTrait;
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'slugs' => true,
    ];
}
