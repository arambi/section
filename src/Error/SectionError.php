<?php 

namespace Section\Error;

use Cake\Error\BaseErrorHandler;

class SectionError extends BaseErrorHandler
{

    public function handleException(Exception $exception)
    {
      die( 'Hola');
    }

    public function _displayError($error, $debug)
    {
      return 'There has been an error!';
    }
    public function _displayException($exception)
    {
      _d( '_displayException');
      return 'There has been an exception!';
    }
}
