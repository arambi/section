<?php 

namespace Section\Seo;

use Cake\Datasource\ModelAwareTrait;
use Cake\I18n\I18n;
use Cake\Routing\Router;
use I18n\Lib\Lang;

class Sitemap
{
  use ModelAwareTrait;

  private $nodes = [];

  private $lang;

  private function getLangs()
  {
    return Lang::get( true);
  }

  public function create()
  {
    $langs = $this->getLangs();
    $this->loadModel( 'Section.Sections');

    foreach( $langs as $lang)
    {
      $this->setLocale( $lang);
      Router::reload();

      $sections = $this->Sections->find()
        ->where([
          'Sections.published' => true,
          'Sections.show_menu' => true,
          'Sections.sitemap_exclude' => false,
          'Sections.action NOT IN' => [
            'external',
            'container',
            'reference',
          ],
          'Sections.is_homepage' => false
        ]);
      
      foreach( $sections as $section)
      {
        $this->nodes ['Sections'][$section->id][$this->lang] = [
          'url' => Router::url( $this->Sections->getUrl( $section), true),
          'date' => $section->modified->format( 'Y-m-d')
        ];

        if( !empty( $section->sectionType->sitemap))
        {
          $this->getFromModel( $section);
        }
      }
    }

    return $this->nodes;
  }
//  
  public function getDisallows()
  {
    $langs = $this->getLangs();
    $this->loadModel( 'Section.Sections');
    $return = [];
    $internal_urls = [
      '/user/users/login',
      '/user/users/logout',
    ];

    foreach( $langs as $lang)
    {
      $this->setLocale( $lang);
      Router::reload();

      $sections = $this->Sections->find()
        ->where([
          'Sections.published' => true,
          'Sections.sitemap_exclude' => true,
        ]);
      
      foreach( $sections as $section)
      {
        $return [] = Router::url( $this->Sections->getUrl( $section));
      }

      foreach( $internal_urls as $url)
      {
        $return [] = '/'. $lang->iso2 . $url;
      }
    }

    return $return;
  }

  private function setLocale( $lang)
  {
    I18n::setLocale( $lang->iso3);
    $this->lang = $lang->iso2;
  }

  private function defaultCallback()
  {
    return function( $content){
      $url = $content->link();
      $date = date( 'Y-m-d', $content->modified->toUnixString());
      return [
        'url' => $url,
        'date' => $date
      ];
    };
  }

  private function getFromModel( $section)
  {
    $model = $section->sectionType->sitemap ['model'];
    $table = $this->loadModel( $model);

    if( method_exists( $table, 'sitemap'))
    {
      $dynamic = $table->sitemap();
      $queryDefault = $dynamic ['query'];
      $callback = $dynamic ['callback'];
    }
    elseif( $table->hasFinder( 'front'))
    {
      $queryDefault = $table->find( 'front');
      $callback = $this->defaultCallback();
    }
    else
    {
      return;
    }

    $limit = 100;
    $offset = 0;
    
    while( true)
    {
      $query = clone $queryDefault;
      $query 
        ->limit( $limit)
        ->offset( $offset)
        ->order([
          $table->alias() . '.'. $table->primaryKey()
        ]);

      $contents = $query->toArray();

      if( count( $contents) == 0)
      {
        break;
      }

      foreach( $contents as $content)
      {
        $this->nodes [$model][$content->id][$this->lang] = $callback( $content);
      }

      $offset = $offset + $limit;
    }
  }
}