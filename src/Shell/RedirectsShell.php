<?php
namespace Section\Shell;

use Cake\Console\Shell;

/**
 * Redirects shell command.
 */
class RedirectsShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $table = $this->getTableLocator()->get('Section.Redirects');
        $file = TMP . 'redirects.csv';
        $contents = file_get_contents($file);
        $rows = explode("\n", $contents);

        foreach ($rows as $row) {
            $cols = explode(';', $row);
            $redirect_url = $this->removeHost($cols[1]);
            $origin_url = $this->removeHost($cols[0]);
            $entity = $this->getEntity($origin_url, $redirect_url);
            $table->save($entity);
        }
    }

    private function getEntity($origin_url, $redirect_url)
    {
        $table = $this->getTableLocator()->get('Section.Redirects');

        $entity = $table->find()
            ->where([
                'origin_url' => $origin_url,
            ])
            ->first();

        if (!$entity) {
            $entity = $table->newEntity([
                'origin_url' => $origin_url,
                'redirect_url' => $redirect_url,
            ]);
        } else {
            $entity = $table->patchEntity($entity, [
                'redirect_url' => $redirect_url,
            ]);
        }
        
        return $entity;
    }

    private function removeHost($url)
    {
        $url = trim($url);
        $path = parse_url($url, PHP_URL_PATH);
        $query = parse_url($url, PHP_URL_QUERY);

        if (!empty($query)) {
            $path .= "?$query";
        }

        return $path;
    }
}
