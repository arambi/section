<?php
namespace Section\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\Filesystem\Folder;

/**
 * Realiza la instalación de la aplicación
 */
class ConfiguratorShell extends Shell
{

/**
 * Crea el fichero de configuración por defecto del plugin
 * 
 * @return void
 */
  public function main()
  {
    $data = "<?php
      \$config ['Section']['menus'] = array(
          [
            'key' => 'main',
            'title' => __d( 'admin', 'Principal'),
          ],
          [
            'key' => 'bottom',
            'title' => __d( 'admin', 'Inferior'),
          ],
          [
            'key' => 'none',
            'title' => __d( 'admin', 'Ninguno'),
          ],
      );
    ";

    // Creación de ficheros de configuración
    $this->writefile( $data );
  }

  public function writefile( $data )
  {
    $file = fopen( "config/section.php", "wb");
    fwrite( $file, $data);
    fclose( $file);
    $this->out( 'Configuración para el plugin Section creada');
  }

}