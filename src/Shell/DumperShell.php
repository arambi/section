<?php
namespace Section\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use I18n\Lib\Lang;

/**
 * Dumper shell command.
 */
class DumperShell extends Shell
{

  public $tasks = ['Cofree.Installer'];

  /**
   * main() method.
   *
   * @return bool|int Success or error code.
   */
  public function main() 
  {
    $this->createLayouts();
    
    $this->__pages();
  }

  public function createLayouts()
  {
    $data = [
      'template' => 'default',
      'title' => 'Por defecto',
      'by_default' => 1,
      'wrappers' => [
        [
          'cname' => 'main',
          'title' => 'Principal',
          'rows' => [
            [
              'columns' => [
                [
                  'blocks' => [
                    [
                      'subtype' => 'content'
                    ],
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ];

    $Layouts = TableRegistry::get( 'Section.Layouts');
    $layout = $Layouts->getNewEntity( $data);

    if( $Layouts->saveContent( $layout))
    {
      $this->Installer->out( 'El layout se ha creado correctamente');
    }
  }

  private function __pages()
  {
    $this->__savePage( 'legal');
    $this->__savePage( 'cookies');
    $this->__savePage( 'home');
  }

  public function pages()
  {
    $this->__pages();
  }

  private function __savePage( $page)
  {
    $this->Sections = TableRegistry::get( 'Section.Sections');

    $locales = Lang::iso3();
    $path = Plugin::path( 'Section') . 'src' .DS. 'Shell' .DS. 'Data' .DS;
    $translations = [];

    foreach( $locales as $key => $name)
    {
      $filepath = $path . $key . '/'. $page . '.ctp';

      if( !file_exists( $filepath))
      {
        $filepath = $path . 'spa/'. $page . '.ctp';
      }

      if( file_exists( $filepath))
      {
        $translations [$key]['body'] = file_get_contents( $filepath);
      }
    }

    $data = $this->Installer->getData( 'Section', $page, [
      'translations' => json_encode( $translations)
    ]);

    $entity = $this->Sections->getNewEntity( $data);

    if( $this->Sections->saveContent( $entity))
    {
      $this->Installer->out( 'La página '. $page .' se ha creado correctamente');
    }
    else
    {

    }
  }

}