<p>
  &nbsp;XXXXX (en adelante XXXX) desea informarle que esta página web emplea cookies propias y de terceros para mejorar la experiencia del usuario mediante el análisis de sus hábitos de navegación. Además, compartimos la información sobre el uso que hace de nuestra web con Google, quien puede combinarla con otra información que le haya proporcionado o que haya recopilado a partir del uso que haya hecho de sus servicios.</p>

<p>La aceptación de la presente Política implica que el usuario ha sido informado de forma clara y completa sobre el uso de dispositivos de almacenamiento y recuperación de datos (cookies) así como que XXXXXX dispone del consentimiento del usuario para el uso de las mismas en los términos del artículo 22 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico (LSSICE).</p>

<h3><strong>1. ¿Qué es una cookie y para qué sirve?</strong></h3>

<p>Una cookie es un fichero que se descarga en su ordenador, smartphone, tablet, etc. al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.</p>

<p>El conjunto de "cookies" de todos nuestros usuarios nos ayuda a mejorar la calidad de nuestra web, permitiéndonos controlar qué páginas son útiles, cuáles no y cuáles son susceptibles de mejora. En ningún caso las cookies podrían dañar tu equipo. Por contra, el que estén activas nos ayuda a identificar y resolver los errores.</p>

<h3><strong>2. ¿Clasificación de las cookies?</strong></h3>

<p>Las cookies pueden clasificarse:</p>

<h4>A. Según la entidad que la gestiona</h4>

<p>a) Cookies propias: Son aquellas que se envían a tu equipo desde nuestros propios equipos o dominios y desde el que prestamos el servicio que nos solicitas.</p>

<p>b) Cookies de terceros: Son aquellas que se envían a tu equipo desde un equipo o dominio que no es gestionado por nosotros, sino por otra entidad colaboradora. Como por ejemplo, las usadas por redes sociales.</p>

<h4>B. Según el plazo de tiempo que permanecen activadas</h4>

<p>a) Cookies de sesión: Son cookies temporales que permanecen en el archivo de cookies de tu navegador hasta que abandonas la página web, por lo que ninguna queda registrada en el disco duro de tu ordenador. La información obtenida por medio de estas cookies, sirven para analizar pautas de tráfico en la web.</p>

<p>b) Cookies persistentes: son almacenadas en el disco duro y nuestra web las lee cada vez que realizas una nueva visita. Una cookie permanente posee una fecha de expiración determinada. La cookie dejará de funcionar después de esa fecha.</p>

<h4>C. Según su finalidad</h4>

<p>a) Cookies técnicas: Son aquellas necesarias para la navegación y el buen funcionamiento de nuestra página web. Permiten por ejemplo, controlar el tráfico y la comunicación de datos, o compartir contenidos a través de redes sociales.</p>

<p>b) Cookies de personalización: Son aquéllas que te permiten acceder al servicio con unas características predefinidas en función de una serie de criterios, como por ejemplo el idioma, el tipo de navegador a través del cual se accede al servicio, la configuración regional desde donde se accede al servicio, etc.</p>

<p>c) Cookies de análisis: Son aquéllas que nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios de los servicios prestados.</p>

<p>d) Cookies publicitarias: Son aquéllas que permiten la gestión, de la forma más eficaz posible, de los espacios publicitarios que se pudieran incluir en nuestra página web.</p>

<p>e) Cookies de publicidad comportamental: Estas cookies almacenan información del comportamiento de los usuarios obtenida a través de la observación continuada. Gracias a ellas, podemos conocer los hábitos de navegación en internet y mostrarte publicidad relacionada con tu perfil de navegación.</p>

<h3>3. Cookies que usamos en la web wwww.XXXX.com</h3>

<p>En cumplimiento con lo establecido en el artículo 22.2 de la Ley de Servicios de la Sociedad de la Información y del Comercio Electrónico (LSSI) y en adecuación con la Directiva Europea 2009/136/CE, XXXXXX le informa y usted presta su consentimiento para la utilización de cookies en nuestra web por la mera navegación por la misma.</p>

<h4>A. Cookies Analíticas</h4>

<p>En primer lugar le informamos que empleamos cookies de análisis. La aplicación que utilizamos para obtener y analizar la información de la navegación es: Google Analytics: www.google.com/analytics/ que es un servicio prestado por Google Inc, un compañía de Delaware cuya oficina principal está en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos (“Google”).</p>

<p>Google Analytics utiliza "cookies", que son archivos de texto ubicados en su ordenador, para ayudar al website a analizar el uso que hacen los usuarios del sitio web. La información que genera la cookie acerca de su uso del website (incluyendo su dirección IP) será directamente transmitida y archivada por Google en sus servidores. Google usará esta información por cuenta nuestra con el propósito de seguir la pista de su uso del website, recopilando informes de la actividad del website y prestando otros servicios relacionados con la actividad del website y el uso de Internet. Google podrá transmitir dicha información a terceros cuando así se lo requiera la legislación, o cuando dichos terceros procesen la información por cuenta de Google. Google no asociará su dirección IP con ningún otro dato del que disponga Google.</p>

<p>Google Analytics es una herramienta sencilla y fácil de usar que ayuda a los propietarios de sitios web a medir cómo interactúan los usuarios con el contenido del sitio. Cuando un usuario navega por las páginas de un sitio web, Google Analytics proporciona al propietario de dicho sitio etiquetas JavaScript (bibliotecas) para registrar la información sobre la página que ha visto un usuario, por ejemplo, la URL de la página. Las bibliotecas JavaScript de Google Analytics usan cookies HTTP para "recordar" lo que ha hecho un usuario en páginas o en interacciones con el sitio web anteriores.</p>

<p>Google Analytics admite dos bibliotecas JavaScript (etiquetas) para medir el uso de los sitios web: analytics.js y ga.js.</p>

<p>La biblioteca JavaScript analytics.js forma parte de Universal Analytics y usa una sola cookie propia para:</p>

<ul>
  <li>Distinguir a los usuarios únicos</li>
  <li>Limitar el porcentaje de solicitudes</li>
</ul>

<p>Cuando se usa el fragmento recommended JavaScript recomendado, analytics.js configura las cookies en el dominio del nivel más alto que puede. Configurar las cookies en el dominio de nivel más alto posible permite realizar el seguimiento de los usuarios en varios subdominios sin agregar configuración adicional.</p>

<p>analytics.js configura las cookies siguientes:</p>

<p><img src="/img/cookies_analytics.png" style="width: 600px; max-width: 100%; float: left;"></p>


<p>

  <h4>B. Cookies publicitarias</h4>
  
  <p>Además de estas cookies empleamos cookies publicitarias de Google instaladas por la aplicación Google Maps ®, con la que se permite visualizar la ubicación geográfica de nuestra empresa. Cuando usted accede a la pestaña de “Contacto” de nuestra web Google le instala este tipo de cookies. Google utiliza cookies, como NID u OGP, para poder personalizar los anuncios que se muestran en los servicios de Google. Google utiliza esas cookies, por ejemplo, para recordarle sus búsquedas más recientes, sus interacciones anteriores con los resultados de búsqueda o con la publicidad de un anunciante y sus visitas al sitio web de un anunciante. De ese modo, Google puede mostrarle anuncios personalizados. Estas cookies son enteramente gestionadas por Google. La información sobre su uso en nuestro sitio web, incluyendo su dirección IP, puede ser transmitida a Google y almacenada en sus servidores, si bien, ello no le identifica personalmente a menos que usted haya iniciado sesión en Google, en cuyo caso está vinculado a su cuenta de Google. Si usted no desea que los datos sobre su sesión sean recopilados por este sitio web, modifique las opciones de seguridad y privacidad de su navegador, como indicamos más adelante en el apartado 4 de la presente política de cookies.</p>
  
  <h4>C. Cookies de Youtube</h4>
  
  <p>En nuestra sección “Actualidad”, en nuestro post “Conseguir la excelencia sin red comercial propia”, usted podrá ver un vídeo alojados en el proveedor de videos en streaming Youtube ® de la compañía Youtube LLC, compañía domiciliada en el número 901 de Cherri Avenue, San Bruno, California, CA 94066, Estados Unidos, incluida dentro del grupo empresarial de Google. Al acceder a esta sección de nuestra web y reproducir el vídeo se le instalaran las cookies de Youtube ®. Google establece un número de cookies en cualquier página que incluye un video de Youtube. Dado que son cookies de tercero, XXXXXX no tiene control sobre las cookies de Google, que incluyen una mezcla de fragmentos de información para medir el número y el comportamiento de los usuarios de Youtube, incluida la información que vincule sus visitas a nuestro sitio web con su cuenta de Google si se ha identificado el usuario con su cuenta google. La información sobre su uso en nuestro sitio web, incluyendo su dirección IP, puede ser transmitida a Google y almacenada en sus servidores, si bien, ello no le identifica personalmente a menos que usted haya iniciado sesión en Google, en cuyo caso está vinculado a su cuenta de Google.</p>
  
  <p>Además, al reproducir el vídeo Google instala otras cookies publicitarias denominadas "id", “DSID” e “IDE” que se almacenan en el dominio doubleclick.net cuyo objetivo es mostrarle anuncios relacionados con las preferencias de su navegación.</p>
  
  <p>Si usted no desea que los datos sobre su sesión sean recopilados por este sitio web, modifique las opciones de seguridad y privacidad de su navegador, como indicamos más adelante en el apartado 4 de la presente política de cookies.</p>
  
  <h4>D. Cookies usadas por redes sociales</h4>
  
  <p>En tercer lugar, empleamos cookies de complemento (plug-in) para intercambiar contenido de en redes sociales como Facebook®. Si pincha con su ratón en los iconos de estas redes sociales, podrá acceder a nuestra página en las mismas y acceder a la información que compartimos. Estos complementos almacenan y acceden a cookies en el equipo terminal usuario que permiten a la red social identificar a sus miembros mientras estos interactúan con los complementos. Por ello debe usted saber que si navega por nuestra página estando identificado en la correspondiente red social tiene que tener en cuenta que, si comparte contenido de esta web con amigos a través de la red social, o publica contenido en nuestra página de dichas redes sociales, recibirá cookies de esos sitios web. No podemos controlar los ajustes de las cookies de terceros, por lo que le sugerimos que compruebe los sitios web de dichos terceros para obtener más información sobre sus cookies y cómo gestionarlas.</p>
  
  <ul>
    <li><a href="https://www.facebook.com/policies/cookies/" target="_blank">Política de cookies Facebook</a></li>
  </ul>
  <h4>E. Cookies de uso interno</h4>
  
  <p>Finalmente, este sitio web emplea cookies técnicas para el funcionamiento de la web, concretamente para determinar si el usuario ha aceptado el uso de cookies.</p>
  
  <h3>4. ¿Cómo puedo configurar o deshabilitar las cookies?</h3>
  
  <p>Para permitir, conocer, bloquear o eliminar las cookies instaladas en tu equipo puedes hacerlo mediante la configuración de las opciones del navegador instalado en su ordenador. Por ejemplo puedes encontrar información sobre cómo hacerlo en el caso que uses como navegador:</p>
  
  <ul>
    <li>Firefox: <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectlocale=es&amp;redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank">aquí</a></li>
    <li>Chrome: <a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank">aquí</a></li>
    <li>Explorer: <a href="https://support.microsoft.com/es-es/products/windows?os=windows-10" target="_blank">aquí</a></li>
    <li>Safari: <a href="https://support.apple.com/kb/ph5042?locale=es_ES" target="_blank">aquí</a></li>
    <li>Safari para IOs: <a href="https://support.apple.com/es-es/HT201265" target="_blank">aquí</a></li>
    <li>Chrome para Android: <a href="https://support.google.com/chrome/answer/114662?hl=es&amp;visit_id=undefined&amp;rd=1" target="_blank">aquí</a></li>
  </ul>
  <p>XXXXXX se exonera de responsabilidad del uso y finalidades que pueda realizar Google Inc. sobre los datos obtenidos mediante este proceso. Puede consultar la política de privacidad de Google en el siguiente enlace <a href="https://www.google.es/intl/es/policies/privacy/" target="_blank">https://www.google.es/intl/es/policies/privacy/</a>. Por otra parte, si quieres no ser rastreado por Google Analytics a través de todas las páginas web, ve al siguiente link: <a href="http://tools.google.com/dlpage/gaoptout" target="_blank">http://tools.google.com/dlpage/gaoptout</a></p>
  
  <p>Si necesita mayor información acerca del funcionamiento de las cookies, así como la forma de eliminarlas, puede dirigirte a los siguientes enlaces:</p>
  
  <ul>
    <li>Entrada en la Wikipedia sobre las cookies.</li>
    <li>Documento técnico explicativo sobre las cookies (página en inglés)</li>
    <li><a href="http://www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a> (página en inglés)</li>
  </ul>
</p>

<table width="652" cellspacing="0" cellpadding="7">
  <colgroup>
    <col>
    <col>
    <col>
    <col>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <td colspan="7" height="21">
        <h3 class="indentado"><strong>Relación de cookies empleadas en el dominio www.XXXXXX.com</strong></h3>
      </td>
    </tr>
    <tr>
      <td colspan="7" height="22">
        <h4 align="center"><strong>COOKIES DE TERCEROS</strong></h4>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado"><strong>Servidor desde el que se envía</strong></p>
      </td>
      <td colspan="2">
        <p class="indentado"><strong>Nombre</strong></p>
      </td>
      <td>
        <p class="indentado"><strong>Proveedor</strong></p>
      </td>
      <td>
        <p class="indentado"><strong>Finalidad</strong></p>
      </td>
      <td>
        <p class="indentado"><strong>Caducidad</strong></p>
      </td>
      <td>
        <p class="indentado"><strong>Excluida/No Excluida (del deber de la información y consentimiento)</strong></p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">.XXXXXX.com</p>
      </td>
      <td colspan="2">
        <p class="indentado">_ga</p>
      </td>
      <td>
        <p class="indentado">Google Analytics</p>
      </td>
      <td>
        <p class="indentado">Analítica (servicio Google Analytics): Se usa para distinguir a los usuarios. Utilizamos cookies de Google Analytics embebidas en nuestro sitio web para recopilar información acerca de cómo los visitantes navegan en nuestro sitio web. Utilizamos la información para elaborar informes y ayudar a mejorar el sitio. Las cookies recopilan información de forma anónima, incluyendo el número de visitantes, de donde vienen y las páginas que han visitado en nuestra web.</p>
      </td>
      <td>
        <p class="indentado">Persistente (dos años)</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">.comerteam.es</p>
      </td>
      <td colspan="2">
        <p class="indentado">_gat</p>
      </td>
      <td>
        <p class="indentado">Google Analytics</p>
      </td>
      <td>
        <p class="indentado">Analítica (servicio Google Analytics): Se usa para limitar el porcentaje de solicitudes. Utilizamos cookies de Google Analytics embebidas en nuestro sitio web para recopilar información acerca de cómo los visitantes navegan en nuestro sitio web. Utilizamos la información para elaborar informes y ayudar a mejorar el sitio. Las cookies recopilan información de forma anónima, incluyendo el número de visitantes, de donde vienen y las páginas que han visitado en nuestra web.</p>
      </td>
      <td>
        <p class="indentado">Persistente (diez minutos)</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">.google.com</p>
      </td>
      <td colspan="2">
        <p class="indentado">NID</p>
      </td>
      <td>
        <p class="indentado">Google</p>
      </td>
      <td>
        <p class="indentado">Gestión de Publicidad: Google utiliza cookies, como las cookies PREF, NID y SID, para poder personalizar los anuncios que se muestran en los servicios de Google. Utiliza esas cookies, por ejemplo, para recordarte tus búsquedas más recientes, tus interacciones anteriores con los resultados de búsqueda o con la publicidad de un anunciante y tus visitas al sitio web de un anunciante. De ese modo, Google puede mostrarte anuncios personalizados.</p>
      </td>
      <td>
        <p class="indentado">Persistente (6 meses)</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">youtube.com</p>
      </td>
      <td colspan="2">
        <p class="indentado">VISITOR_INFO1_LIVE</p>
      </td>
      <td>
        <p align="center">Youtube</p>
      </td>
      <td>
        <p>Almacenar la estimación del ancho de banda de cada video de youtube.com reproducido</p>
      </td>
      <td>
        <p class="indentado">Persistente (8 meses)</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">.youtube.com</p>
      </td>
      <td colspan="2">
        <p class="indentado">YSC</p>
      </td>
      <td>
        <p align="center">Youtube</p>
      </td>
      <td>
        <p>Contiene un identificador único para permitir el control de visitas a videos de Youtube.</p>
      </td>
      <td>
        <p class="indentado">Sesión</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">.youtube.com</p>
      </td>
      <td colspan="2">
        <p class="indentado">PREF</p>
      </td>
      <td>
        <p align="center">Youtube</p>
      </td>
      <td>
        <p class="indentado">Gestión de Publicidad: Google utiliza cookies, como las cookies PREF, NID y SID, para poder personalizar los anuncios que se muestran en los servicios de Google. Utilizamos esas cookies, por ejemplo, para recordarte tus búsquedas más recientes, tus interacciones anteriores con los resultados de búsqueda o con la publicidad de un anunciante y tus visitas al sitio web de un anunciante. De ese modo, podemos mostrarte anuncios personalizados en Google.</p>
      </td>
      <td>
        <p class="indentado">Persistente (2 años)</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
    <tr>
      <td colspan="7">
        <h4 align="center"><strong>COOKIES PROPIAS</strong></h4>
      </td>
    </tr>
    <tr>
      <td>
        <p class="indentado">www.XXXXXX.es</p>
      </td>
      <td colspan="2">
        <p class="indentado">LawCookie [cookie control]</p>
      </td>
      <td>
        <p class="indentado">&nbsp;</p>
      </td>
      <td>
        <p class="indentado">Cookie técnica usada pare detectar si el usuario ha aceptado la instalación de cookies.</p>
      </td>
      <td>
        <p class="indentado">De sesión</p>
      </td>
      <td>
        <p class="indentado">SI</p>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <p class="indentado">www.XXXXXX.com</p>
      </td>
      <td>
        <p class="indentado">CAKEPHP</p>
      </td>
      <td>
        <p class="indentado">&nbsp;</p>
      </td>
      <td>
        <p class="indentado">Cookie técnica empleada para identificar al usuario</p>
      </td>
      <td>
        <p class="indentado">Persistente</p>
      </td>
      <td>
        <p class="indentado">NO</p>
      </td>
    </tr>
  </tbody>
</table>