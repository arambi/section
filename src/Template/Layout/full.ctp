<?php 
/**
 * Layout usado para pruebas con sections y wrappers
 */
?>
<?= $this->cell( 'Section.Wrapper::display', [
    'name' => 'right'
]) ?>
<?= $this->cell( 'Section.Wrapper::display', [
    'name' => 'main', 
    'content' => $this->fetch( 'content')
]) ?>
