<!DOCTYPE html>
<html lang="en">

<head>
  <?= $this->Html->css([
    '/test_plugin/css/bootstrap.min',
    '/test_plugin/css/modern-business'
  ]) ?>
</head>
<body>
<div class="container">
  <nav class="nav-main">
    <?= $this->Nav->nav( 'main') ?>
  </nav>
  <div class="row">
    <div class="col-md-8">
      <?= $this->cell( 'Section.Wrapper::display', [
          'name' => 'main', 
          'content' => $this->fetch( 'content')
      ]) ?>
    </div>
    <div class="col-md-4">
      <?= $this->cell( 'Section.Wrapper::display', [
          'name' => 'right'
      ]) ?>
    </div>
  </div>
  <footer>
    <nav class="nav-footer">
      <?= $this->Nav->nav( 'bottom') ?>
    </nav>
  </footer>
</div>
</body>
</html>
