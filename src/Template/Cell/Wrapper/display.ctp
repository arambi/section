<?php foreach( $rows as $row): ?>
  <div class="row">
    <?php foreach( $row->columns as $column): ?>
      <div class="col-sm-<?= $column->cols ?>">
        <?php foreach( $column->blocks as $block): ?>
            <?= $this->cell( $block->block_type ['cell'], ['block' => $block, 'content' => $content]) ?>
        <?php endforeach ?>
      </div>
    <?php endforeach ?>
  </div>
<?php endforeach ?>