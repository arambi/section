<nav class="nav-webmap">
  <h2><?= __d( 'app', 'Mapa web') ?></h2>
  <?= $this->element( 'webmap', ['sections' => $sections]) ?>
</nav>