<div class="c-form-access">
  <h1><?= $this->Nav->sectionTitle() ?></h1>
  <?= __d( 'app', 'La página a la que quieres acceder es de acceso restringido.') ?>
  <div class="c-form form">
      <?php if( isset( $error)): ?>
        <div class="error-message error">
          <?= __d( 'app', 'La contraseña introducida no es correcta') ?>
        </div>
      <?php endif ?>
    <?= $this->Form->create( null) ?>
      <?= $this->Form->input( 'pwd', [
        'label' => __d( 'app', 'Contraseña'),
        'class' => 'not-empty'
      ]) ?>
      <?= $this->Form->button( __d( 'app', 'Acceder')) ?>
    <?= $this->Form->end() ?>
  </div>
</div>