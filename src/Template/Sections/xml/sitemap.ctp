<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xhtml="http://www.w3.org/1999/xhtml"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
  <?php foreach( $nodes as $model): ?>
    <?php foreach( $model as $node): ?>
      <url>
        <?php $first = current( $node) ?>
        <loc><?= $first ['url'] ?></loc>
        <?php if( count( $node) > 1): ?>
          <?php foreach( $node as $lang => $loc): ?>
            <xhtml:link rel="alternate" hreflang="<?= $lang ?>" href="<?= $loc ['url'] ?>" />
          <?php endforeach ?>
        <?php endif ?>
        <lastmod><?= $first ['date'] ?></lastmod>
      </url>
    <?php endforeach ?>
  <?php endforeach ?>

</urlset>
