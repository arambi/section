<?php if( $this->Nav->isHomepage()): ?>
    <?= $this->cell( 'Block.Row', ['rows' => $this->request->params ['section']->rows]) ?>
<?php else: ?>
    <div class="section-view">
      <?= $this->cell( 'Block.Row', ['rows' => $this->request->params ['section']->rows]) ?>
    </div>
<?php endif ?>