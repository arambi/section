  <div class="row wrapper wrapper-update border-bottom white-bg page-heading">
    <ng-include src="'header.html'"></ng-include>
  </div>
  <div class="animated fadeIn">
    <div class="row" ng-init="crudConfig = data.crudConfig">
      <ng-include src="'Manager/_warning.html'"></ng-include>
      <div class="col-md-12">
        <h2 ng-if="data.crudConfig.view.columns.length > 1" class="display-field">{{ data.content[data.crudConfig.model.displayField] }}</h2>
        <form cf-form class="form-horizontal">
          <input ng-model="data.content.id" type="hidden" class="form-control">
          <div class="manager-page">
            <div class="manager-page__tabs">
              <cr-tabs columns="data.crudConfig.view.columns"></cr-tabs>
            </div>
            <div class="manager-page__contents">
              <div ng-show="currentColumn == $index" ng-repeat="column in data.crudConfig.view.columns">
                <crud-col box="column.box" size="column.cols"></crud-col>
              </div>
            </div>
          </div>
        </form>
      </div>
      <!-- <div class="col-md-3">
        <div ng-repeat="menu in data.contents">
          <div class="ibox" ng-if="menu.menu.key == data.content.menu">
            <?= $this->element( 'Section.menu_tree') ?>
          </div>
        </div>
      </div> -->
    </div>
  </div>

  <?= $this->element( 'Section.menu_template') ?>


