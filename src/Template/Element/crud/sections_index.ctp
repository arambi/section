
<div class="row wrapper wrapper-index border-bottom white-bg page-heading">
  <ng-include src="'header.html'"></ng-include>
</div>
<div class="wrapper wrapper-content animated fadeIn" ng-controller="sectionsCtrl">
  <div class="row">
    <div ng-repeat="menu in data.contents" class="col-lg-4" ng-init="clearleft = ($index == 3 || $index == 6)" ng-class="{'clearleft': clearleft}">
      <div class="ibox" ng-if="menu.menu.key != 'none'">
        <?= $this->element( 'Section.menu_tree') ?>
      </div>
    </div>

    <div ng-init="menu = data.contents.none" class="col-lg-12">
      <div class="ibox">
        <div class="ibox-title">
          <h5>{{ menu.menu.title }}</h5>
        </div>
        <div class="ibox-content">
          <a ng-model="data" cf-modal="'/admin/section/sections/add/' + menu.menu.key + '/0'" href class="btn btn-outline btn-default btn-xs m-b"><i class="fa fa-plus"></i> <span class="bold"><?= __d( 'admin', 'Añadir') ?></span></a>

          <div ui-tree="treeOptions" class="dd cf-sections-list" data-max-depth="4" id="tree-root-{{ menu.menu.title }}">
            <ol class="dd-list" ui-tree-nodes="" ng-model="menu.contents">
              <li class="dd-item" ng-repeat="content in menu.contents" ui-tree-node ng-include="'nodes_renderer.html'" ng-init="contents = menu.contents"></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->element( 'Section.menu_template') ?>
