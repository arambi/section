<div class="form-group">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ field.label }}</label>
  <div class="col-lg-9"> 
    <div ng-repeat="(_key, label) in content.sectionType.actionLayouts" class="row m-t m-b"> 
      <label>{{ label }}</label>
      <select ng-model="content[field.key][_key]" class="form-control" style="width:350px; display: inline">
        <option style="display: none" value="">{{ field.empty }}</option>
        <option ng-repeat="(id, title) in field.options" value="{{ id }}" ng-selected="content[field.key][_key] == id">{{ title }}</option>
      </select>
    </div>
  </div>
</div>