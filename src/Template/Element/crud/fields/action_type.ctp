<div class="form-group">
  <label></label>
  <div class="col-lg-9">
    <h2><i class="{{ content.sectionType.icon }}"></i> {{ content.sectionType.label }}</h2>
    <p ng-if="content.action != 'anchor'"><i class=""></i> <a target="_blank" href="{{ content.url }}">{{ content.url }}</a></p>
  </div>
</div>