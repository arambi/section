<div class="form-group"  ng-repeat="(key, values) in field.options">
  <label ng-help tooltip="{{ field.help }}" class="col-lg-3 control-label">{{ values.label }}</label>
  <div class="col-lg-6">
    <span class="select-selected" ng-repeat="_key in content.settings.sections_categories[key]">{{ values.options[_key] }} </span>
    <select class="form-control" ng-model="content.settings.sections_categories[key]" style="width:100%;" multiple>
      <option ng-selected="content.settings.sections_categories[key].indexOf( _key) > -1" ng-repeat="(_key, value) in values.options" value="{{ _key }}">{{ value }}</option>
    </select>
  </div>
</div>