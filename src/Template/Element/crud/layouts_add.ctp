<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2><?= __d( "admin", "Crear nuevo layout") ?></h2>
  </div>
</div>
<div class="row">
  <div class="col-xl-12">
    <div class="ibox float-e-margins">
      <div class="ibox-content cf-section-actions">
        <a class="btn btn-default dim" ng-repeat="(key, action) in data.templates" ng-click="cancel()" href="#admin/section/layouts/create/{{ key }}">
          {{ key }}
        </a> 
      </div>
    </div>
  </div>
</div>  

