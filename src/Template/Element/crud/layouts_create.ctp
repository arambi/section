<!-- <pre>{{ data.content | json}}</pre> -->

  <div class="row wrapper border-bottom white-bg page-heading">
    <ng-include src="'header.html'"></ng-include>
  </div>
  <div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <form class="form-horizontal">
          <input ng-model="data.content.id" type="hidden" class="form-control">
          <div class="manager-page">
            <div class="manager-page__contents no-columns">
              <div ng-repeat="column in data.crudConfig.view.columns">
                <crud-col box="column.box" size="column.cols"></crud-col>
              </div>
              <div class="row">
                <div ng-repeat="wrapper in data.content.wrappers" class="col-lg-{{ data.wrapperNames[data.content.template].wrappers[wrapper.cname].cols }} pull-{{ data.wrapperNames[data.content.template].wrappers[wrapper.cname].position }}">
                  <div class="ibox">
                    <div class="ibox-content">
                      <h3>{{ data.wrapperNames[data.content.template].wrappers[wrapper.cname].name }}</h3>
                      <ng-include 
                        ng-init="field = data.crudConfig.view.fields.blocks; content = wrapper; crudConfig.blockWrapper = 'wrapper-'+ wrapper.cname; crudConfig.model.alias = 'Wrappers'; crudConfig.blocks = data.crudConfig.layouts[data.content.template].wrappers[wrapper.cname].blocks" 
                        src="'Block/fields/blocks.html'"></ng-include>
                    </div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </form>
    </div>
  </div>


