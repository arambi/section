<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-10">
    <h2><?= __d( "admin", "Crear nueva sección") ?></h2>
  </div>
</div>
<div class="row">
  <div class="col-xl-12">
    <div class="ibox float-e-margins">
      <div class="ibox-content cf-section-actions">
        <a class="btn btn-default dim" ng-repeat="(key, action) in data.actions" ng-click="cancel()" href="#admin/section/sections/create/{{ data.content.menu }}/{{ data.content.parent_id }}/{{ key }}?_{{ data.time }}">
          <i class="{{ action.icon }} block m-b-xs fa-2x"></i> {{ action.label }}
        </a> 
      </div>
    </div>
  </div>
</div>  