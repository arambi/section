<div class="ibox-title">
  <h5>{{ menu.menu.title }}</h5>
</div>
<div class="ibox-content">
  <div ui-tree="treeOptions" class="dd cf-sections-list" data-max-depth="4" id="tree-root-{{ menu.menu.title }}">
    <ol class="dd-list" ui-tree-nodes="" ng-model="menu.contents">
      <li class="dd-item" ng-repeat="content in menu.contents" ui-tree-node ng-init="contents = menu.contents" ng-include="'nodes_renderer.html'"></li>
    </ol>
  </div>
  <a ng-model="data" cf-modal="'/admin/section/sections/add/' + menu.menu.key + '/0'" href class="btn btn-outline btn-default btn-xs m-t"><i class="fa fa-plus"></i> <span class="bold"><?= __d( 'admin', 'Añadir') ?></span></a>
</div>