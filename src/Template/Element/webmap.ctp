<ul>
<?php foreach( $sections as $section): ?>
    <li><a href="<?= $section->link() ?>"><?= $section->title ?></a>
      <?php if( !empty( $section->children)): ?>
          <?= $this->element( 'webmap', ['sections' => $section->children]) ?>
      <?php endif ?>
    </li>
<?php endforeach ?>
</ul>