<!-- Nested node template -->
<script type="text/ng-template" id="nodes_renderer.html">
  <div class="dd-handle cf-section-list-item">
    <span class="" ng-show="!collapsed && content.nodes.length > 0" style="width: 40px; margin-right: 10px; flex-grow: 0" ng-click="collapsed = !collapsed"><i class="fa fa-plus"></i></span>
    <span class="" ng-show="collapsed && content.nodes.length > 0" style="width: 40px; margin-right: 10px; flex-grow: 0" ng-click="collapsed = !collapsed"><i class="fa fa-minus"></i></span>

    <span title="{{ data.actionTypes[content.sectionType.key] }}" ui-tree-handle><i class="cf-section-icon {{ content.sectionType.icon }}"></i> {{ content.title }}</span> 
    <i ng-if="content.is_homepage == 1" class="fa fa-home"></i> <i title="<?= __d('admin', 'No publicado') ?>" ng-if="!content.published == 1" class="fa fa-remove"></i>
    <a class="pull-right btn btn-white btn-xs" ng-model="data" cf-modal="'/admin/section/sections/add/' + menu.menu.key + '/' + content.id" href><i class="fa fa-plus"></i></a>
    <a class="pull-right btn btn-white btn-xs" href confirm-delete cf-url="{{ content.urls.delete }}" cf-destroy="scope.contents.splice( scope.$index, 1)"><i class="fa fa-trash"></i></a> 
    <a title="{{ actionTypes[content.sectionType.key] }}" class="pull-right btn btn-white btn-xs" href="#admin/section/sections/update/{{ content.id }}"><i class="fa fa-pencil"></i></a>
  </div>
  <ol class="dd-list" ui-tree-nodes="" ng-model="content.nodes" ng-init="nodes = content.nodes" ng-show="collapsed">
    <li class="dd-item" ng-repeat="content in nodes" ng-init="contents = nodes" ui-tree-node ng-include="'nodes_renderer.html'">
    </li>
  </ol>
</script>