<?php
namespace Section\Action;

class Action
{
  /**
   * El key computerizado
   * @var string
   */
  public $key;

/**
 * El nombre humano de la action
 * @var string
 */
  public $label;

/**
 * El nombre del plugin de la route
 * @var null\string
 */

  public $plugin = null;

/**
 * El nombre del controller de la route
 * @var null\string
 */
  public $controller = null;

/**
 * El nombre de la action de la route
 * @var string
 */
  public $action = 'index';

/**
 * el class del icono de FontAwesome
 * @var null
 */
  public $icon;

/**
 * Si es true, se autocreará la sección (si no existe) cuando se acceda en el admin a ver el listado de secciones
 * @var boolean
 */
  public $autocreate = false;

  public $defaults = [];

/**
 * Array con las acciones que podrán tener un layout distinto. Por ejemplo, un layout distinto para el index y otro para el view
 * @var array
 */
  public $actionLayouts = false;

/**
 * Array con valores action => route_params. Por ejemplo, si queremos crear una ruta blog/posts/view/:slug, debemos indicar aquí 'view' => '/:slug'
 * Todas las rutas para esa section serán /ruta-seccion/titulo-del-post, donde `titulo-del-post` será el slug
 * @var [type]
 */
  public $actions = [];

/**
 * Si se indica un model, la sección tendrá como hijos a los elementos de esa tabla
 * Deberá ser un array de tipo
 * [
 *   'model' => 'Plugin.Table',
 *   'action' => 'category'
 * ]
 * @var array | null
 */
  public $childrensTable = null;

  public $wildcard = false;

  public $withBlocks = false;

  public $blockDefaults = [];


/**
 * Constructor
 * Utiliza las keys de data para las propiedades del objeto
 * Ver descripción en las declaraciones de cada propiedad
 *   
 * @param array $data
 */
  public function __construct( array $data)
  {
    foreach( $data as $key => $value)
    {
      $this->$key = $value;
    }
  }

/**
 * Convierte en array todas las propiedades del objeto
 * @return array 
 */
  public function toArray()
  {
    $keys = array(
        'key',
        'label',
        'plugin',
        'controller',
        'action',
        'icon',
        'childrensTable',
        'withBlocks',
        'blockDefaults'
    );

    $return = [];

    foreach( $keys as $key) 
    {
      $return [$key] = $this->$key;
    }

    return $return;
  }

  public function url()
  {
    return [
      'plugin' => $this->plugin,
      'controller' => $this->controller,
      'action' => $this->action,
    ];
  }

}