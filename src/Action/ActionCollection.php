<?php
namespace Section\Action;
use Section\Action\Action;
use Cake\Collection\Collection;
use Cake\ORM\TableRegistry;
use Website\Lib\Website;
use I18n\Lib\Lang;

/**
 * Se encarga de gestionar las acciones de las secciones
 */
class ActionCollection
{
/**
 * Conjunto de objetos Action disponibles en el script de la aplicación
 * @var array
 */
  protected static $_actions = [];

/**
 * Secciones NO disponibles
 * 
 * @var array
 */
protected static $_notAvailables = [];

/**
 * Recoge un nuevo action y lo guarda
 * Ver las opciones de keys para $data en Section\Action\Action
 * 
 * @param string $key  El nombre computerizado de la action
 * @param array  $data Conjunto de opciones
 */
  public static function set( $key, array $data)
  {
    $data ['key'] = $key;
    $action = new Action( $data);

    static::$_actions [$key] = $action;
  }

/**
 * Toma un objeto, dado el key
 * @param  string $key
 * @return \Section\Action\Action
 */
  public static function get( $key = null)
  {
    if( !$key)
    {
      return static::getAvailables();
    }

    if( !isset( static::$_actions [$key]))
    {
      return null;
    }
    return static::$_actions [$key];
  }

  public static function getAvailables()
  {
    $return = [];

    foreach( static::$_actions as $key => $action)
    {
      if( !in_array( $key, static::$_notAvailables))
      {
        $return [$key] = $action;
      }
    }

    return $return;
  }

  public static function autocreate()
  {

    $SectionsTable = TableRegistry::get( 'Section.Sections');
    $collection = new Collection( json_decode( json_encode( static::$_actions), true));

    $actions = $collection->match(['autocreate' => 1]);

    foreach( $actions as $action)
    {
      $action = self::get( $action ['key']);

      if( !isset( $action->defaults))
      {
        continue;
      }

      if( !empty( $action->autocreateCallback))
      {
        $function = $action->autocreateCallback;

        if( !$function())
        {
          continue;
        }
      }

      foreach( $action->defaults as $key => $value)
      {
        if( $SectionsTable->hasTranslate( $key))
        {
          foreach( Lang::iso3() as $iso3 => $name)
          {
            $data ['_translations'][$iso3][$key] = $value;
          }
        }
        else
        {
          $data [$key] = $value;
        }
      }
          
      $data ['action'] = $action->key;

      if( $SectionsTable->exists( ['action' => $data ['action']]))
      {
        continue;
      }
      $section = $SectionsTable->getNewEntity( $data);
      
      if( !$SectionsTable->saveContent( $section))
      {
        \Cake\Log\Log::debug( $section);
      }
    }
  }

/**
 * Devuelve un array combinado con la key y el label de cada action
 * 
 * @return array
 */
  public static function types()
  {
    $collection = new Collection( json_decode( json_encode( self::$_actions), true));
    return $collection->combine( 'key', 'label')->toArray();
  }

  /**
   * Configura la variable $_availables
   * 
   * @param  array $config
   * @return void
   */
  public static function setNotAvailables( $config)
  {
    self::$_notAvailables = array_merge( self::$_notAvailables, $config);
  }
}