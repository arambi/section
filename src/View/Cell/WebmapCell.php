<?php
namespace Section\View\Cell;

use Cake\View\Cell;
use Cake\Core\Configure;

/**
 * Webmap cell
 */
class WebmapCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display()
  {
    $this->loadModel( 'Section.Sections');
    $menus = Configure::read( 'Section.menus');
    $sections = [];

    foreach( $menus as $key => $menu)
    {
      if( $this->Sections->hasBehavior( 'Blockable'))
      {
        $this->Sections->removeBehavior( 'Blockable');
      }

      $data = $this->Sections->getByMenu( $menu ['key'], 'nav', function( $query){
        $query->where([
          'reference_id IS NULL',
          'no_webmap' => false
        ]);
      });
      $sections = array_merge( $sections, $data);
    }

    $this->set( compact( 'sections'));
  }
}
