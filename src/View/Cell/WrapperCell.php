<?php
namespace Section\View\Cell;

use Cake\View\Cell;

/**
 * Wrapper cell
 */
class WrapperCell extends Cell
{

  /**
   * List of valid options that can be passed into this
   * cell's constructor.
   *
   * @var array
   */
  protected $_validCellOptions = [];

  /**
   * Default display method.
   *
   * @return void
   */
  public function display( $cname, $content = null, $view = null)
  {
    $wrappers = $this->request->params ['layout']->wrappers;

    foreach( (array)$wrappers as $_wrapper)
    {
      if( $_wrapper->cname == $cname)
      {
        $wrapper = $_wrapper;
        break;
      }
    }

    if( isset( $wrapper))
    {
      $this->set( ['rows' => $wrapper->rows, 'content' => $content]);
    }
    else
    {
      $this->set( ['rows' => [], 'content' => $content]);
    }

    if( $view !== null)
    {
      $this->template = $view;
    }
  }
}
