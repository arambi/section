<?php
namespace Section\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Section\Action\ActionCollection;

/**
 * Links helper
 */
class LinksHelper extends Helper
{

    public $helpers = [
        'Section.Nav'
    ];

    public function url(string $key, $fullbase = false)
    {
        $action = ActionCollection::get($key);

        if ($action) {
            return $this->Nav->url($action->url(), $fullbase);
        }
    }

    public function classActive($key, $class = 'active')
    {
        $section = $this->request->getParam('section');

        if ($section && $section->action == $key) {
            return $class;
        }
    }
}
