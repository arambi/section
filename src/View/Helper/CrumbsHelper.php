<?php

namespace Section\View\Helper;

use Cake\View\View;
use Cake\View\Helper;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Section\Crumbs\Crumbs;

/**
 * Crumbs helper
 */
class CrumbsHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public $helpers = [
        'Section.Nav',
        'Html'
    ];

    private $options = [];

    private $crumbs = [];

    public function display($options = [])
    {
        if (!$this->request->getParam('section')) {
            return;
        }

 
        $defaults = [
            'homepage' => false,
            'last' => true,
            'ulClass' => false,
            'liClass' => false,
            'firstClass' => false,
            'lastClass' => false,
            'writeUl' => true,
        ];

        $this->options = array_merge($defaults, $options);

        $this->crumbs = Crumbs::get();
        $maincontent = $this->Nav->getMainContent();

        if ($maincontent && !collection($this->crumbs)->firstMatch(['title' => $maincontent->full_title])) {
            if (!empty($maincontent->full_title)) {
                $this->crumbs[] = [
                    'title' => Text::truncate($maincontent->full_title, 60, ['exact' => false]),
                    'url' => false
                ];
            }
        }

        $out = [];

        $position = 1;

        if ($this->options['homepage']) {
            $el = [
                $this->Html->link($this->options['homepage'], $this->Nav->homelink(), [
                    'itemprop' => 'name',
                    'escape' => false
                ]),
                '<meta itemprop="position" content="' . $position . '">',
                '<meta itemprop="item" content="' . Router::url($this->Nav->homelink(), true) . '">',
            ];

            $out[] = $this->Html->tag('li', implode("\n", $el), [
                'itemprop' => 'itemListElement',
                'itemscope',
                'name' => $this->options['homepage'],
                'itemtype' => 'http://schema.org/ListItem',
                'class' => $this->liHomeClass()
            ]);

            $position++;
        }


        foreach ($this->crumbs as $key => $crumb) {
            $url = is_array($crumb['url']) ? $this->Nav->url($crumb['url']) : $crumb['url'];
            $isLast = false;

            if (!$crumb['url'] || count($this->crumbs) == ($key + 1)) {
                if (count($this->crumbs) == ($key + 1) && !$this->options['last']) {
                    continue;
                }

                $link = $this->Html->tag('span', $crumb['title'], [
                    'itemprop' => 'name',
                    'aria-current' => 'page'
                ]);

                $link .= '<meta itemprop="position" content="' . $position . '" />';
                $link .= '<meta itemprop="item" content="' . Router::url($url, true) . '" />';
                $isLast = true;
            } else {
                $link = $this->Html->link($crumb['title'], $url, [
                    'itemprop' => 'name',
                    'escape' => false
                ]);

                $link .= '<meta itemprop="position" content="' . $position . '" />';
                $link .= '<meta itemprop="item" content="' . Router::url($url, true) . '" />';
            }

            $out[] = $this->Html->tag('li', $link, [
                'itemprop' => 'itemListElement',
                'itemscope',
                'itemtype' => 'http://schema.org/ListItem',
                'class' => $this->liClass($key, $isLast),
            ]);

            $position++;
        }

        if ($this->options['writeUl']) {
            return $this->Html->tag('ul', implode("\n", $out), [
                'itemscope',
                'itemtype' => "http://schema.org/BreadcrumbList",
                'class' => $this->options['ulClass']
            ]);
        } else {
            return implode("\n", $out);
        }

    }


    public function liHomeClass()
    {
        if ($this->options['homepage']) {
            return (string)$this->options['liClass'] . (string)$this->options['firstClass'];
        }

        return $this->options['liClass'];
    }

    public function liClass($key, $isLast = false)
    {
        if (!$this->options['homepage'] && $key == 0) {
            return (string)$this->options['liClass'] .' '. (string)$this->options['firstClass'];
        } elseif ($isLast) {
            return (string)$this->options['liClass'] .' '. (string)$this->options['lastClass'];
        }

        return $this->options['liClass'];
    }
}
