<?php

namespace Section\View\Helper;

use ArrayObject;
use I18n\Lib\Lang;
use Cake\I18n\I18n;
use Cake\View\View;
use Cake\Event\Event;
use Cake\View\Helper;
use User\Auth\Access;
use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Website\Lib\Website;
use Section\Crumbs\Crumbs;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;
use Cake\Event\EventManager;
use Section\Routing\RouteData;
use Cake\Collection\Collection;
use Section\Routing\UrlBuilder;
use Cake\View\StringTemplateTrait;
use Cake\Network\Exception\NotFoundException;
use stdClass;

/**
 * Nav helper
 */
class NavHelper extends Helper
{
    use StringTemplateTrait;

    public $helpers = ['Html', 'Url', 'User.Auth', 'Manager.Editable', 'Cofree.Buffer', 'Section.Crumbs'];

    private $combinePath;
    private $pathSlugs;


    protected $_metas = [];

    protected $_defaultConfig = [
        'templates' => [
            'tag' => '<{{tag}}{{attrs}}>{{content}}</{{tag}}>',
        ]
    ];

    public function __construct(View $View, array $config = [])
    {
        parent::__construct($View, $config);
        $this->Sections = $this->TL()->get('Section.Sections');
    }

    /**
     * $options
     * `showChildrens` Muestra las secciones hijas
     * `maxLevels` El número máximo de niveles a mostrar
     * `contentAfter` Callback cuyo resultado se añadirá al elemento. La función recibe como parámetro la sección del menu
     *   
     * @param  [type] $menu    
     * @param  array  $attrs   
     * @param  array  $options 
     * @return 
     */
    public function nav($menu, $attrs = array(), $options = [])
    {
        $sections = $this->Sections->getByMenu($menu);
        return $this->_nav($sections, $attrs, 0, $options);
    }

    public function navFirstHalf($menu, $attrs = array(), $options = [])
    {
        $sections = $this->Sections->getByMenu($menu);
        $half = round(count($sections) / 2);
        $sections = array_slice($sections, 0, $half);
        return $this->_nav($sections, $attrs, 0, $options);
    }

    public function navSecondHalf($menu, $attrs = array(), $options = [])
    {
        $sections = $this->Sections->getByMenu($menu);
        $half = round(count($sections) / 2);
        $sections = array_slice($sections, $half);
        return $this->_nav($sections, $attrs, 0, $options);
    }

    private function _nav($sections, $attrs, $level, $options, $parent = null)
    {
        $defaultAttrs = array(
            'ul' => [],
            'li' => [],
            'a' => [],
            'classCurrent' => 'current',
            'classHasChildrens' => 'has-childrens',
        );

        $defaultOptions = [
            'showChildrens' => false,
            'maxLevels' => 0,
            'contentAfter' => false
        ];

        $attrs = array_merge($defaultAttrs, $attrs);
        $options = array_merge($defaultOptions, $options);
        $links = array();
        $_attrs = array(
            'wrap_submenu' => false
        );

        $attrs = array_merge($_attrs, $attrs);

        foreach ((array)$sections as $section) {
            if (!$this->hasPermissions($section)) {
                continue;
            }

            if ($section->action == 'reference') {
                $_section = $section;
                $section = $data = $this->Sections->find('nav')
                    ->where([
                        'Sections.id' => $section->reference_id,
                        'Sections.published' => 1,
                    ])
                    ->first();

                if (!$section) {
                    continue;
                }

                foreach (['title', 'title_menu', 'anchor', 'cname'] as $field) {
                    $section->set($field, $_section->$field);
                }
            }

            // Muestra la sección hija
            if ($section->show_children) {
                $children = (new Collection($section->children))->firstMatch([
                    'id' => $section->show_children_id
                ]);

                $params = UrlBuilder::sectionParams($children, $parent);

                if (empty($children->sectionType->actions) || !empty($children->content_id)) {
                    $params['section_id'] = $children->id;
                }
            } elseif ($section->sectionType->key != 'external') {
                $params = UrlBuilder::sectionParams($section, $parent);

                if (empty($section->sectionType->actions) || !empty($section->content_id)) {
                    $params['section_id'] = $section->id;
                }

                if (property_exists($section->sectionType, 'sectionsCategories') && !empty($section->settings) && isset($section->settings->sections_categories)) {
                    $params['section_categories'] = (array)$section->settings->sections_categories;
                    $params['action'] = 'categorized';
                }
            }

            if ($section->action == 'anchor' && $parent) {
                $url = $this->Sections->getUrl($parent) . '#' . $section->anchor;
            } elseif ($section->sectionType->key == 'external') {
                $url = $section->external_url;
            } else {
                $url = $this->url($params);
            }

            if (isset($attrs['a']['levels'])) {
                $_attrs = $attrs['a']['levels'][$level];
            } else {
                $_attrs = $attrs['a'];
            }

            if (!isset($_attrs['class'])) {
                $_attrs['class'] = '';
            }

            if ($section->target_blank) {
                $_attrs['target'] = '_blank';
            }

            if ($section->sitemap_exclude) {
                $_attrs['rel'] = 'nofollow';
            }

            $isCurrent = $this->isCurrent($section);

            if ($isCurrent) {
                $_attrs['class'] .= ' ' . $attrs['classCurrent'];
            }

            if (!empty($section->children) && $options['showChildrens']) {
                $_attrs['class'] .= ' ' . $attrs['classHasChildrens'];
            }

            $_attrs['class'] .= ' navslug-' . $section->slug;

            if (array_key_exists('template', $attrs)) {
                if (is_callable($attrs['template'])) {
                    $title = $attrs['template']($section);
                } else {
                    $title = vsprintf($attrs['template'], [$section->menuTitle(), $section->subtitle_menu]);
                }
            } else {
                $title = $section->menuTitle();
            }

            $_attrs['escape'] = false;

            $_attrs['class'] = trim($_attrs['class']);

            if ($section->nolink || $section->action == 'container') {
                $li = $this->formatTemplate('tag', [
                    'attrs' => $this->templater()->formatAttributes($_attrs),
                    'tag' => 'a',
                    'content' => $title,
                ]);
            } else {
                if (is_array($url) && !array_key_exists('prefix', $url)) {
                    $url['prefix'] = false;
                }

                $_url = $this->Url->build($url);

                if (!empty($section->anchor) && $section->sectionType->key != 'anchor') {
                    $_url = $_url . '#' . $section->anchor;
                }

                $li = $this->Html->link($title, $_url, $_attrs);
            }

            // Secciones hijas
            if ($options['showChildrens'] && ($options['maxLevels'] == 0 || $level < $options['maxLevels']) && (!empty($section->children) || ($this->isCurrent($section)))) {
                $li .= $this->_nav($section->children, $attrs, ($level + 1), $options, $section);
            }

            if (!empty($options['contentAfter']) && is_callable($options['contentAfter'])) {
                $li .= $options['contentAfter']($section);
            }

            $links[] = [
                'node' => $li,
                'isCurrent' => $isCurrent,
                'section' => $section,
                'hasChildrens' => !empty($section->children) && $options['showChildrens']
            ];
        }


        $attributes = isset($attrs['ul'][$level]) ? $attrs['ul'][$level] : array();

        if (isset($attrs['before'])) {
            $links = array_merge($attrs['before'], $links);
        }

        if (isset($attrs['after'])) {
            $links = array_merge($links, $attrs['before']);
        }

        if (empty($links)) {
            return;
        }

        $li_attrs = isset($attrs['li'][$level]) ? $attrs['li'][$level] : [];
        $options['level'] = $level;

        $ul = $this->buildMenu($links, $li_attrs, $attributes, $options);

        if ($level == 1 && $attrs['wrap_submenu']) {
            return $this->Html->tag($attrs['wrap_submenu']['tag'], $ul, $attrs['wrap_submenu']['attrs']);
        }

        return $ul;
    }


    /**
     * Devuelve un submenú para la sección actual.
     * Ver SectionsTable::getSubnav()
     * 
     * @param  integer $level
     * @return string HTML
     */
    public function subNav($level = 0, $attrs = [], $options = [])
    {
        $path = $this->Sections->getSubNav($this->request->param('section'), $level);
        $return = $this->_nav($path, $attrs, 0, $options);
        return $return;
    }

    public function hasSubNav($level = 0)
    {
        $path = $this->Sections->getSubNav($this->request->param('section'), $level);
        return !empty($path);
    }

    /**
     * Devuelve una sección del path, indicando el nivel
     * 
     * @param  integer $level
     * @return Section\Model\Entity\Section
     */
    public function getSectionFromPath($level)
    {
        $path = $this->Sections->getPath($this->request->param('section'));

        if (isset($path[$level])) {
            return $path[$level];
        }
    }

    /**
     * Devuelve true si la section pasada está en la navegación actual
     * 
     * @param  Section\Model\Entity\Section  $section 
     * @return boolean
     */
    public function isCurrent($section)
    {
        if (!$this->request->param('section')) {
            return false;
        }

        if (empty($this->currentPath)) {
            $this->currentPath = $this->Sections->getPath($this->request->params['section'], [], $this->request->param('slug'));
        }
        $ids = $this->combinePath();
        $slugs = $this->pathSlugs();
        $content_type = $section->content_type ? $section->content_type : 'Sections';
        $section_combi = $section->id . $content_type;

        if ($section->has('is_current') && $section->is_current) {
            $func = $section->is_current;
            $is = $func($this->currentPath, $slugs);

            if ($is) {
                return true;
            }
        }

        $return = (!$section->content_slug && in_array($section_combi, $ids)) || (!empty($section->content_slug) && in_array($section->content_slug, $slugs));
        return $return;
    }

    private function pathSlugs()
    {
        if (!empty($this->pathSlugs)) {
            return $this->pathSlugs;
        }

        $slugs = explode('/', $this->request->url);
        $slugs2 = (new Collection($this->currentPath))->extract('content_slug')->toArray();
        $slugs = array_merge($slugs, $slugs2);

        if ($extra = Configure::read('Section.extraPath')) {
            $slugs = array_merge($slugs, $extra);
        }

        $this->pathSlugs = $slugs;
        return $this->pathSlugs;
    }

    private function combinePath()
    {
        if (!empty($this->combinePath)) {
            return $this->combinePath;
        }

        $combi = (new Collection($this->currentPath))->combine('id', 'content_type')->toArray();
        $return = [];

        foreach ($combi as $key => $content_type) {
            if (!$content_type) {
                $content_type = 'Sections';
            }

            $return[] = $key . $content_type;
        }

        $this->combinePath = $return;
        return $this->combinePath;
    }
    /**
     * Devuelve el menú de navegación para los idiomas
     * Options
     * - current        Si es true mostrará el idioma actual
     * - fullName       Si es true mostrará el nombre del idioma completo (English). Si no, solo las tres primeras letras
     * - currentClass   El nombre del CSS class para el idioma actual
     * - ul_class       El class CSS para la etiqueta <ul>
     * 
     * @param  array  $options [description]
     * @return string HTML
     */
    public function langs($options = [])
    {
        $_options = [
            'current' => false,
            'fullName' => true,
            'currentClass' => 'current',
            'ul_class' => false,
            'li_class' => '',
            'key' => 'name',
            'ul' => true,
            'li' => true,
            'a_class' => false,
            'disable' => []
        ];

        $options += $_options;
        $out = [];

        $langs = Lang::get();
        $links =  Configure::read('I18n.links');
        $langs = array_filter($langs, function ($value) {
            return $value->published;
        });

        if (count($langs) == 1) {
            return;
        }

        foreach ($langs as $lang) {
            if (!$lang->published || in_array($lang->iso2, $options['disable'])) {
                continue;
            }

            $link = @$links[$lang->iso2]['link'];

            if (!$options['current'] && $lang->iso2 == Lang::current()) {
                continue;
            }

            $name = $lang->{$options['key']};

            if (!$options['fullName']) {
                $name = substr($name, 0, 3);
            }

            $attrs = ['title' => $name];

            $attrs['class'] = 'lang-' . $lang->iso2 . ' ' . $options['li_class'];

            if ($lang->iso2 == Lang::current()) {
                $attrs['class'] .= ' ' . $options['currentClass'];
            }

            if ($lang->iso2 == Lang::current()) {
                $out[] = $this->Html->tag('li', '<span>' . $name . '</span>', $attrs);
            } else {
                $link = Router::url($link);

                if (Configure::read('I18n.domains') && Configure::read('I18n.domains.' . $lang->iso2)) {
                    $link = Configure::read('I18n.domains.' . $lang->iso2) . $link;
                }

                if (Configure::read('Manager.preview')) {
                    $url = parse_url($link);

                    if (!array_key_exists('query', $url)) {
                        $query  = [];
                    } else {
                        parse_str($url['query'], $query);
                    }

                    $query['managing'] = true;
                    $link = $url['path'] . '?' . http_build_query($query);
                }

                $a_attrs = [];

                if ($options['a_class']) {
                    $a_attrs['class'] = $options['a_class'];
                }

                $a = $this->Html->link($name, $link, $a_attrs);

                if ($options['li']) {
                    $out[] = $this->Html->tag('li', $a, $attrs);
                } else {
                    $out[] = $a;
                }
            }
        }

        if ($options['ul']) {
            return $this->Html->tag('ul', implode("\n", $out), [
                'class' => $options['ul_class']
            ]);
        } else {
            return implode("\n", $out);
        }
    }

    public function hrefLangs()
    {
        $langs = Lang::get();
        $links =  Configure::read('I18n.links');

        $langs = array_filter($langs, function ($value) {
            return $value->published;
        });

        if (count($langs) == 1) {
            return;
        }

        foreach ($langs as $lang) {
            if (!$lang->published) {
                continue;
            }

            $link = @$links[$lang->iso2]['link'];

            if ($link) {
                $link = Router::url($link);

                if (Configure::read('I18n.domains') && Configure::read('I18n.domains.' . $lang->iso2)) {
                    $link = Configure::read('I18n.domains.' . $lang->iso2) . $link;
                }

                $out[] = '<link rel="alternate" hreflang="' . $lang->iso2 . '" href="' . $link . '" />';
            }
        }

        return implode("\n", $out);
    }

    /**
     * Devuelve el idioma actual, dado una columna
     * 
     * @return string
     */
    public function currentLang($key = 'name')
    {
        return Lang::current($key);
    }

    /**
     * Devuelve el título de la página
     * 
     * @return HTML tag 
     */
    public function pageTitle()
    {
        if (Website::get('settings.use_seo_title') && !empty($this->_metas['title'])) {
            return $this->Html->tag('title', $this->_metas['title']);
        }

        $site = Website::get();
        $section_title = $this->sectionTitle();
        $pageType = $this->getPageType();
        $column = $pageType . '_title_format';
        $title = $site->$column;
        $title = str_replace('%', ':', $title);
        $content = $this->getMainContent();
        $title_content = is_object($content) ? $content->title : '';

        $title = Text::insert($title, [
            's' => $site->title,
            'c' => $section_title,
            'i' => $title_content
        ]);

        return $this->Html->tag('title', strip_tags($title));
    }

    public function getSection()
    {
        if (!empty($this->request->getParam('section'))) {
            return $this->request->getParam('section');
        }
    }

    /**
     * Devuelve el título de la sección
     * 
     * @return string 
     */
    public function sectionTitle($level = false)
    {
        if ($level !== false) {
            $crumbs = Crumbs::get();
            $section = $crumbs[$level];
            return $section['title'];
        }

        if (isset($this->request->params['section'])) {
            $section = $this->request->params['section'];
            $section_title = $section->title;
        } else {
            $section_title = '';
        }

        return strip_tags($section_title);
    }

    public function menuTitle($locale = null)
    {
        $current = Lang::current('iso3');
        $section = $this->request->getParam('section');

        if ($section && $locale !== null && $locale != $current) {
            $this->Sections->setLocale($locale);
            $section = $this->Sections->get($section->id);
            $this->Sections->setLocale($current);
            return $section->menuTitle();
        } else if ($section) {
            return $section->menuTitle();
        }
    }

    /**
     * Toma el contenido principal del web
     * Sirve para usar el contenido en titulos o enlaces a redes sociales
     * 
     * @return Entity 
     */
    public function getMainContent()
    {
        $viewVars = $this->_View->getVars();
        $content = false;

        $variable = strtolower(Inflector::singularize($this->request->controller));

        if (in_array('content', $viewVars)) {
            $content = $this->_View->get('content');
        } elseif (in_array($variable, $viewVars)) {
            $content = $this->_View->get($variable);
        }

        return $content;
    }

    public function getPageType()
    {
        if ($this->getMainContent()) {
            return 'item';
        }

        if ($this->isHomepage()) {
            return 'homepage';
        }

        return 'section';
    }

    public function isHomepage()
    {
        return isset($this->request->params['section']) && $this->request->params['section']->is_homepage;
    }

    public function hasPermissions($section)
    {
        if (!Configure::read('User.hasUsers') || !$section->restricted || !$this->Auth->user()) {
            if (!(($section->restricted || $section->no_access_group) && $section->hide_without_permissions)) {
                return true;
            }
        }

        if (empty($section->groups) && empty($section->no_groups)) {
            return false;
        }

        $access = new stdClass();

        if ($section->restricted && !empty($section->groups)) {
            $groups = collection($section->groups)->extract('id')->toArray();
            $user = $this->Auth->user();
            $access->access = $user && in_array($user['group_id'], $groups);
        }

        if ($section->no_access_group && !empty($section->no_groups)) {
            $groups = collection($section->no_groups)->extract('id')->toArray();
            $user = $this->Auth->user();
            $access->access = !in_array(@$user['group_id'], $groups);
        }

        $event = new Event('Section.Section.hasPermissions', $this, [$access, $section, $user]);
        EventManager::instance()->dispatch($event);
        return $access->access;
    }

    public function user($li_attrs = [])
    {
        $links = [];

        if ($this->Auth->isLogged()) {
            $links[] = $this->Html->tag('span', $this->Auth->user('name'));

            $url = [
                'plugin' => 'User',
                'controller' => 'Users',
                'action' => 'edit'
            ];

            $links[] = $this->sectionLink($url, [], __d('user', 'Editar cuenta'));

            $url = [
                'plugin' => 'User',
                'controller' => 'Users',
                'action' => 'logout'
            ];

            $links[] = $this->sectionLink($url, [], __d('user', 'Salir'));
        } else {
            $url = [
                'plugin' => 'User',
                'controller' => 'Users',
                'action' => 'login'
            ];

            $links[] = $this->sectionLink($url, [], __d('user', 'Entrada'));

            $url = [
                'plugin' => 'User',
                'controller' => 'Users',
                'action' => 'register'
            ];

            $links[] = $this->sectionLink($url, [], __d('user', 'Registro'));
        }

        return $links;
    }

    public function crumbs($options = [])
    {
        return $this->Crumbs->display($options);
    }

    public function buildMenu($links, $li_attrs, $ul_attrs = [], $options = [])
    {
        $out = [];
        $default_options = [
            'ulTag' => 'ul',
            'liTag' => 'li',
        ];

        $options = $options + $default_options;
        $_li_attrs = $li_attrs + [
            'class' => ''
        ];

        $_li_class = $_li_attrs['class'];

        $li_tag = isset($options['liTag']['levels']) ? $options['liTag']['levels'][$options['level']] : $options['liTag'];
        $ul_tag = isset($options['ulTag']['levels']) ? $options['ulTag']['levels'][$options['level']] : $options['ulTag'];

        foreach ($links as $key => $link) {
            $_li_attrs['class'] = $_li_class;

            if ($link['hasChildrens']) {
                $_li_attrs['class'] .= ' has-li-childrens';
            }

            if ($key == 0) {
                $_li_attrs['class'] .= ' first';
            }

            if ($key == count($links) - 1) {
                $_li_attrs['class'] .= ' last';
            }

            if (is_array($link)) {
                $node = $link['node'];

                if ($link['isCurrent']) {
                    $_li_attrs['class'] .= ' current';
                }
            } else {
                $node = $link;
            }

            if (!empty($link['section']->cname)) {
                $_li_attrs['class'] .= ' cname-' . $link['section']->cname;
            }

            if ($li_tag) {
                $out[] = $this->Html->tag($li_tag, $node, $_li_attrs);
            } else {
                $out[] = $node;
            }
        }

        if ($ul_tag) {
            return $this->Html->tag($ul_tag, implode("\n", $out), $ul_attrs);
        } else {
            return implode("\n", $out);
        }
    }

    public function sectionLink($url, $attrs = [], $alt = false)
    {
        $section = RouteData::getSection($url);

        if ($section && is_object($section['section'])) {
            $route = $url;

            if (empty($section['section']->sectionType->actions)) {
                $route['section_id'] = $section['section']->id;
            }

            return $this->Html->link($section['section']->menuTitle(), $route, $attrs);
        } else {
            return $this->Html->link($alt, $url, $attrs);
        }
    }

    public function url($url, $fullbase = false)
    {
        $section = RouteData::getSection($url);
        if ($section && is_object($section['section'])) {
            $route = $url;

            if (empty($section['section']->sectionType->actions)) {
                $route['section_id'] = $section['section']->id;
            }

            if (property_exists($section['section']->sectionType, 'sectionsCategories') && !empty($section['section']->settings) && isset($section['section']->settings->sections_categories)) {
                $route['section_categories'] = (array)$section['section']->settings->sections_categories;
            }

            if (!Configure::read('I18n.disable')) {
                $route['locale'] = Lang::current('iso3');
                $route['lang'] = Lang::current('iso2');
            }

            if (!array_key_exists('prefix', $route)) {
                $route['prefix'] = false;
            }

            return Router::url($route, $fullbase);
        }

        if (!Configure::read('I18n.disable')) {
            $url['locale'] = Lang::current('iso3');
            $url['lang'] = Lang::current('iso2');
        }

        return $url;
    }

    /**
     * Busca una section por cname y devuelve la URL
     * 
     * @param  string $cname 
     * @return string        
     */
    public function cnameUrl($cname, $locale = null)
    {
        $url = $this->Sections->cnameUrl($cname, $locale);

        if (!$url) {
            return '';
        }

        if (substr($url, -1) == '/') {
            $url = substr($url, 0, -1);
        }

        return $url;
    }


    public function urlHome()
    {
        return '/' . $this->currentLang('iso2');
    }

    public function share($options = [])
    {
        $defaults = [
            'shares' => ['twitter', 'facebook', 'googleplus', 'linkedin', 'pinterest'],
            'showLabel' => false,
            'id' => 'share'
        ];

        $options = array_merge($defaults, $options);
        $options['showCount'] = false;
        $return = '<div id="' . $options['id'] . '"></div>';


        if (!$this->request->is('ajax')) {
            $this->Buffer->start();
        }
        echo '<script type="text/javascript">' . '$(function(){$("#' . $options['id'] . '").jsSocials(' . json_encode($options) . ')});</script>';
        if (!$this->request->is('ajax')) {
            $this->Buffer->end();
        }

        return $return;
    }
    /**
     * Retorna una url a partir del Id de una sección
     * 
     * @param  integer $section_id
     * @return string
     */
    public function urlFromSectionId($section_id)
    {
        return $this->Sections->getUrl($section_id);
    }

    /**
     * devuelve el nombre del sitio
     * @return string 
     */
    public function getsitename()
    {
        return Website::get()->title;
    }

    public function getAction()
    {
        return $this->request->getParam('controller') . '.' . $this->request->getParam('action');
    }
    /**
     * Devuelve el valor del website indicado como parámetro
     * 
     * @param  string $key
     * @return mixed
     */
    public function website($key)
    {
        return Website::get($key);
    }

    public function piece($position, $type = 'text', $default = null)
    {
        $trace = debug_backtrace();
        $path_file = $trace[0]['file'];
        $paths = explode('/', $path_file);
        $paths = array_reverse($paths);
        $file = str_replace('.ctp', '', $paths[0]);
        $folder = $paths[1];

        $piece = $this->TL()->get('Section.Pieces')->getPiece([
            'file' => $file,
            'folder' => $folder,
            'position' => $position,
        ]);

        $config = Configure::read('Pieces.collect');

        if (!$config) {
            $config = [];
        }

        $config[] = $piece->id;

        Configure::write('Pieces.collect', $config);

        if ($type == 'text') {
            return '<div class="block block-text b-text">' . $this->Editable->field($piece, 'body', $type, $default) . '</div>';
        } else {
            return $this->Editable->field($piece, 'body', $type, $default);
        }
    }



    public function homelink()
    {
        $return = '/';

        if (!Configure::read('I18n.disable')) {
            $return .= Lang::current('iso2');
        }

        return $return;
    }

    public function setMetas($metas)
    {
        $this->_metas = $metas;
    }

    public function getMeta($key)
    {
        if (array_key_exists($key, $this->_metas)) {
            return $this->_metas[$key];
        }
    }

    public function bodyClass()
    {
        $section = $this->request->param('section');

        if (!$section) {
            return;
        }

        $return = [];

        if (!empty($section->settings->class_body)) {
            $return[] = $section->settings->class_body;
        }

        if ($this->isFullwidth()) {
            $return[] = 'fullwidth-page';
        }

        return implode(' ', $return);
    }

    public function setFullwidth()
    {
        return Configure::write('Section.fullwidth', true);
    }

    public function isFullwidth()
    {
        return Configure::read('Section.fullwidth');
    }

    public function links()
    {
        if (!$this->request->param('section')) {
            return null;
        }

        $links = $this->TL()->get('Section.Links')->find()
            ->where([
                'Links.foreign_key' => $this->request->param('section')->id
            ]);

        return $links;
    }

    public function analytics()
    {
        if ($this->website('settings.google_analytics')) {
            $code = $this->website('settings.google_analytics');
            $script = <<<EOF
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '$code', 'auto');
      ga('send', 'pageview');
EOF;
            return $this->Html->scriptBlock($script);
        }
    }


    public function webPhone($content = null, $class = null)
    {
        $phone = $this->website('settings.phone');
        $phones = explode(',', $phone);
        $phones = array_map('trim', $phones);
        $out = [];

        foreach ($phones as $phone) {
            if ($content ===  null) {
                $_content = $phone;
            } else {
                $_content = $content;
            }

            $class_tag = $class ? ' class="' . $class . '"' : '';

            $out[] = '<a' . $class_tag . ' href="tel:' . str_replace(' ', '', $phone) . '">' . $_content . '</a>';
        }

        return implode("\n", $out);
    }

    public function webEmail($content = null, $class = null)
    {
        $email = $this->website('email');
        $emails = explode(',', $email);
        $emails = array_map('trim', $emails);

        $out = [];

        foreach ($emails as $email) {
            if ($content === null) {
                $content = $email;
            }

            $class_tag = $class ? ' class="' . $class . '"' : '';

            $out[] = '<a' . $class_tag . ' href="mailto:' . $email . '">' . $content . '</a>';
        }

        return implode("\n", $out);
    }

    public function TL()
    {
        return TableRegistry::getTableLocator();
    }
}
