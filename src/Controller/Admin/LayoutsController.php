<?php
namespace Section\Controller\Admin;

use Section\Controller\AppController;
use Manager\Controller\CrudControllerTrait;
use Cake\Core\Configure;
use Section\Layout\LayoutCollection;
use Cake\Utility\Hash;
use Cake\Collection\Collection;

/**
 * Layouts Controller
 *
 * @property \Section\Model\Table\LayoutsTable $Layouts
 */
class LayoutsController extends AppController
{
  use CrudControllerTrait;

  public function add()
  {
    $this->CrudTool->addSerialized( [
      'templates' => LayoutCollection::get()
    ]);
  }

  protected function _beforeCreate()
  {
    if( isset( $this->request->params ['pass'][0]))
    {
      $template = LayoutCollection::get( $this->request->params ['pass'][0]);
      $wrappers = [];

      foreach( $template->wrappers as $key => $wrapper)
      {
        $content = $this->__prepareWrapper( $wrapper, $key);
        $wrappers [] = $this->Layouts->Wrappers->emptyEntityProperties( $content);
      }

      $this->Table->crud->setContent( [
        'wrappers' => $wrappers,
      ]);

      $this->__setWrappersName();
    }
  }

  private function __prepareWrapper( $wrapper, $cname)
  {
    if( !empty( $wrapper->defaultBlock))
    {
      $this->Layouts->Wrappers->removeBehavior( 'Blockable');
      $this->Layouts->Wrappers->addBehavior( 'Block.Blockable', [
        'defaults' => [
          $wrapper->defaultBlock
        ]
      ]);
    }
    else
    {
      $this->Layouts->Wrappers->removeBehavior( 'Blockable');
      $this->Layouts->Wrappers->addBehavior( 'Block.Blockable');
    }
    
    $content = $this->Layouts->Wrappers->getNewEntity([
      'cname' => $cname
    ]);

    return $content;
  }

  protected function _create()
  {
    if( isset( $this->request->params ['pass'][0]))
    {
      $this->Table->crud->setContent( [
        'template' => $this->request->params ['pass'][0]
      ]);
    }
  }

  protected function _beforeUpdate()
  {
    $this->__setWrappersName();
  }

  protected function _update()
  {
    // Comprueba que están todos los wrappers descritos en el json del layout
    $content = $this->Table->crud->getContent();
    $template = LayoutCollection::get( $content ['template']);
    $collection = new Collection( $content ['wrappers']);

    foreach( $template->wrappers as $cname => $wrapper)
    {
      $has = $collection->firstMatch(['cname' => $cname]);
      
      if( !$has)
      {
        $new = $this->__prepareWrapper( $wrapper, $cname);
        $content ['wrappers'][] = $this->Layouts->Wrappers->emptyEntityProperties( $new);
      }
    }

    $this->Table->crud->setContent([
      'wrappers' => $content ['wrappers']
    ]);
  }

  private function __setWrappersName()
  {
    $configure = LayoutCollection::get();
    $this->CrudTool->addSerialized( ['wrapperNames' => $configure]);
  }

}
