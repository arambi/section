<?php
namespace Section\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Section\Controller\AppController;

/**
 * Links Controller
 *
 * @property \Section\Model\Table\LinksTable $Links
 */
class LinksController extends AppController
{
    use CrudControllerTrait;
}
