<?php
namespace Section\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Section\Controller\AppController;

/**
 * Pieces Controller
 *
 * @property \Section\Model\Table\PiecesTable $Pieces
 */
class PiecesController extends AppController
{
    use CrudControllerTrait;
}
