<?php

namespace Section\Controller\Admin;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Block\Lib\BlocksRegistry;
use Cake\ORM\TableRegistry;
use Section\Action\ActionCollection;
use Section\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Sections Controller
 *
 * @property Section\Model\Table\SectionsTable $Sections
 */
class SectionsController extends AppController
{
    use CrudControllerTrait;

    /**
     * Listado de secciones ordenadas por menús y anidadas
     * Los menús vienen dados desde App/config/section.php
     * 		
     * @return void
     */
    public function index()
    {
        ActionCollection::autocreate();
        $this->Sections->crud->action('index');
        $this->CrudTool->addSerialized([
            'contents' => $this->Sections->getSections(),
            'actionTypes' => ActionCollection::types(),
        ]);
    }

    /**
     * Muestra las opciones de creación de una section
     * El listado de opciones viene dado desde App/config/section.php
     * 
     * @return void
     */
    public function add($menu = 'default', $parent_id = 0)
    {
        $this->CrudTool->addSerialized([
            'actions' => ActionCollection::get(),
            'time' => time()
        ]);

        $this->Sections->crud->setContent([
            'menu' => $menu,
            'parent_id' => $parent_id,
        ]);
    }

    /**
     * Llamado desde CrudCrontrollerTrait cuando se crea guarda registro
     * Usado para hacer cualquier tipo de modificación
     * 
     * @return void
     */
    private function _afterSave($content, $created = false)
    {
        $this->CrudTool->addSerialized([
            'updateScope' => [
                'contents' => $this->Sections->getSections()
            ]
        ]);
    }


    private function createBlocks($blocks)
    {
        $this->Sections->crud->defaults(function ($data) use ($blocks) {
            $data['rows'] = [];
            foreach ($blocks as $key => $name) {
                $block = BlocksRegistry::get($name);
                $class = $block['className'];
                $Block = new $class();

                if (method_exists($Block, 'defaults')) {
                    $newblock = $Block->defaults();
                } else {
                    $newblock = [
                        'subtype' => $name,
                        'position' => 1,
                    ];
                }

                $newblock['block_type'] = $block;
                $newblock['published'] = true;
                $tableBlocks = TableRegistry::getTableLocator()->get('Block.Blocks');
                $entity = $tableBlocks->newEntity($newblock);
                $tableBlocks->save($entity);
                $data['rows'][$key] = [
                    'position' => 1,
                    'published' => true,
                    'columns' => [
                        [
                            'published' => true,
                            'blocks' => [
                                0 => $entity->toArray()
                            ],
                            'position' => 1,
                            'cols' => 12,
                        ]
                    ]
                ];
            }
            return $data;
        });
    }

    protected function _beforeCreate($data)
    {
        if ($this->request->getParam('pass.0')) {

            $defaults = ['text', 'custom_text'];
            $action = ActionCollection::get($this->request->getParam('pass.2'));

            if (!empty($action->blockDefaults)) {
                $this->createBlocks($action->blockDefaults);
            }
        }
    }

    private function _create()
    {
        if (isset($this->request->params['pass'][0])) {
            $type = $this->request->params['pass'][0];
            $this->Sections->crud->setContent([
                'menu' => $type,
                'menuType' => collection(Configure::read('Section.menus'))->firstMatch(['key' => $type]),
                'parent_id' => $this->request->params['pass'][1],
                'action' => $this->request->params['pass'][2],
                'sectionType' => ActionCollection::get($this->request->params['pass'][2])
            ]);
        }

        if (isset($this->request->params['pass'][2])) {
            $this->__setContentId(ActionCollection::get($this->request->params['pass'][2]));
        }
    }

    protected function _update($content)
    {
        $this->__setMenu($content->menu);
        $this->__setContentId($content->sectionType);
    }


    private function __setContentId($sectionType)
    {
        if (!empty($sectionType->content_id)) {
            $this->Table->crud->addFields([
                'content_id' => $sectionType->content_id
            ]);
        }
    }

    private function __setMenu($menu)
    {
        ActionCollection::autocreate();

        $this->CrudTool->addSerialized([
            'actionTypes' => ActionCollection::types(),
            'menuType' => collection(Configure::read('Section.menus'))->firstMatch(['key' => $menu]),
        ]);
    }

    public function _beforeUpdateFind($query)
    {
        $query->formatResults(function ($results) {
            return $results->map(function ($result) {
                $result['url'] = Router::url($this->Sections->getUrl($result['id']), true);
                return $result;
            });
        });
    }

    public function order()
    {
        $this->Sections->reordering($this->request->data);
        $this->autoRender = false;
        return $this->response;
    }
}
