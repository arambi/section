<?php
namespace Section\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Section\Controller\AppController;

/**
 * Redirects Controller
 *
 * @property \Section\Model\Table\RedirectsTable $Redirects
 */
class RedirectsController extends AppController
{
    use CrudControllerTrait;
}
