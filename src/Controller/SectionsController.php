<?php
namespace Section\Controller;

use Cake\Event\Event;
use Cake\Routing\Router;
use Section\Seo\Sitemap;
use Cake\ORM\TableRegistry;
use Section\Controller\AppController;

/**
 * Sections Controller
 *
 * @property \Section\Model\Table\SectionsTable $Sections
 */
class SectionsController extends AppController
{
  public function initialize()
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
  }

  public function beforeFilter( Event $event)
  {
    parent::beforeFilter( $event);
    $this->Auth->allow( ['sitemap']);

    if( $this->request->is( 'ajax'))
    {
      $this->viewBuilder()->layout( 'ajax');
    }
  }

  /**
   * Index method
   *
   * @return void
   */
  public function index()
  {
    $this->set( 'sections', $this->paginate($this->Sections));
  }


  public function view()
  {
    
  }

  public function sitemap()
  {
    set_time_limit( 120);
    ini_set('memory_limit', '255M');

    $sitemap = new Sitemap();
    $nodes = $sitemap->create();
    $this->set( compact( 'nodes'));
  }

  public function file()
  {
    $section = $this->getRequest()->getParam( 'section');

    if( !empty( $section->file))
    {
      $file = $section->file->currentLocale();

      if( $file)
      {
        return $file->download( $this->getResponse());
      }
    }

    $this->Section->noFound();
  }

  public function access()
  {
    $request = $this->getRequest();
    $session = $request->getSession();
    $section_id = $session->read( 'SectionAccess.accessId');

    $section = $this->Sections->findById( $section_id)->first();
      
    if( !$section)
    {
      $this->notFound();
    }
    
    if( $request->is( 'post'))
    {
      $password = $request->getData( 'pwd');

      if( $section && $section->pwd == $password)
      {
        $url = $this->Sections->getUrl( $section);
        $session->write( 'SectionAccess.section.'. $section->id, true);
        $session->delete( 'SectionAccess.accessId');
        $this->redirect( $url);
      }
      else
      {
        $this->set( 'error', true);
      }
    }
  }
}
