<?php
namespace Section\Controller\Component;

use I18n\Lib\Lang;
use Cake\I18n\I18n;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Section\Crumbs\Crumbs;
use Cake\ORM\TableRegistry;
use Cake\Routing\Route\Route;
use Cake\Controller\Component;
use Section\Routing\RouteData;
use Section\Routing\UrlBuilder;
use Section\Routing\SectionsRoutes;
use Cake\Controller\ComponentRegistry;
use Cake\Network\Exception\NotFoundException;

/**
 * Section component
 */
class SectionComponent extends Component
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

  public $components = ['Auth'];

  const EXCEPTIONS_TO_REDIRECT_LANG = [
    '/sitemap.xml',
    '/upload/uploads/uploadfile',
    '/robots.txt'
  ];

  const EXCEPTIONS_PLUGINS = [
    'DebugKit',
  ];

  const EXTENSIONS_EXCEPT = [
    '.jpg',
    '.jpeg',
    '.gif',
    '.js',
    '.css',
    '.map',
    '.png',
    '.woff',
    '.eot',
    '.ttf',
    '.otf',
  ];

/**
 * Busca la sección actual y busca también el layout vinculado o el por defecto
 * 
 * @param  Event  $event
 * @return void
 */
  public function beforeFilter( Event $event)
  {
    $this->Sections = TableRegistry::get( 'Section.Sections');
    $this->Controller = $event->getSubject();
    $this->__setSection();
    $this->observeLocale();
    $this->observeSlash();
    $this->__addHelpers();
    $this->__checkAccess();
    $this->__checkPasswordAccess();
    setlocale( LC_ALL, Lang::current( 'locale') .'.UTF-8');
  }

  private function observeLocale()
  {
    $request = $this->Controller->getRequest();

    if( in_array( $request->getParam( 'plugin'), self::EXCEPTIONS_PLUGINS))
    {
      return;
    }

    if( $this->hasExtensionException())
    {
      return;
    }

    if( !$request->getParam( 'section'))
    {
      return;
    }

    if( !Configure::read( 'I18n.disable') && !$request->is( 'ajax') && !$request->is( 'json') && $request->getParam( 'prefix') != 'admin')
    {
      $lang = $request->getParam( 'lang');

      if (!$lang) {
        $lang = $this->getLangParam();
      }

      $url = $request->getAttribute( 'here') == '/' ? '' : $request->getAttribute( 'here');

      if( !$lang && !in_array( $request->getAttribute( 'here'), self::EXCEPTIONS_TO_REDIRECT_LANG))
      {
        if( $request->getAttribute( 'country'))
        {
          $prefix = '/'. $request->getAttribute( 'country') .'-'. Lang::current( 'iso2'); 
        }
        else
        {
          $prefix = '/'. Lang::current( 'iso2'); 
        }
        $this->Controller->redirect( $prefix . $url);
      }
      elseif( Configure::read( 'Country') && !$request->getAttribute( 'country'))
      {
        $url = '/'. Configure::read( 'Country') . '-'. substr( $url, 1);
        $this->Controller->redirect( $url);
      }
    }
  }

  private function getLangParam()
  {
    $request = $this->Controller->getRequest();
    if ($request->getAttribute('here') == '/') {
      return false;
    }
    
    $parts = explode('/', $request->getAttribute('here'));
    
    $langs = collection(Lang::get(true))->extract('iso2')->toArray();

    if (!Configure::read( 'Country')) {
      if (isset($parts[1]) && in_array($parts[1], $langs)) {
        return $parts[1];
      }
    } else {
      if (isset($parts[1])) {
        [$country, $lang] = explode('-', $parts[1]);

        if (!empty($lang) && in_array($lang, $langs)) {
          return $lang;
        }
      }
    }

  }

  private function hasExtensionException()
  {
    $request = $this->Controller->getRequest();
    $url = strtolower( $request->getAttribute( 'here'));
    
    if( strpos( $url, '.') !== false)
    {
      foreach( self::EXTENSIONS_EXCEPT as $ext)
      {
        if( strpos( $url, $ext) !== false)
        {
          return true;
        }
      }
    }

    return false;
  }

  private function observeSlash()
  {
    $request = $this->Controller->getRequest();

    if( !$request->is( 'ajax') && !$request->is( 'json') && $request->getParam( 'prefix') != 'admin')
    {
      if( $request->getAttribute( 'here') != '/' && substr( $request->getAttribute( 'here'), -1) == '/')
      {
        $this->Controller->redirect( substr( $request->getAttribute( 'here'), 0, -1));
      }
    }
  }

/**
 * Setea la sección actual y el layout
 * @return void
 */
  private function __setSection()
  {
    $request = $this->Controller->request;
    $url = $request->url;

    if( isset( $request->params ['prefix']) &&  $request->params ['prefix'] == 'admin')
    {
      return;
    }

    if( isset( $request->params ['lang']) && substr( $url, 0, 2) == $request->params ['lang'])
    {
      $url = substr( $url, 2);
    }

    $route = RouteData::current();

    if( $route)
    {
      $params = RouteData::get( $route->template);
      $this->addParams( $params);
    }
    else
    {
      $this->addParamDefaultLayout();
    }    

    if( isset( $request->params ['layout']))
    {
      $this->Controller->viewBuilder()->layout( $request->params ['layout']->template);
    } 

    $this->__setLocaleLinks();
  }

  private function __checkAccess()
  {
    $request = $this->Controller->request;
    $section = $request->getParam( 'section');

    if( !$section) {
      return;
    }

    if( !$section->restricted && !$section->no_access_group)
    {
      $this->Auth->allow();
    } else {
      if( $section->no_access_group && !empty( $section->no_groups)) {
        $groups = collection( $section->no_groups)->extract( 'id')->toArray();
        
        if( !in_array( $this->Auth->user( 'group_id'), $groups)) {
          $this->Auth->allow();
          return;
        }
      }

      $this->Auth->deny();
    }
  }

  private function __checkPasswordAccess()
  {
    $request = $this->Controller->request;
    $section = $request->getParam( 'section');

    if( !$section)
    {
      return;
    }

    if( !Configure::read( 'Section.withPassword') || !$section->with_pwd)
    {
      return;
    }

    $session = $this->Controller->getRequest()->getSession();
    
    if( $session->read( 'SectionAccess.section.'. $section->id))
    {
      return;
    }

    $session->write( 'SectionAccess.accessId', $section->id);

    $this->Controller->redirect( $this->url([
      'plugin' => 'Section',
      'controller' => 'Sections',
      'action' => 'access',
    ]));
  }


  private function __setLocaleLinks()
  {
    $request = $this->Controller->request;

    if( !$section = $request->param( 'section'))
    {
      return;
    }

    $path = $this->Sections->getPath( $section);
    $_section = collection( $path)->nest( 'id', 'parent_id')->toArray();
    SectionsRoutes::addSection( $_section [0], '', false, true);
    SectionsRoutes::dynamics();

    $links = [];

    foreach( Lang::get( true) as $language)
    {
      $locale = $language->iso3;
      $name = $language->name;

      if( $locale != Lang::current( 'iso3'))
      {
        $params = $request->params;
        unset( $params ['_matchedRoute']);
        
        $params ['locale'] = $locale;

        if( $section->action == 'entry')
        {
          $params ['section_id'] = $section->id;
        }

        $matched = $request->param( '_matchedRoute');
        
        try {
          $url = Router::url( $params);

          if( Configure::read( 'Country'))
          {
            $locale_url = substr( $url, 0, 6);
            $locale_url = substr( $locale_url, 0, 4) . Lang::getIso2( $locale);
            $links [Lang::getIso2( $locale)] = [
              'name' => $name,
              'link' => $locale_url . substr( $url, 6)
            ];
          }
          else 
          {
            $links [Lang::getIso2( $locale)] = [
              'name' => $name,
              'link' => '/'. Lang::getIso2( $locale) . substr( $url, 3)
            ];
          }
        } catch (\Exception $e) {
          
        }
      }
      else
      {
        $links [Lang::getIso2( $locale)] = [
          'name' => $name,
          'link' => false
        ];
      }
    }

    Configure::write( 'I18n.links', $links);
  }

  private function __addHelpers()
  {
    $this->Controller->helpers [] = 'Section.Nav';
    $this->Controller->helpers [] = 'Upload.Uploads';
  }
/**
 * Añade al Request el parámetro `section` que es la entidad de Layout
 * También añade el parámetro 'content_id'
 * 
 * @param integer $section_id
 */
  public function addParams( $params)
  {
    $request = $this->Controller->request;

    if( is_array($params) && array_key_exists('section_id', $params)) {
      $section = $this->Sections->findSection( $params ['section_id']);
    } else {
      $section = null;
    }

    if( $section && $section->show_children)
    {
      $this->Controller->redirect( $this->Sections->getUrl( $section->show_children_id));
    }

    $request->addParams( ['section' => $section]);

    if( $section)
    {
      $this->addLayoutParam( $section);
      $path = $this->Sections->getPath( $section, [], $request->param( 'slug'));
      $parts = explode( '/', $request->url);

      foreach( $path as $_section)
      {
        if( $_section->in_crumbs)
        {
          $params = UrlBuilder::sectionParams( $_section, null);

          if( empty( $_section->sectionType->actions) || !empty( $_section->content_id))
          {
            $params ['section_id'] = $_section->id;
          }

          // $paramsUrl = $this->Sections->paramsUrl( $params);
          if (!in_array($_section->action, ['container', 'anchor'])) {
            Crumbs::add( $_section->title, Router::url($params));
          }
        }
        
      }

      if( $section->fullwidth)
      {
        Configure::write( 'Section.fullwidth', true);
      }
    }
    else
    {
      $this->addParamDefaultLayout();
    }
    return $section;
  }

/**
 * Añade al Request el parámetro `layout` que es la entidad de Layout
 * 
 * @param Section\Model\Entity\Section $section
 */
  public function addLayoutParam( $section)
  {
    $request = $this->Controller->request;

    if( $section->has_action_layouts && isset( $section->action_layouts->{$request->action}))
    {
      $layout_id = $section->action_layouts->{$request->action};
    }
    else
    {
      $layout_id = $section->layout_id;
    }

    $layout = TableRegistry::get( 'Section.Layouts')->findLayout( $layout_id);

    foreach( $layout->wrappers as $wrapper)
    {
      if( $wrapper->fullwidth)
      {
        Configure::write( 'Section.fullwidth', true);
      }
    }

    $request->addParams( ['layout' => $layout]);
  }

/**
 * Añade al Request el parámetro `layout` que es la entidad de Layout. Toma el layout por defecto
 */
  public function addParamDefaultLayout()
  {
    $request = $this->Controller->request;
    $request->addParams( ['layout' => TableRegistry::get( 'Section.Layouts')->findDefault()]);
  }


  public function url( $url)
  {
    $section = RouteData::getSection( $url);
// _d( $section);
    if( $section && is_object( $section ['section']))
    {
      $route = $url;

      if( empty( $section ['section']->sectionType->actions))
      {
        $route ['section_id'] = $section ['section']->id;
      }

      return Router::url( $route);      
    }

    return $url;
  }

  public function notFound( $msg = 'Página no encontrada')
  {
    throw new NotFoundException( $msg);
  }

}
