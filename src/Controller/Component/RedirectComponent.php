<?php

namespace Section\Controller\Component;

use Cake\Event\Event;
use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Redirect component
 */
class RedirectComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function beforeFilter(Event $event)
    {
        $path = env('REQUEST_URI');
        $table = TableRegistry::getTableLocator()->get('Section.Redirects');
        $redirect = $table
            ->findByOriginUrl($path)
            ->first();

        if ($redirect) {
            $redirect->counter++;
            $table->save($redirect);
            return $this->getController()->redirect($redirect->redirect_url);
        }
    }
}
