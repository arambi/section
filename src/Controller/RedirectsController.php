<?php
namespace Section\Controller;

use Section\Controller\AppController;

/**
 * Redirects Controller
 *
 * @property \Section\Model\Table\RedirectsTable $Redirects
 */
class RedirectsController extends AppController
{
  
  public function initialize() 
  {
    parent::initialize();
    $this->loadComponent( 'RequestHandler');
    $this->Auth->allow();
  }

  public function index()
  {
    $query = $this->Redirects
      ->find( 'front')
    ;

    $contents = $this->paginate( $query);
    $this->set( compact( 'contents'));
  }

  public function view()
  {
    $content = $this->Redirects
      ->find( 'front')
      ->find( 'slug', ['slug' => $this->request->getParam( 'slug')])
      ->first();
    
    if( !$content)
    {
      $this->Section->notFound();
    }

    $this->set( compact( 'content'));
  }
}
