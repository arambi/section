<?php 

namespace Section\Crumbs;


class Crumbs
{
  protected static $_crumbs = [];

  public static function add( $title, $url)
  {
    static::$_crumbs [] = [
      'title' => $title,
      'url' => $url
    ];
  }

  public static function get()
  {
    return static::$_crumbs;
  }

  public static function reset()
  {
    static::$_crumbs = [];
  }
}