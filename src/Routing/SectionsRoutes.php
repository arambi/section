<?php

namespace Section\Routing;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Section\Routing\RouteData;
use Cake\ORM\TableRegistry;
use I18n\Lib\Lang;

/**
 * 
 */
class SectionsRoutes
{

    /**
     * Variable donde se almacenan las routas dinámicas, que se serán ejecutadas al final
     * @var array
     */
    protected static $_dynamics = [];

    protected static $_routeClass = 'I18n.I18nRoute';

    public static function setRouteClass($routeClass)
    {
        static::$_routeClass = $routeClass;
    }

    public static function add($section, $legacy_path, $slug, $with_slugs = false)
    {
        $params = [
            'routeClass' => static::$_routeClass
        ];

        if (empty($section->slug)) {
            return;
        }

        if (in_array($section->action, ['anchor', 'container'])) {
            return;
        }


        if ($with_slugs) {
            $path = $legacy_path . '/' . $slug->slug;
        } else {
            $path = $legacy_path . '/' . $section->slug;
        }

        $homepage_path = false;

        // Página principal
        if ($section->action == 'reference') {
            return;
        }

        if ($section->is_homepage == 1) {
            $homepage_path = $path;
            $path = '/';
        }

        if (!is_object($section->sectionType)) {
            return;
        }

        $url = array(
            'plugin' => $section->sectionType->plugin,
            'controller' => $section->sectionType->controller,
            'action' => $section->sectionType->action,
        );

        if (property_exists($section->sectionType, 'prefix')) {
            $url['prefix'] = $section->sectionType->prefix;
        }

        if (property_exists($section->sectionType, 'sectionsCategories') && is_object($section->settings) && isset($section->settings->sections_categories)) {
            $url['action'] = 'categorized';
            $url['section_categories'] = (array)$section->settings->sections_categories;
        }

        RouteData::addSection($url, $section);

        $_url = $url;
        $_path = $path;

        if (empty($section->sectionType->actions) || !empty($section->content_id)) {
            $url['section_id'] = $section->id;
        }

        if ($section->sectionType->wildcard) {
            $_path .= '/*';
        }

        if (!Configure::read('I18n.disable')) {
            if ($with_slugs) {
                $url['locale'] = $slug->locale;
            } else {
                $url['locale'] = Lang::current('iso3');
            }
        }

        Router::connect($_path, $url, $params);

        $final_path = $_path;

        if (!Configure::read('I18n.disable')) {
            $final_path = '/:lang' . $final_path;
        }


        RouteData::add($final_path, [
            'content_id' => $section->content_id,
            'section_id' => $section->id,
        ]);


        if (isset($section->sectionType->actions)) {
            if ($section->is_homepage == 1) {
                $_path = $homepage_path;
            }

            foreach ($section->sectionType->actions as $action => $_params) {
                if (!empty($section->content_id)) {
                    $_url['content_id'] = $section->content_id;
                }

                $options = [
                    'action' => $action
                ];

                if (!Configure::read('I18n.disable')) {
                    if ($with_slugs) {
                        $options['locale'] = $slug->locale;
                    } else {
                        $options['locale'] = Lang::current('iso3');
                    }
                }

                // Las routes dinámicas se almacenan en esta variable para ser conectadas más tarde
                static::$_dynamics[] = [
                    'route' => $_path . $_params,
                    'params' => array_merge($_url, $options),
                    'options' => $params
                ];

                // Router::connect( $_path . $_params, array_merge( $_url, ['action' => $action]), $params);

                $final_path = $_path;

                if (!Configure::read('I18n.disable')) {
                    $final_path = '/:lang' . $final_path;
                }

                RouteData::add($final_path . $_params, [
                    'content_id' => $section->content_id,
                    'section_id' => $section->id,
                ]);
            }
        }
    }

    /**
     * Realiza el Router::connect para las rutas dinámicas
     */
    public static function dynamics()
    {
        foreach (static::$_dynamics as $route) {
            Router::connect($route['route'], $route['params'], $route['options']);
        }

        static::$_dynamics = [];
    }


    public static function nestedRoutes($sections, $path = '', $locale = false, $with_slugs = false)
    {
        foreach ($sections as $section) {
            if (!$section->is_content) {
                self::addSection($section, $path, $locale, $with_slugs);
            }
        }
    }

    public static function addSection($section, $path, $locale, $with_slugs = false)
    {
        if ($with_slugs && !empty($section->slugs)) {
            foreach ($section->slugs as $slug) {
                if (!$locale || $slug->locale == $locale) {
                    self::add($section, $path, $slug, true);
                }
            }

            if (!empty($section->children)) {
                foreach ($section->slugs as $slug) {
                    if (!$locale || $slug->locale == $locale) {
                        self::nestedRoutes($section->children, $path . '/' . $slug->slug, $slug->locale, $with_slugs);
                    }
                }
            }
        } else {
            self::add($section, $path, $section->slug);

            if (!empty($section->children)) {
                self::nestedRoutes($section->children, $path . '/' . $section->slug, $with_slugs);
            }
        }
    }
}
