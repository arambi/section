<?php

namespace Section\Routing;

use Cake\ORM\TableRegistry;

class UrlBuilder
{
    public static function sectionParams($section, $parent)
    {
        if ($section->sectionType->key == 'external') {
            return $section->external_url;
        }

        if ($section->sectionType->childrensTable) {
            if (is_callable($section->sectionType->childrensTable['action'])) {
                return $section->sectionType->childrensTable['action']($section, $parent);
            }

            $action = $section->content_slug ? $section->sectionType->childrensTable['action'] : $section->sectionType->action;
            $slug = $section->content_slug;

            return [
                'plugin' => $section->sectionType->plugin,
                'controller' => $section->sectionType->controller,
                'action' => $action,
                $section->sectionType->childrensTable['param'] => $slug
            ];
        } else {
            if ($section->action == 'container') {
                $section = TableRegistry::getTableLocator()->get('Section.Sections')->find('front')
                    ->where([
                        'Sections.parent_id' => $section->id
                    ])
                    ->first();
            }

            $action = $section->sectionType->action;

            return [
                'plugin' => $section->sectionType->plugin,
                'controller' => $section->sectionType->controller,
                'action' => $action,
            ];
        }
    }
}
