<?php

namespace Section\Routing; 

use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Collection\Collection;
/**
* 
*/
class RouteData
{
  protected static $_routes = [];

  protected static $_current;

  protected static $_sections = [];

  protected static $_collection;

  public static function add( $url, $data)
  {
    if( substr( $url, -1) == '/' && !Configure::read( 'I18n.disable'))
    {
      $url = substr( $url, 0, strlen( $url) - 1);
    }

    static::$_routes [$url] = $data;
  }

  public static function addSection( $url, $section)
  {
    static::$_sections [] = [
      'section' => $section,
      'url' => $url
    ];
  }

  public static function getSection( $url)
  {
    if( empty( static::$_collection))
    {
      self::$_collection = new Collection( self::$_sections);
    }

    if( isset( $url ['lang']))
    {
      unset( $url ['lang']);
    }

    if( isset( $url ['pass']))
    {
      unset( $url ['pass']);
    }

    foreach( $url as $key => $value)
    {
      if( !empty( $value))
      {
        $match ['url.' . $key] = $value;
      }
      
    }

    $section = self::$_collection->firstMatch( $match);

    return $section;
  }

  public static function get( $url)
  {
    if( isset( self::$_routes [$url]))
    {
      return self::$_routes [$url];
    }

    return false;
  }

  public static function url( $url, $full = false)
  {
    $section = self::getSection( $url);

    if( isset( $section ['section']))
    {
      $url ['section_id'] = $section ['section']->id;
      return Router::url( $url, $full);
    }

    return Router::url( $url, $full);
  }

  public static function all()
  {
    return self::$_routes;
  }

  public static function current( $route = false)
  {
    if( $route)
    {
      static::$_current = $route;
    }

    return static::$_current;
  }

  public static function sectionTitle( $url)
  {
    $section = self::getSection( $url);

    if( $section && is_object( $section ['section']))
    {
      $route = $url;

      if( empty( $section ['section']->sectionType->actions))
      {
        $route ['section_id'] = $section ['section']->id;
      }

      return $section ['section']->menuTitle();    
    }
    
    return false;
  }
  
}
