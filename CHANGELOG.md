## 2015-02-07

### Añadido subtítulo para Sections
Añadido subtítulo para los menús de las secciones

### Añadido template para NavHelper::nav()
Se puede usar colocando la clave "template" en el array del segundo argumento, donde usando % % es el title_menu y el subtitle_menu respectivamente
p.e. 'template' => "<strong>%s<span>%s</span></strong>" nos dará <strong>Título del menú<span>Subtítulo del menú</span></strong>
