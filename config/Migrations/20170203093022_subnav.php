<?php

use Phinx\Migration\AbstractMigration;

class Subnav extends AbstractMigration
{

  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
      ->addColumn( 'show_submenu', 'boolean', ['default' => 1, 'null' => false])
      ->save();
  }
}
