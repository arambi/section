<?php

use Phinx\Migration\AbstractMigration;

class Header extends AbstractMigration
{

  public function change()
  {
    $sections = $this->table( 'sections');

    if( !$sections->hasColumn( 'header'))
    {
      $sections->addColumn( 'header', 'text', ['default' => null, 'null' => true]);
    }
    
    if( !$sections->hasColumn( 'header_background'))
    {
      $sections->addColumn( 'header_background', 'text', ['default' => null, 'null' => true]);
    }

    if( !$sections->hasColumn( 'page_background'))
    {
      $sections->addColumn( 'page_background', 'text', ['default' => null, 'null' => true]);
    }
    
    $sections->save();
  }
}
