<?php

use Phinx\Migration\AbstractMigration;

class SectionReference extends AbstractMigration
{
  
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
      ->addColumn( 'reference_id', 'integer', ['null' => true, 'default' => null])
      ->save();
  }
}
