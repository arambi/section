<?php

use Phinx\Migration\AbstractMigration;

class Subtitle extends AbstractMigration
{
  public function up()
  {
    $sections = $this->table( 'sections');
    $sections->addColumn( 'subtitle_menu', 'string', ['default' => null, 'null' => true])->save();
  }

  public function down()
  {
    $sections = $this->table( 'sections');
    $sections->removeColumn( 'subtitle_menu')->save();;
  }
}
