<?php

use Phinx\Migration\AbstractMigration;

class Published extends AbstractMigration
{
  public function up()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'published', 'boolean', ['default' => 1, 'null' => false])
        ->update();
  }

  public function down()
  {
    $sections = $this->table( 'sections');
    $sections->removeColumn( 'published');
  }
}
