<?php

use Phinx\Migration\AbstractMigration;

class ChildrenShow extends AbstractMigration
{
  public function up()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'show_children', 'boolean', ['default' => 0, 'null' => true])
        ->addColumn( 'show_children_id', 'integer', ['default' => null, 'null' => true])
        ->update();
  }

  public function down()
  {
    $sections = $this->table( 'sections');
    $sections->removeColumn( 'show_children');
    $sections->removeColumn( 'show_children_id');
  }
}
