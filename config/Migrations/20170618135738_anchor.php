<?php

use Phinx\Migration\AbstractMigration;

class Anchor extends AbstractMigration
{
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
      ->addColumn( 'anchor', 'string', ['default' => null, 'null' => true])
      ->save();
  }
}
