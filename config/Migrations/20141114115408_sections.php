<?php

use Phinx\Migration\AbstractMigration;

class Sections extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
     * 
    public function change()
    {
      $this->table( 'sections')->addColumn( 'has_action_layouts', 'boolean', ['default' => 0, 'null' => false])->update();
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $sections = $this->table( 'sections');
        $sections
            ->addColumn( 'site_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'layout_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'action_layouts', 'text', ['default' => NULL, 'null' => true])
            ->addColumn( 'has_action_layouts', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'title', 'string', ['null' => false])
            ->addColumn( 'title_menu', 'string', ['default' => null, 'null' => true])
            ->addColumn( 'parent_id', 'integer', ['default' => 0, 'null' => false])
            ->addColumn( 'content_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'position', 'integer', ['limit' => 6, 'null' => false])
            ->addColumn( 'body_class', 'string', ['default' => null, 'null' => true])
            ->addColumn( 'action', 'string', ['null' => false])
            ->addColumn( 'target_blank', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'menu', 'string', ['limit' => 16, 'null' => false])
            ->addColumn( 'is_homepage', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'settings', 'text', ['default' => null, 'null' => true])

            // Para cuestiones de seguridad
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['parent_id'])
            ->addIndex( ['content_id'])
            ->addIndex( ['layout_id'])
            ->addIndex( ['position'])
            ->save();

        $layouts = $this->table( 'layouts');
        $layouts
            ->addColumn( 'site_id', 'integer', ['default' => null, 'null' => true])
            ->addColumn( 'template', 'string', ['null' => false, 'limit' => 36])
            ->addColumn( 'title', 'string', ['null' => false])

             // Para cuestiones de seguridad
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

            ->addColumn( 'by_default', 'boolean', ['default' => 0, 'null' => false])
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['site_id'])
            ->addIndex( ['by_default'])
            ->save();

        $wrappers = $this->table( 'wrappers');
        $wrappers
            ->addColumn( 'title', 'string', ['null' => false])
            ->addColumn( 'layout_id', 'integer', ['null' => false])
            ->addColumn( 'cname', 'string', ['null' => false])

             // Para cuestiones de seguridad
            ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['layout_id'])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'sections');
      $this->dropTable( 'layouts');
      $this->dropTable( 'wrappers');
    }
}