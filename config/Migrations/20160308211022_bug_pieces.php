<?php

use Phinx\Migration\AbstractMigration;

class BugPieces extends AbstractMigration
{
  public function up()
  {
    $pieces = $this->table( 'pieces');
    $pieces
        ->changeColumn( 'file', 'string', ['default' => null, 'null' => true])
        ->save();    
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $pieces
        ->changeColumn( 'file', 'integer', ['default' => null, 'null' => true])
        ->save();    
  }
}
