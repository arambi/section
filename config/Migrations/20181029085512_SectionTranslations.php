<?php
use Migrations\AbstractMigration;

class SectionTranslations extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sections_translations = $this->table( 'sections_translations');

    if( !$sections_translations->hasColumn( 'url'))
    {
      $sections_translations
        ->addColumn( 'external_url', 'string', ['null' => true, 'default' => null])
        ->update();  
    }
  }
}
