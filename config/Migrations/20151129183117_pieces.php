<?php

use Phinx\Migration\AbstractMigration;

class Pieces extends AbstractMigration
{
    
  public function up()
  {
    $pieces = $this->table( 'pieces');
    $pieces
        ->addColumn( 'position', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'file', 'integer', ['default' => null, 'null' => true])
        ->addColumn( 'template', 'string', ['null' => false])
        ->addColumn( 'folder', 'string', ['null' => false])
        ->addColumn( 'body', 'text', ['default' => null, 'null' => true])

        // Para cuestiones de seguridad
        ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

        ->addColumn( 'created', 'datetime', ['default' => null])
        ->addColumn( 'modified', 'datetime', ['default' => null])
        ->addIndex( ['file'])
        ->addIndex( ['folder'])
        ->addIndex( ['position'])
        ->save();    
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $this->dropTable( 'pieces');
  }
}
