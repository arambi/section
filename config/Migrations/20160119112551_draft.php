<?php

use Phinx\Migration\AbstractMigration;

class Draft extends AbstractMigration
{
   
  public function up()
  {
    $contents = $this->table( 'sections');
    $contents
      ->addColumn( 'is_draft', 'boolean', ['null' => true, 'default' => 0])
      ->update();
  }
  
  public function down()
  {
    $contents = $this->table( 'sections');
    $contents->removeColumn( 'is_draft')->update();
  }
}
