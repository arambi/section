<?php

use Phinx\Migration\AbstractMigration;

class SectionTitleNull extends AbstractMigration
{

  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->changeColumn( 'title', 'string', ['null' => true, 'default' => null])
        ->save();
  }
}
