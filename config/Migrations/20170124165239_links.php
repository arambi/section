<?php

use Phinx\Migration\AbstractMigration;

class Links extends AbstractMigration
{

  public function change()
  {
    $links = $this->table( 'links');
    $links
        ->addColumn( 'title', 'string', ['limit' => 255, 'null' => true])
        ->addColumn( 'model', 'string', ['limit' => 64, 'null' => true])
        ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => true])
        ->addColumn( 'section_id', 'string', ['limit' => 36, 'null' => true])
        ->addColumn( 'doc', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'settings', 'text', ['default' => null, 'null' => true])
        ->addColumn( 'position', 'integer', ['limit' => 6, 'null' => false])
        ->addColumn( 'target_blank', 'boolean', ['default' => 0, 'null' => false])
        ->addColumn( 'type', 'string', ['limit' => 36, 'null' => true])
        ->addColumn( 'url', 'string', ['limit' => 255, 'null' => true])
        ->addColumn( 'created', 'datetime', array('default' => null))
        ->addColumn( 'modified', 'datetime', array('default' => null))
        ->addIndex( ['model', 'foreign_key'], ['unique' => false])
        ->addIndex( ['model'], ['unique' => false])
        ->save();
  }
}
