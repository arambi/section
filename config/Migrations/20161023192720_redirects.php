<?php

use Phinx\Migration\AbstractMigration;

class Redirects extends AbstractMigration
{
  public function change()
  {
    $redirects = $this->table( 'redirects');
    $redirects
        ->addColumn( 'origin_url', 'string', ['null' => false])
        ->addColumn( 'redirect_url', 'text', ['null' => false])
        ->addIndex( ['origin_url'])
        ->save();
  }
}
