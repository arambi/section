<?php

use Phinx\Migration\AbstractMigration;

class Draft2 extends AbstractMigration
{

  public function change()
  {
    $contents = $this->table( 'sections');
    $contents
      ->changeColumn( 'is_draft', 'boolean', ['null' => false, 'default' => 0])
      ->update();
  }
}
