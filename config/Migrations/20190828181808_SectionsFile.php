<?php
use Migrations\AbstractMigration;

class SectionsFile extends AbstractMigration
{
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'file', 'text', ['default' => null, 'null' => true])
        ->update();
  }
}
