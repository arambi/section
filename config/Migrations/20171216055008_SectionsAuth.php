<?php
use Migrations\AbstractMigration;

class SectionsAuth extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'restricted', 'boolean', ['default' => 0, 'null' => true])
        ->update();
    
    $sections_groups = $this->table( 'sections_groups');
    $sections_groups
        ->addColumn( 'section_id', 'integer', ['null' => false])
        ->addColumn( 'group_id', 'integer', ['null' => false])
        ->addIndex( ['section_id', 'group_id'])
        ->addIndex( ['section_id'])
        ->addIndex( ['group_id'])
        ->create();
  }
}
