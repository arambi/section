<?php

use Phinx\Migration\AbstractMigration;

class Key extends AbstractMigration
{
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
      ->addColumn( 'cname', 'string', ['default' => null, 'null' => true])
      ->addIndex( 'cname')
      ->save();
  }
}
