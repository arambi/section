<?php

use Phinx\Migration\AbstractMigration;

class Show extends AbstractMigration
{
  public function up()
  {
    $sections = $this->table( 'sections');

    // Presente en las migas de pan
    $sections->addColumn( 'show_menu', 'boolean', ['default' => 1, 'null' => false])->update();
  }

  public function down()
  {
    $sections = $this->table( 'sections');
    $sections->removeColumn( 'show_menu')->update();
  }
}
