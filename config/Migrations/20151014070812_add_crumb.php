<?php

use Phinx\Migration\AbstractMigration;

class AddCrumb extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
      $sections = $this->table( 'sections');

      // Presente en las migas de pan
      $sections->addColumn( 'in_crumbs', 'boolean', ['default' => 1, 'null' => false])->update();
    }

    public function down()
    {
      $sections->removeColumn( 'in_crumbs')->update();
    }
}
