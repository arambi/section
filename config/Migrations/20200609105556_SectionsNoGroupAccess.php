<?php
use Migrations\AbstractMigration;

class SectionsNoGroupAccess extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $this->table( 'sections')
      ->addColumn( 'no_access_group', 'boolean', ['default' => 0, 'null' => true])
      ->update();
    
    $this->table( 'sections_nogroups')
      ->addColumn( 'section_id', 'integer', ['null' => false])
      ->addColumn( 'group_id', 'integer', ['null' => false])
      ->addIndex( ['section_id', 'group_id'])
      ->addIndex( ['section_id'])
      ->addIndex( ['group_id'])
      ->create();
  }
}
