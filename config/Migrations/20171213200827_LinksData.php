<?php
use Migrations\AbstractMigration;

class LinksData extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $links = $this->table( 'links');
    $links
        ->addColumn( 'data1', 'string', ['default' => null, 'null' => true])
        ->addColumn( 'data2', 'string', ['default' => null, 'null' => true])
        ->addColumn( 'data3', 'string', ['default' => null, 'null' => true])
        ->update();
  }
}
