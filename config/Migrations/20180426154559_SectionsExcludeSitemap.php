<?php
use Migrations\AbstractMigration;

class SectionsExcludeSitemap extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'sitemap_exclude', 'boolean', ['default' => 0, 'null' => false])
        ->update();
  }
}
