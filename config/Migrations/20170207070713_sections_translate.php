<?php

use Phinx\Migration\AbstractMigration;

class SectionsTranslate extends AbstractMigration
{

  public function up()
  {
    $sections = $this->table( 'sections_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $sections
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'title_menu', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'header', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'subtitle_menu', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->save(); 

    $pieces = $this->table( 'pieces_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $pieces
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'body', 'text', ['default' => null, 'null' => true])
      ->save(); 

    $links = $this->table( 'links_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $links
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['limit' => 255, 'null' => true])
      ->addColumn( 'url', 'string', ['limit' => 255, 'null' => true])
      ->save(); 
  }

  public function down()
  {
    $this->dropTable( 'sections_translations');
    $this->dropTable( 'pieces_translations');
    $this->dropTable( 'links_translations');
  }
}
