<?php

use Phinx\Migration\AbstractMigration;

class SectionsImgs extends AbstractMigration
{

  public function change()
  {
    $sections = $this->table( 'sections');

    if( !$sections->hasColumn( 'header'))
    {
       $sections->addColumn( 'header', 'text', ['default' => null, 'null' => true]);
    }

    if( !$sections->hasColumn( 'bg_header'))
    {
      $sections->addColumn( 'bg_header', 'text', ['default' => null, 'null' => true]);
    }

    if( !$sections->hasColumn( 'bg_page'))
    {
      $sections->addColumn( 'bg_page', 'text', ['default' => null, 'null' => true]);
    }

    $sections->save();
  }
}
