<?php
use Migrations\AbstractMigration;

class SectionsPassword extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'with_pwd', 'boolean', ['default' => 0, 'null' => false])
        ->addColumn( 'pwd', 'string', ['default' => null, 'null' => true])
        ->update();
  }
}
