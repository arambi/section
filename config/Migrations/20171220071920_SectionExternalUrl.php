<?php
use Migrations\AbstractMigration;

class SectionExternalUrl extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change()
  {
    $sections = $this->table( 'sections');
    $sections
        ->addColumn( 'external_url', 'string', ['default' => null, 'null' => true])
        ->update();
  }
}
