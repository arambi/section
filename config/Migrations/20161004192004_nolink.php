<?php

use Phinx\Migration\AbstractMigration;

class Nolink extends AbstractMigration
{
  public function change()
  {
    $sections = $this->table( 'sections');

    $sections
      ->addColumn( 'nolink', 'boolean', ['default' => 0, 'null' => false])
      ->save();
  }
}
