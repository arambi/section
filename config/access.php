<?php

$config ['Access'] = [
  'layouts' => [
    'name' => 'Estructura',
    'options' => [
      'edit' => [
        'name' => 'Edición',
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Layouts',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Layouts',
            'action' => 'add',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Layouts',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Layouts',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Layouts',
            'action' => 'delete',
          ]
        ]
      ]
    ]
  ],
  'sections' => [
    'name' => 'Secciones',
    'options' => [
      'edit' => [
          'name' => 'Edición',
          'nodes' => [
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'index',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'create',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'update',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'delete',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'add',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'update',
            ],
            [
              'prefix' => 'admin',
              'plugin' => 'Section',
              'controller' => 'Sections',
              'action' => 'order',
            ]
          ]
      ]
    ]
  ]
];