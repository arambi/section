<?php
use Cake\Core\Configure;
use Section\Action\ActionCollection;
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;
use Block\Lib\BlocksRegistry;


ActionCollection::set( 'entry', [
  'label' => 'Página fija',
  'plugin' => 'Section',
  'controller' => 'Sections',
  'action' => 'view',
  'icon' => 'fa fa-file-text-o',
]);

ActionCollection::set( 'reference', [
  'label' => 'Página existente',
  'icon' => 'fa fa-mail-forward',
]);

ActionCollection::set( 'external', [
  'label' => 'Página externa',
  'icon' => 'fa fa-link',
]);

ActionCollection::set( 'container', [
  'label' => 'Sección contenedor',
  'icon' => 'fa fa-level-down',
]);

ActionCollection::set( 'file', [
  'label' => 'Archivo',
  'icon' => 'fa fa-file',
  'plugin' => 'Section',
  'controller' => 'Sections',
  'action' => 'file',
]);

ActionCollection::set( 'anchor', [
  'label' => 'Ancla',
  'icon' => 'fa fa-file',
  'plugin' => 'Section',
  'controller' => 'Sections',
  'action' => 'view',
  'icon' => 'fa fa-anchor',
]);

if (Configure::read('Section.withPassword')) {
  ActionCollection::set( 'access', [
    'label' => 'Página de acceso',
    'plugin' => 'Section',
    'controller' => 'Sections',
    'action' => 'access',
    'icon' => 'fa fa-lock',
    'autocreate' => true,
    'defaults' => [
      'title' => 'Acceso a página restringida',
      'menu' => 'none',
      'show_menu' => false
    ]
  ]);
}

if( file_exists( ROOT .DS. 'config' .DS. 'section.php'))
{
  Configure::load( 'section');
}


// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add( [
  'name' => 'Secciones',
  'icon' => 'fa fa-sitemap',
  'key' => 'sections',
  'plugin' => 'Section',
  'controller' => 'Sections',
  'action' => 'index',
]);

NavigationCollection::add( [
  'name' => 'Plantillas',
  'icon' => 'fa fa-th-list',
  'plugin' => 'Section',
  'key' => 'layouts',
  'controller' => 'Layouts',
  'action' => 'index',
]);


Access::add( 'layouts', [
  'name' => 'Estructura',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Layouts',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Layouts',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Layouts',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Layouts',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Layouts',
          'action' => 'delete',
        ]
      ]
    ]
  ]
]);

Access::add( 'sections', [
  'name' => 'Secciones',
  'options' => [
    'edit' => [
        'name' => 'Edición',
        'nodes' => [
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'draft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'savedraft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'deletedraft',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'previewredirect',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'add',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'order',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'field',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'autocomplete',
          ],
          [
            'plugin' => 'Section',
            'controller' => 'Sections',
            'action' => 'view',
          ]
        ]
    ]
  ]
]);



Access::add( 'pieces', [
  'name' => 'Párrafos',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Pieces',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);






// Bloque Mapa web
BlocksRegistry::add( 'webmap', [
    'key' => 'webmap',
    'title' => __d( 'admin', 'Mapa web'),
    'icon' => 'fa fa-sitemap',
    'afterAddTarget' => 'parent',
    'inline' => true,
    'unique' => true,
    'deletable' => true,   
    'className' => 'Section\\Model\\Block\\WebmapBlock',
    'cell' => 'Section.Webmap::display',
    'blockView' => 'Section/blocks/webmap'
]);




Access::add( 'links', [
  'name' => 'Enlaces',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Links',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);








Access::add( 'redirects', [
  'name' => 'Redirecciones',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'sortable',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Section',
          'controller' => 'Redirects',
          'action' => 'field',
        ]
      ]
    ]
  ]
]);



NavigationCollection::add( [
  'name' => 'Redirecciones',
  'parentName' => 'Redirecciones',
  'plugin' => 'Section',
  'controller' => 'Redirects',
  'action' => 'index',
  'icon' => 'fa fa-square',
]);