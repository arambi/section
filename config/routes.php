<?php

use I18n\Lib\Lang;
use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Routing\RouteBuilder;
use Section\Routing\SectionsRoutes;
use Cake\Datasource\ConnectionManager;

Router::plugin(
    'Section',
    ['path' => '/{lang}/section'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::plugin(
    'Section',
    ['path' => '/section'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

$argv = env('argv');

if (isset($argv[1]) && $argv[1] == 'migrations') {
    return;
}

$connection = ConnectionManager::get('default');

foreach (['sections', 'slugs', 'seos'] as $table) {
    $results = $connection->execute("SHOW TABLES LIKE '$table'")->fetchAll('assoc');

    if (empty($results)) {
        return;
    }
}

if (Configure::read('Installing')) {
    return;
}

Router::extensions(['xml']);

$sessionConfig = (array)Configure::read('Session') + [
    'defaults' => 'php',
];

$session = new Session($sessionConfig);


if (isset($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '.') === false && $session->read('Manager.preview')) {
    Configure::write('Manager.preview', true);
    $session->delete('Manager.preview');
}

session_write_close();

$SectionsTable = TableRegistry::getTableLocator()->get('Section.Sections');
$SectionsTable->removeBehavior('Blockable');
$query = $SectionsTable->find('nav')
    ->order('position');

if (!Configure::read('Manager.preview')) {
    $query->where(['Sections.published' => 1]);
}

$sections = $query->toArray();
TableRegistry::getTableLocator()->remove('Section.Sections');
SectionsRoutes::nestedRoutes($sections);
SectionsRoutes::dynamics();

Router::connect('/sitemap', [
    'plugin' => 'Section',
    'controller' => 'Sections',
    'action' => 'sitemap'
]);
